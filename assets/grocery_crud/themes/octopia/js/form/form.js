$(document).ready(function() {

	$('#crudForm #form-button-save').on('click', function(e){
		e.preventDefault();
		e.stopPropagation();
		$("#report-error").addClass('d-none');
		$.ajax({
			url: validation_url,
			data: $('#crudForm').serialize(),
			method: 'post',
			dataType: 'json',
			success: function (output) {
				if (output.success) {
					$.ajax({
						url: $('#crudForm').attr('action'),
						data: $('#crudForm').serialize(),
						method: 'post',
						dataType: 'json',
						success: function (output) {
							if (output.success) {
								location.href = list_url;
							} else {
								if(!output.error_message.length) {
									output.error_message = 'Ha ocurrido un error';
								}
								$("#report-error").html('<p>'+output.error_message+'</p>');
								$("#report-error").removeClass('d-none');
							}
						}
					});

				} else {
					if(!output.error_message.length) {
						output.error_message = 'Ha ocurrido un error';
					}
					$("#report-error").html('<p>'+output.error_message+'</p>');
					$("#report-error").removeClass('d-none');
				}
			}
		});
	});

} );
