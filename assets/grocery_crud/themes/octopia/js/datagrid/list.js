$(document).ready(function() {
	$('#table').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'copy',
				className: 'btn btn-primary'
			},
			{
				extend: 'csv',
				className: 'btn btn-primary'
			},
			{
				extend: 'excel',
				className: 'btn btn-primary'
			},
			{
				extend: 'pdf',
				className: 'btn btn-primary'
			},
			{
				extend: 'print',
				className: 'btn btn-primary'
			}
		]
	} );

	$(".delete-row").on('click', function(e){
		e.preventDefault();
		$("#delete-form").attr('action', $(this).data('target'));
		$('#delete-modal').modal('show');
	});

	$("#delete-form button[type=submit]").on('click', function(e){
		e.preventDefault();
		$.ajax({
			url: $("#delete-form").attr('action'),
			dataType: 'json',
			success: function (output) {
				if (output.success) {
					location.reload();
				} else {
					alert(output.error_message);
				}
			}
		});
		$('#delete-modal').modal('hide');
	});
} );
