<div class="crud-form" data-unique-hash="<?php echo $unique_hash; ?>">
    <div class="gc-container table_<?php echo $table; ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="table-label">
                    <div class="floatL l5">
                        <?php echo $this->l('form_edit'); ?> <?php echo $subject?>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-container table-container">
                        <?php echo form_open( $update_url, 'method="post" id="crudForm"  enctype="multipart/form-data" class="form-horizontal"'); ?>

                            <?php foreach($fields as $field) { ?>
                                <div class="form-group <?php echo $field->field_name; ?>_form_group">
                                    <label class="col-sm-3 control-label">
                                        <?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?>
                                    </label>
                                    <div class="col-sm-9">
                                        <?php echo $input_fields[$field->field_name]->input; ?>
                                    </div>
                                </div>
                            <?php }?>

                            <?php if(!empty($hidden_fields)){?>
                                <!-- Start of hidden inputs -->
                                <?php
                                foreach($hidden_fields as $hidden_field){
                                    echo $hidden_field->input;
                                }
                                ?>
                                <!-- End of hidden inputs -->
                            <?php } ?>
                            <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
                            <div class="form-group">
								<div id='report-error' class='report-div error bg-danger px-2 text-white d-none'></div>
								<div id='report-success' class='report-div success bg-success px-2 text-white d-none'></div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-7">
									<button class="btn btn-default btn-success b10" type="submit" id="form-button-save">
										<i class="fa fa-check"></i>
										<?php echo $this->l('form_save'); ?>
									</button>
									<a href="<?php echo $list_url; ?>" class="btn btn-default cancel-button b10" id="cancel-button">
										<i class="fa fa-warning"></i>
										<?php echo $this->l('form_cancel'); ?>
									</a>
                                </div>
                            </div>

                        <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";

	var __url__ = "<?php echo base_url(); ?>";
</script>
