<?php


    // Backwards compatibility for older Grocery CRUD versions
    $unset_clone = isset($unset_clone) ? $unset_clone : true;

    $colspans = (count($columns) + 2);

    //Start counting the buttons that we have:
    $buttons_counter = 0;

    if (!$unset_edit) {
        $buttons_counter++;
    }

    if (!$unset_read) {
        $buttons_counter++;
    }

    if (!$unset_delete) {
        $buttons_counter++;
    }

    if (!$unset_clone) {
        $buttons_counter++;
    }

    if (!empty($list[0]) && !empty($list[0]->action_urls)) {
        $buttons_counter = $buttons_counter +  count($list[0]->action_urls);
    }

    //The search column string exists only in version 1.5.6 or higher
    $search_column_string =
        preg_match('/1\.(5\.[6-9]|[6-9]\.[0-9])/', Grocery_CRUD::VERSION)
            ? $this->l('list_search_column') : 'Buscar {column_name}';

    $search_column_string = '{column_name}';

    $hasAlerti18n = preg_match('/1\.(5\.[8-9]|[6-9]\.[0-9])/', Grocery_CRUD::VERSION);

    $alert_multiple_delete = $hasAlerti18n
        ? $this->l('alert_delete_multiple') : 'Are you sure that you want to delete those {items_amount} items?';

    $alert_multiple_delete_one = $hasAlerti18n
        ? $this->l('alert_delete_multiple_one') : 'Are you sure that you want to delete this 1 item?';

    $list_displaying = str_replace(
        array(
            '{start}',
            '{end}',
            '{results}'
        ),
        array(
            '<span class="paging-starts">1</span>',
            '<span class="paging-ends">10</span>',
            '<span class="current-total-results">'. $this->get_total_results() . '</span>'
        ),
        $this->l('list_displaying'));

    include(__DIR__ . '/common_javascript_vars.php');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';

    var subject = '<?php echo $subject?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list_url = '<?php echo $ajax_list_url;?>';
    var unique_hash = '<?php echo $unique_hash; ?>';

    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var THEME_VERSION = '1.4.4';
</script>

    <br/>
    <div class="container-fluid gc-container">
 		<div class="row">
        	<div class="table-section w-100">
                <div class="table-container">
                    <?php echo form_open("", 'method="post" autocomplete="off" id="gcrud-search-form"'); ?>

                        <?php if(isset($filters)){ ?>
                            <div class="">  <?php //grocery-crud-table ?>
                                <div class="header-search search-js"> <?php //gc-search-row ?>
                                    <div class='general-search row'>
                                        <?php foreach($filters as $filter){?>
                                            <div class="col-md-2">
                                                <label for="<?php echo $filter->name; ?>"><?php echo str_replace('{column_name}', $filter->display_as, $search_column_string); ?></label>
                                                <?php echo str_replace("Seleccionar ".$filter->display_as, $filter->display_as, $filter->input); ?>
                                            </div>
                                        <?php } ?>

                                        <?php if($filters_adv){?>
                                            <div class="col-md-2">
                                                <button class="btn bg_gris" type="button" data-toggle="collapse" data-target="#collapseAdvanced" aria-expanded="false" aria-controls="collapseAdvanced">
                                                    Avanzados
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="collapse advanced-search row" id="collapseAdvanced">
                                        <?php if($filters_adv){?>

                                            <?php foreach($filters_adv as $filter){ ?>
                                                    <div class="col-md-3">
                                                        <label for="<?php echo $filter->name; ?>"><?php echo str_replace('{column_name}', $filter->display_as, $search_column_string); ?></label>
                                                        <?php echo str_replace("Seleccionar ".$filter->display_as, $filter->display_as, $filter->input); ?>
                                                    </div>
                                            <?php } ?>

                                        <?php } ?>
                                    </div>
                                   
                                </div>
                            </div>

                        

                        <?php } ?>

                        <div class="header-tools">
                            <?php if(!$unset_add){?>
                                <div class="floatL t5">
                                    <a class="btn btn-default" href="<?php echo $add_url?>"><i class="fa fa-plus"></i> &nbsp; <?php echo $this->l('list_add'); ?> <?php echo $subject?></a>
                                </div>
                            <?php } ?>
                            <?php if(!$unset_goTo){ ?>
                                <?php foreach ($goTo_array as $goTo_label => $goTo_url) { ?>
                                    <div class="floatL t5">
										<?php if(!is_array($goTo_url)) : ?>
                                        	<a class="btn bg_gris search_lista" href="<?php echo $goTo_url?>"><?php echo $goTo_label?></a>
										<?php else : ?>
                                        <a id="<?php echo $goTo_url['id']; ?>" class="btn bg_gris search_lista <?php echo $goTo_url['class']; ?>" data-url="<?php echo $goTo_url['url']; ?>"><?php echo $goTo_label?></a>
										<?php endif; ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>

                        <div class="block-content block-content-full">
        			        <table id="table" class="table table-bordered table-striped table-vcenter w-100 js-dataTable-full">
								<thead>
									<tr>
										<?php /*
										<th colspan="2" <?php if ($buttons_counter === 0) {?>class="hidden"<?php }?>>
											<?php echo $this->l('list_actions'); ?>
										</th>
 */ ?>
										<?php foreach($columns as $column){?>
											<th class="column-with-ordering" data-order-by="<?php echo $column->field_name; ?>"><?php echo $column->display_as; ?></th>
										<?php } ?>
										<td></td>
									</tr>
								</thead>
								<tbody>
									<?php include(__DIR__."/list_tbody.php"); ?>
								</tbody>
								<tfoot><tr></tr></tfoot>
                            </table>
                        </div>
                            <!-- Table Footer -->
        					<div class="footer-tools">

								<div class="floatR r5">

									<!-- Start of: Settings button -->
									<div class="btn-group floatR t20 l10 settings-button-container" style='display:none'>
										<button type="button" class="btn btn-default gc-bootstrap-dropdown settings-button dropdown-toggle">
											<i class="fa fa-cog r5"></i>
											<span class="caret"></span>
										</button>

										<ul class="dropdown-menu dropdown-menu-right">
											<li>
												<a href="javascript:void(0)" class="clear-filtering">
													<i class="fa fa-eraser"></i> Borrar filtros
												</a>
											</li>
										</ul>
									</div>
									<!-- End of: Settings button -->

								</div>

								<div class="clear"></div>
                            </div>
                            <!-- End of: Table Footer -->

                    <?php echo form_close(); ?>
                </div>
        	</div>

            <!-- Delete confirmation dialog -->
			<div class="modal fade show" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-popout" aria-modal="true">
				<div class="modal-dialog modal-dialog-popout" role="document">
					<div class="modal-content">
						<div class="block block-themed block-transparent mb-0">
							<div class="block-header bg-primary-dark">
								<h4 class="modal-title text-white"><?php echo $this->l('list_delete'); ?></h4>
								<div class="block-options">
									<button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
										<i class="fa fa-fw fa-times"></i>
									</button>
								</div>
							</div>
							<div class="block-content">
								<p><?php echo $this->l('alert_delete'); ?></p>							</div>
							<div class="block-content block-content-full text-right bg-light">
								<button type="button" class="btn btn-sm btn-light" data-dismiss="modal"><?php echo $this->l('form_cancel'); ?></button>
								<?php echo form_open("", 'method="post" autocomplete="off" id="delete-form" class="d-inline"'); ?>
									<button type="submit" class="btn btn-sm btn-primary"><?php echo $this->l('list_delete'); ?></button>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
            <!-- End of Delete confirmation dialog -->

            </div>
        </div>
