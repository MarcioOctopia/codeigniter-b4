<?php


    // Backwards compatibility for older Grocery CRUD versions
    $unset_clone = isset($unset_clone) ? $unset_clone : true;

    $colspans = (count($columns) + 2);

    //Start counting the buttons that we have:
    $buttons_counter = 0;

    if (!$unset_edit) {
        $buttons_counter++;
    }

    if (!$unset_read) {
        $buttons_counter++;
    }

    if (!$unset_delete) {
        $buttons_counter++;
    }

    if (!$unset_clone) {
        $buttons_counter++;
    }

    if (!empty($list[0]) && !empty($list[0]->action_urls)) {
        $buttons_counter = $buttons_counter +  count($list[0]->action_urls);
    }

    //The search column string exists only in version 1.5.6 or higher
    $search_column_string =
        preg_match('/1\.(5\.[6-9]|[6-9]\.[0-9])/', Grocery_CRUD::VERSION)
            ? $this->l('list_search_column') : 'Buscar {column_name}';

    $search_column_string = '{column_name}';

    $hasAlerti18n = preg_match('/1\.(5\.[8-9]|[6-9]\.[0-9])/', Grocery_CRUD::VERSION);

    $alert_multiple_delete = $hasAlerti18n
        ? $this->l('alert_delete_multiple') : 'Are you sure that you want to delete those {items_amount} items?';

    $alert_multiple_delete_one = $hasAlerti18n
        ? $this->l('alert_delete_multiple_one') : 'Are you sure that you want to delete this 1 item?';

    $list_displaying = str_replace(
        array(
            '{start}',
            '{end}',
            '{results}'
        ),
        array(
            '<span class="paging-starts">1</span>',
            '<span class="paging-ends">10</span>',
            '<span class="current-total-results">'. $this->get_total_results() . '</span>'
        ),
        $this->l('list_displaying'));

    include(__DIR__ . '/common_javascript_vars.php');
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';

    var subject = '<?php echo $subject?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list_url = '<?php echo $ajax_list_url;?>';
    var unique_hash = '<?php echo $unique_hash; ?>';

    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var THEME_VERSION = '1.4.4';
</script>

    <br/>
    <div class="container-fluid gc-container">
        <div class="success-message hidden"><?php
        if($success_message !== null){?>
           <?php echo $success_message; ?> &nbsp; &nbsp;
        <?php }
        ?></div>

 		<div class="row">
        	<div class="table-section">
                <div class="table-label">
                    <div class="floatL l5">
                        <?php echo $subject_plural; ?>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="table-container">
                    <?php echo form_open("", 'method="post" autocomplete="off" id="gcrud-search-form"'); ?>

                        <?php if(isset($filters)){ ?>
                            <div class="">  <?php //grocery-crud-table ?>
                                <div class="header-search search-js"> <?php //gc-search-row ?>
                                    <div class='general-search row'>
                                        <?php foreach($filters as $filter){?>
                                            <div class="col-md-2">
                                                <label for="<?php echo $filter->name; ?>"><?php echo str_replace('{column_name}', $filter->display_as, $search_column_string); ?></label>
                                                <?php echo str_replace("Seleccionar ".$filter->display_as, $filter->display_as, $filter->input); ?>
                                            </div>
                                        <?php } ?>

                                        <?php if($filters_adv){?>
                                            <div class="col-md-2">
                                                <button class="btn bg_gris" type="button" data-toggle="collapse" data-target="#collapseAdvanced" aria-expanded="false" aria-controls="collapseAdvanced">
                                                    Avanzados
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="collapse advanced-search row" id="collapseAdvanced">
                                        <?php if($filters_adv){?>

                                            <?php foreach($filters_adv as $filter){ ?>
                                                    <div class="col-md-3">
                                                        <label for="<?php echo $filter->name; ?>"><?php echo str_replace('{column_name}', $filter->display_as, $search_column_string); ?></label>
                                                        <?php echo str_replace("Seleccionar ".$filter->display_as, $filter->display_as, $filter->input); ?>
                                                    </div>
                                            <?php } ?>

                                        <?php } ?>
                                    </div>
                                   
                                </div>
                            </div>

                        

                        <?php } ?>

                        <div class="header-tools">
                            <?php if(!$unset_add){?>
                                <div class="floatL t5">
                                    <a class="btn btn-default" href="<?php echo $add_url?>"><i class="fa fa-plus"></i> &nbsp; <?php echo $this->l('list_add'); ?> <?php echo $subject?></a>
                                </div>
                            <?php } ?>
                            <?php if(!$unset_goTo){ ?>
                                <?php foreach ($goTo_array as $goTo_label => $goTo_url) { ?>
                                    <div class="floatL t5">
										<?php if(!is_array($goTo_url)) : ?>
                                        	<a class="btn bg_gris search_lista" href="<?php echo $goTo_url?>"><?php echo $goTo_label?></a>
										<?php else : ?>
                                        <a id="<?php echo $goTo_url['id']; ?>" class="btn bg_gris search_lista <?php echo $goTo_url['class']; ?>" data-url="<?php echo $goTo_url['url']; ?>"><?php echo $goTo_label?></a>
										<?php endif; ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div class="floatR">
                                <?php if(!$unset_export) { ?>
                                    <a class="btn btn-default t5 gc-export" data-url="<?php echo $export_url; ?>">
                                        <i class="fa fa-cloud-download floatL t3"></i>
                                        <span class="hidden-xs floatL l5">
                                            <?php echo $this->l('list_export');?>
                                        </span>
                                        <div class="clear"></div>
                                    </a>
                                <?php } ?>
                                <?php if(!$unset_print) { ?>
                                    <a class="btn btn-default t5 gc-print" data-url="<?php echo $print_url; ?>">
                                        <i class="fa fa-print floatL t3"></i>
                                        <span class="hidden-xs floatL l5">
                                            <?php echo $this->l('list_print');?>
                                        </span>
                                        <div class="clear"></div>
                                    </a>
                                <?php } ?>

                                <?php if(0) { ?>
                                    <a class="btn btn-primary search-button t5">
                                        <i class="fa fa-search"></i>
                                        <input type="text" name="search" class="search-input" />
                                    </a>
                                <?php } ?>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="scroll-if-required">
        			        <table class="table table-bordered grocery-crud-table table-hover">
        					<thead>
        						<tr>
        							<th colspan="2" <?php if ($buttons_counter === 0) {?>class="hidden"<?php }?>>
                                        <?php echo $this->l('list_actions'); ?>
                                    </th>
                                    <?php foreach($columns as $column){?>
                                        <th class="column-with-ordering" data-order-by="<?php echo $column->field_name; ?>"><?php echo $column->display_as; ?></th>
                                    <?php }?>
        						</tr>
        						
        						<tr class="filter-row gc-search-row">
        							<td class="no-border-right <?php if ($buttons_counter === 0) {?>hidden<?php }?>">
                                        <?php if (!$unset_delete) { ?>
            							     <div class="floatL t5">
            							         <input type="checkbox" class="select-all-none" />
            							     </div>
                                         <?php } ?>
        							 </td>
        							<td class="no-border-left <?php if ($buttons_counter === 0) {?>hidden<?php }?>">
                                        <div class="floatL">
                                            <a href="javascript:void(0);" title="<?php echo $this->l('list_delete')?>"
                                               class="hidden btn btn-default delete-selected-button">
                                                <i class="fa fa-trash-o text-danger"></i>
                                                <span class="text-danger"><?php echo $this->l('list_delete')?></span>
                                            </a>
                                        </div>
                                        <div class="floatR l5">
                                            <a href="javascript:void(0);" class="btn btn-default gc-refresh">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </td>
                                    
                                    <?php foreach($columns as $column){?>
                                        <td>
                                            <!-- search -->
                                        </td>
                                    <?php }?>
        						</tr>

        					</thead>
        					<tbody>
                                <?php include(__DIR__."/list_tbody.php"); ?>
        					</tbody>
                            </table>
                        </div>
                            <!-- Table Footer -->
        					<div class="footer-tools">

                                            <!-- "Show 10/25/50/100 entries" (dropdown per-page) -->
                                            <div class="floatL t20 l5">
                                                <div class="floatL t10">
                                                    <?php list($show_lang_string, $entries_lang_string) = explode('{paging}', $this->l('list_show_entries')); ?>
                                                    <?php echo $show_lang_string; ?>
                                                </div>
                                                <div class="floatL r5 l5 t3">
                                                    <select name="per_page" class="per_page form-control">
                                                        <?php foreach($paging_options as $option){?>
                                                            <option value="<?php echo $option; ?>"
                                                                    <?php if($option == $default_per_page){?>selected="selected"<?php }?>>
                                                                        <?php echo $option; ?>&nbsp;&nbsp;
                                                            </option>
                                                        <?php }?>
                                                    </select>
                                                </div>
                                                <div class="floatL t10">
                                                    <?php echo $entries_lang_string; ?>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <!-- End of "Show 10/25/50/100 entries" (dropdown per-page) -->


                                            <div class="floatR r5">

                                                <!-- Buttons - First,Previous,Next,Last Page -->
                                                <ul class="pagination">
                                                    <li class="disabled paging-first"><a href="#"><i class="fa fa-step-backward"></i></a></li>
                                                    <li class="prev disabled paging-previous"><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                                    <li>
                                                        <span class="page-number-input-container">
                                                            <input type="number" value="1" class="form-control page-number-input" />
                                                        </span>
                                                    </li>
                                                    <li class="next paging-next"><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                                    <li class="paging-last"><a href="#"><i class="fa fa-step-forward"></i></a></li>
                                                </ul>
                                                <!-- End of Buttons - First,Previous,Next,Last Page -->

                                                <input type="hidden" name="page_number" class="page-number-hidden" value="1" />

                                                <!-- Start of: Settings button -->
                                                <div class="btn-group floatR t20 l10 settings-button-container" style='display:none'>
                                                    <button type="button" class="btn btn-default gc-bootstrap-dropdown settings-button dropdown-toggle">
                                                        <i class="fa fa-cog r5"></i>
                                                        <span class="caret"></span>
                                                    </button>

                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li>
                                                            <a href="javascript:void(0)" class="clear-filtering">
                                                                <i class="fa fa-eraser"></i> Borrar filtros
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!-- End of: Settings button -->

                                            </div>


                                            <!-- "Displaying 1 to 10 of 116 items" -->
                                            <div class="floatR r10 t30">
                                                <?php echo $list_displaying; ?>
                                                <span class="full-total-container hidden">
                                                    <?php echo str_replace(
                                                                "{total_results}",
                                                                "<span class='full-total'>" . $this->get_total_results() . "</span>",
                                                                $this->l('list_filtered_from'));
                                                    ?>
                                                </span>
                                            </div>
                                            <!-- End of "Displaying 1 to 10 of 116 items" -->

                                            <div class="clear"></div>
                            </div>
                            <!-- End of: Table Footer -->

                    <?php echo form_close(); ?>
                </div>
        	</div>

            <!-- Delete confirmation dialog -->
            <div class="delete-confirmation modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?php echo $this->l('list_delete'); ?></h4>
                        </div>
                        <div class="modal-body">
                            <p><?php echo $this->l('alert_delete'); ?></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->l('form_cancel'); ?></button>
                            <button type="button" class="btn btn-danger delete-confirmation-button"><?php echo $this->l('list_delete'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Delete confirmation dialog -->

            <!-- Delete Multiple confirmation dialog -->
            <div class="delete-multiple-confirmation modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?php echo $this->l('list_delete'); ?></h4>
                        </div>
                        <div class="modal-body">
                            <p class="alert-delete-multiple hidden">
                                <?php echo str_replace('{items_amount}', '<span class="delete-items-amount"></span>', $alert_multiple_delete); ?>
                            </p>
                            <p class="alert-delete-multiple-one hidden">
                                <?php echo $alert_multiple_delete_one; ?>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?php echo $this->l('form_cancel'); ?>
                            </button>
                            <button type="button" class="btn btn-danger delete-multiple-confirmation-button"
                                    data-target="<?php echo $delete_multiple_url; ?>">
                                <?php echo $this->l('list_delete'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Delete Multiple confirmation dialog -->

            </div>
        </div>
