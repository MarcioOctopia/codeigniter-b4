/*
$(function() {
    var tabTemplate = "<li><a href='#{href}'>#{label}</a> <span class='ui-icon ui-icon-close' data-value='{value}' data-field='{field}' role='presentation'>Remove Tab</span></li>";

    var init_tabs = $( ".tabs" ).tabs();

    function addTab(field, label, value) {


        var id = field +'-relation-'+ value,
            li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ).replace( /\{value\}/g, value ).replace( /\{field\}/g, field)),
            tabContentTemplate = $('#'+field+"Template").html(),
            tabContentHtml = tabContentTemplate.replace( /\{primary_key_value\}/g, value )

        if($("#"+id).size()>0){
            var tabs = $('#'+field+"Tabs");
            tabs.tabs({ active: $("#"+id).index('#'+field+"Tabs div.ui-tabs-panel") });
        }
        else{
            var tabs = $('#'+field+"Tabs");
            tabs.find( ".ui-tabs-nav" ).append( li );
            tabs.append( "<div id='" + id + "'><p>" + tabContentHtml + "</p></div>" );
            tabs.tabs( "refresh" );
            tabs.tabs({ active: (tabs.find( ".ui-tabs-nav li").size()-1) });

            $('#'+id).find('.datepicker-input').removeClass('hasDatepicker').datepicker({
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true
            });

            $('#'+id).find('.datetime-input').removeClass('hasDatepicker').datetimepicker({
                timeFormat: 'hh:mm:ss',
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true
            });

            $('#'+id).find('textarea').tinymce(tinymce_options);

            $('#'+id).find(".chzn-container").remove();
            $('#'+id).find(".chosen-select,.chosen-multiple-select").removeClass('chzn-done').chosen({allow_single_deselect:true});
            $('#'+id).find(".radio-uniform").uniform();

            $('#field-'+field).children('option[value=' + value + ']').attr('selected', true);


        }
    }


    $(".relation_table_value_selector").change(function(){
        if($(this).val()!=""){
            addTab($(this).data('field'), $(this).children('option:selected').text(), $(this).val())
        }
        else
        {
            var field = $(this).data('field');
            $('#field-'+field).val('');
            var tabs = $('#'+field+"Tabs");
            var tab_count = tabs.tabs('length');
            for (i=0; i<tab_count; i++){
                tabs.tabs( "remove" , 0 )
            }
        }
    })

    // close icon: removing the tab on click
    init_tabs.delegate( "span.ui-icon-close", "click", function() {
        var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
        $( "#" + panelId ).remove();
        init_tabs.tabs( "refresh" );
        var field = $(this).data('field');
        var value = $(this).data('value');
        $('#field-'+field).children('option[value=' + value + ']').removeAttr('selected');
    });


});
*/



$(function() {
    var tabTemplate = "";

    // armo el template
    tabTemplate += "<li id='{id}'>";
        tabTemplate += "<div class='col-md-2 relation_nton_name'>";
            tabTemplate += "{label} <span class='ui-icon ui-icon-close' data-value='{value}' data-field='{field}' role='presentation'>Remove Tab</span>";
        tabTemplate += "</div>";
        tabTemplate += "<div class='col-md-10 relation_nton_datos'>";
            tabTemplate += "{datos}";
        tabTemplate += "</div>";
    tabTemplate += "</li>";



    function addTab(field, label, value) {

        var id = field +'-relation-'+ value,
            tabContentTemplate = $('#'+field+"Template").html(),
            tabContentHtml = tabContentTemplate.replace( /\{primary_key_value\}/g, value ),
            li = $( tabTemplate.replace( /\{id\}/g, id ).replace( /\{label\}/g, label ).replace( /\{value\}/g, value ).replace( /\{field\}/g, field).replace( /\{datos\}/g, tabContentHtml))
            

        if($("#"+id).size()>0)
        {
        }
        else
        {
            var tabs = $('#'+field+"Tabs > ul");
            tabs.append( li );

/*
            $('#'+id).find('.datepicker-input').removeClass('hasDatepicker').datepicker({
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true
            });

            $('#'+id).find('.datetime-input').removeClass('hasDatepicker').datetimepicker({
                timeFormat: 'hh:mm:ss',
                dateFormat: js_date_format,
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true
            });

            $('#'+id).find('textarea').tinymce(tinymce_options);

            $('#'+id).find(".chzn-container").remove();
            
            $('#'+id).find(".radio-uniform").uniform();
*/      
            $('#'+id+" .chosen-select").chosen({allow_single_deselect:true});
            $('#'+id+" .chosen-container").css('width', 'auto');


            // eliminar campo
            var id_chosen_container = $('#'+id+" .chosen-container").attr('id');
            
            if (typeof id_chosen_container !== 'undefined') {
                var partes = id_chosen_container.split('__');
                
                if (partes.length > 1) {
                    var nombre_campo = partes[1];
                    
                    $('#'+id).find('#field_'+field+'Extras__primary_key_value___' + nombre_campo + '__chosen').remove();
                }
            }
            // fin eliminar campo
            
            $('#field-'+field).children('option[value=' + value + ']').attr('selected', true);

            $('#'+id+" span.ui-icon-close").on('click', function() {
                $('#'+id).remove();
                
                var field = $(this).data('field');
                var value = $(this).data('value');
                $('#field-'+field).children('option[value=' + value + ']').removeAttr('selected');
            });
        }
    }


    $(".relation_table_value_selector").change(function(){
        if($(this).val()!=""){
            addTab($(this).data('field'), $(this).children('option:selected').text(), $(this).val())
        }
    });

    $('span.ui-icon-close.remove_nton').on('click', function() {
        var field = $(this).data('field');
        var value = $(this).data('value');

        $('#'+value).remove();
        $('#field-'+field).children('option[value=' + value + ']').removeAttr('selected');
    });


    // PARA EL CAMBIO DE OPCIONES
        
        $('.relation_table_value_selector').change(function(){
            change_preguntas($(this).data('field'), $(this).val())
        });

        var divTemplate = '';                 
        divTemplate += '<div class="opciones">';
            divTemplate += '<div class=\"col-md-10\">';
                divTemplate += '<input class=\"form-control {clase}\" value=\"{value}\" maxlength=\"255\" type=\"text\" data-field=\"{field}\" data-option=\"{option}\">';
            divTemplate += '</div>';
            divTemplate += '<div class=\"col-md-2\">';
                divTemplate += '<button class=\"btn btn-default cancel-button delete_option\" data-field=\"{field}\" data-option=\"{option}\" type=\"button\"><i class=\"fa fa-trash\"></i></button>';
                divTemplate += '<button class=\"btn btn-default btn-success option_add\" data-field=\"{field}\" data-option=\"{option}\" type=\"submit\"><i class=\"fa fa-plus\"></i></button> ';
            divTemplate += '</div>';
        divTemplate += '</div>';

        function change_preguntas(field, value)
        {
            var id = 'field-'+field+'Extras\\['+value+'\\]';
            var id_opciones = id+'\\[opciones\\]';
            var id_preguntas = id+'\\[tipos_preguntas\\]';
            
            $('#'+id_opciones).hide();
            $('#cke_'+id_opciones).hide();

            var val = $('#'+id_opciones).val();
            var valores = '';

			if(val != undefined) valores = val.split(",");

            if(valores.length)
            {
                $.each(valores, function(k, v){
                    add_option(field, value, v);
                })
            }
            else
            {
                add_option(field, value, '');
            }

            //mostrar esto solo cuando sea el tipo de pregunta correcto
            $('#'+id_preguntas).change(function(){
                change_pregunta_tipo($(this).val(), field, value)
            });
            change_pregunta_tipo($('#'+id_preguntas).val(), field, value);

        }

        function change_pregunta_tipo(val, field, option)
        {
            var option_to_show = 'Selector de opciones';
            if(val != option_to_show)
            {
                $('#'+field+'-relation-'+option+' .'+field+'Extra_opciones').hide();
            }
            else
            {
                $('#'+field+'-relation-'+option+' .'+field+'Extra_opciones').show();
            }
        }

        function add_option(field, option, value)
        {
            var clase = 'option_' + field + '_' + option;
            var div = $( divTemplate.replace( /\{option\}/g, option ).replace( /\{clase\}/g, clase ).replace( /\{value\}/g, value ).replace( /\{field\}/g, field) );

            var content = $('#'+field+'-relation-'+option+' .'+field+'Extra_opciones');

            div.find('input').on('change', function(event) {
                event.preventDefault();
                var field = $(this).data('field');
                var option = $(this).data('option');
                change_option(field, option);
            });

            div.find('.delete_option').on('click', function(event) {
                event.preventDefault();
                var field = $(this).data('field');
                var option = $(this).data('option');

                var cant = content.find('.opciones').length;

                if(cant != 1) $(this).parent().parent().remove();
                
                change_option(field, option);

                content.find('.opciones').last().find('.option_add').show();
            });

            div.find('.option_add').on('click', function(event) {
                event.preventDefault();
                var field = $(this).data('field');
                var option = $(this).data('option');
                add_option(field, option, '');

                change_option(field, option);
            });

            content.find('.option_add').hide();

            content.append( div );
        }

        function change_option(field, option)
        {

			var opciones = [];
			
            $('.option_'+field+'_'+option).each(function(){
                var valor = $(this).val();

                if(valor ) opciones.push(valor);
            });

            $('#field-'+field+'Extras\\['+option+'\\]\\[opciones\\]').val(opciones.join(','));
        }

        // para el edit
        $('.edit_charge_field').each(function(){
            var id = $(this).attr('id');
            var datos = id.split("-");

            var field = datos[0];
            var option = datos[2];
            
            change_preguntas(field, option);
        });
});