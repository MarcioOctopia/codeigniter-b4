<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once 'Base_Controller.php';

class CI_Controller extends Base_Controller {

	public $cssFiles = [];
	public $jsFiles = [];

	public function __construct()
	{
		parent::__construct();
	}

	public function render($template, $data = [], $clean = false, $html = false)
	{

		$data['template'] = $template;

		if($clean) {
			$base = 'base';
		} else {
			$base = 'app';
		}
		return $this->load->view('layout/'.$base, $data, $html);
	}

	public function renderCrud($data)
	{

		if(isset($data['grocery'])) {
			
			if(!isset($data['template'])) $data['template'] = 'grocery/default';

			$data['grocery']->unset_jquery();
			$data['grocery']->unset_read();

			if($data['grocery']->getState() == 'list' || $data['grocery']->getState() == 'ajax_list') {
				$this->jsFiles([
					"assets/dashmix/js/plugins/datatables/jquery.dataTables.min.js",
					"assets/dashmix/js/plugins/datatables/dataTables.bootstrap4.min.js",
					"assets/dashmix/js/plugins/datatables/buttons/dataTables.buttons.min.js",
					"assets/dashmix/js/plugins/datatables/buttons/buttons.print.min.js",
					"assets/dashmix/js/plugins/datatables/buttons/buttons.html5.min.js",
					"assets/dashmix/js/plugins/datatables/buttons/buttons.flash.min.js",
					"assets/dashmix/js/plugins/datatables/buttons/buttons.colVis.min.js",
					"assets/grocery_crud/themes/octopia/js/datagrid/list.js"
				]);
			}

			if($data['grocery']->getState() == 'add' || $data['grocery']->getState() == 'edit') {
				$this->jsFiles([
					"assets/grocery_crud/themes/octopia/js/form/form.js"
				]);
			}

			try{

				$data['crud'] = $data['grocery']->render();

			} catch(Exception $e) {

				if($e->getCode() == 14) //The 14 is the code of the "You don't have permissions" error on grocery CRUD.
				{
					if(isset($data['crud-deny-redirect']))
					{
						redirect($data['crud-deny-redirect'], 'refresh');
					}
					else
					{
						// por el unset_read cuando sea read lo mando al edit
						if(grocery_CRUD_States::STATE_READ == $data['grocery']->get_state_code()) {
							$url = current_url();
							$url = str_replace('/read/', '/edit/', $url);
							redirect($url, 'refresh');
						}

						show_error('Acceso Denegado', '403');
					}
				} else {
					print_r($e->getMessage());
				}
			}
		}

		return $this->load->view('layout/app', $data);
	}

	public function cssFiles($files)
	{
		if(!is_array($files))
			$files = [$files];

		foreach ($files as $file) {
			$this->cssFiles[] = base_url().$file;
		}

	}

	public function jsFiles($files)
	{
		if(!is_array($files))
			$files = [$files];

		foreach ($files as $file) {
			$this->jsFiles[] = base_url().$file;
		}

	}

	public function set_css($files)
	{
		$this->cssFiles($files);
	}

	public function set_js_lib($files)
	{
		$this->jsFiles($files);
	}

}
