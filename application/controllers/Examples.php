<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Examples extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->lang->load('auth');
		$this->load->library(['ion_auth']);
		$this->load->library('grocery_CRUD');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
	}

	public function offices_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('offices');
		$crud->set_subject('Office');
		$crud->required_fields('city');
		$crud->columns('city','country','phone','addressLine1','postalCode');

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

	public function employees_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('employees');
		$crud->set_relation('officeCode','offices','city');
		$crud->display_as('officeCode','Office City');
		$crud->set_subject('Employee');

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

	public function customers_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('customers');
		$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
		$crud->display_as('salesRepEmployeeNumber','from Employeer')
			->display_as('customerName','Name')
			->display_as('contactLastName','Last Name');
		$crud->required_fields('customerName','contactLastName');
		$crud->set_rules('customerName', 'required');
		$crud->set_subject('Customer');
		$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

	public function orders_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_relation('customerNumber','customers','contactLastName');
		$crud->display_as('customerNumber','Customer');
		$crud->set_table('orders');
		$crud->set_subject('Order');
		$crud->unset_add();
		$crud->unset_delete();

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

	public function products_management()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('products');
		$crud->set_subject('Product');
		$crud->unset_columns('productDescription');
		$crud->callback_column('buyPrice',array($this,'valueToEuro'));

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

	public function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}

}
