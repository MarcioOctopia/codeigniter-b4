<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->lang->load('auth');
		$this->load->library(['ion_auth']);
		$this->load->library('grocery_CRUD');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
	}

	public function index()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('users');

		$crud->set_subject('Usuarios');

		$data['grocery'] = $crud;

		$this->renderCrud($data);
	}

}
