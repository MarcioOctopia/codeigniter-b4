<?php
include('Grocery_crud_model_base.php');

class Grocery_crud_model extends Grocery_crud_model_base {
    // AGREGADO
    protected $relation_1_1 = [];
    
	
	function get_table_name(){
	
		return $this->table_name;
	
	}
	
    function get_extra_edit_values($table, $where)
    {
        $result = $this->db->from($table)
                            ->where($where)
                            ->get()
                            ->row();

        return $result;
    }

    function enum_select( $table , $field )
    {
        $row = $this->db->query("SHOW COLUMNS FROM ".$table." LIKE '$field'")->row()->Type;
        $regex = "/'(.*?)'/";
        preg_match_all( $regex , $row, $enum_array );
        $enum_fields = $enum_array[1];
        foreach ($enum_fields as $key=>$value)
        {
            $enums[$value] = $value;
        }
        return $enums;
    }

    function db_relation_n_n_update($field_info, $post_data, $main_primary_key, $extra_data = array())
    {
        // ADD NTON
        $crud = new Grocery_CRUD(); // solo para funciones de soporte
        $field_name = $field_info->field_name;


        if($extra_data) // arreglo post data porque no lo trae
        {
            $post_data = array();
            foreach($extra_data as $key => $val)
            {
                if($key != 0) $post_data[] = $key;
            }
        }


        $this->db->where($field_info->primary_key_alias_to_this_table, $main_primary_key);
        if(!empty($post_data))
            $this->db->where_not_in($field_info->primary_key_alias_to_selection_table , $post_data);
        $this->db->delete($field_info->relation_table);

        $counter = 0;

        if(!empty($post_data))
        {
            foreach($post_data as $primary_key_value)
            {
                $where_array = array(
                    $field_info->primary_key_alias_to_this_table => $main_primary_key,
                    $field_info->primary_key_alias_to_selection_table => $primary_key_value, 
                );

                $add_array = $where_array;

                $this->db->where($where_array);
                $count = $this->db->from($field_info->relation_table)->count_all_results();
                
                if(!empty($field_info->priority_field_relation_table))
                {
                    $add_array[$field_info->priority_field_relation_table] = $counter;
                }

                if(isset($extra_data[$primary_key_value]))
                {
                    $add_array = array_merge($add_array, $extra_data[$primary_key_value]);
                }

             

                if($count == 0)
                {
                    

                    $this->db->insert($field_info->relation_table, $add_array);
                }
                else
                {
                    $this->db->update( $field_info->relation_table, $add_array , $where_array);
                }

                $counter++;
            }
        }
    }

    // AGREGADO
    function join_relation_1_1($field_name , $related_table , $related_table_fields)
    {
		$related_primary_key = $this->get_primary_key($related_table);

		if($related_primary_key !== false)
		{
			$unique_name = $this->_unique_join_name($field_name);
			$this->db->join( $related_table.' as '.$unique_name , "$unique_name.$related_primary_key = {$this->table_name}.$field_name",'left');

			$this->relation_1_1[$field_name] = array($field_name , $related_table , $related_table_fields);

			return true;
		}

    	return false;
    }
    
    function get_list()
    {
    	if($this->table_name === null)
    		return false;

    	$select = "`{$this->table_name}`.*";

    	//set_relation special queries
    	if(!empty($this->relation))
    	{
    		foreach($this->relation as $relation)
    		{
    			list($field_name , $related_table , $related_field_title) = $relation;
    			$unique_join_name = $this->_unique_join_name($field_name);
    			$unique_field_name = $this->_unique_field_name($field_name);

				if(strstr($related_field_title,'{'))
				{
					//$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
    				$select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
				}
    			else
    			{
    				$select .= ", $unique_join_name.$related_field_title AS $unique_field_name";
    			}

    			if($this->field_exists($related_field_title))
    				$select .= ", `{$this->table_name}`.$related_field_title AS '{$this->table_name}.$related_field_title'";
    		}
    	}

    	//set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
        if(!empty($this->relation_n_n))
    	{
			$select = $this->relation_n_n_queries($select);
    	}
        
        // AGREGADO
        if(!empty($this->relation_1_1))
    	{
    		foreach($this->relation_1_1 as $relation)
    		{
                list($field_name , $related_table , $related_field_titles) = $relation;
                $unique_join_name = $this->_unique_join_name($field_name);
                
                foreach($related_field_titles as $related_field_title)
                {
                    if(strstr($related_field_title,'{'))
                    {
                        //$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
                        $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $related_field_title";
                    }
                    else
                    {
                        $select .= ", $unique_join_name.$related_field_title AS $related_field_title";
                    }

                    if($this->field_exists($related_field_title))
                        $select .= ", `{$this->table_name}`.$related_field_title AS '{$this->table_name}.$related_field_title'";
                }
    		}
    	}

    	$this->db->select($select, false);
        /*$this->db->from($this->table_name);
        $query = $this->db->get_compiled_select();
		$results = $this->db->query(str_replace('&nbsp;', ' ', $query))->result();*/
    	$results = $this->db->get($this->table_name)->result();


/*echo '<pre>';
echo $this->db->last_query();
//print_r($results);
die;*/

    	return $results;
    }

    // AGREGADO
    function get_relation_1_1_selection_array($primary_key_value, $field_name, $related_table, $related_field_titles)
    {
    	$select = "";

        foreach ($related_field_titles as $related_field_title) {
            $select .= ($select == '' ? '' : ', ')."{$related_table}.{$related_field_title}";
        }
        
    	$this->db->select($select, false);

    	$primary_key = $this->get_primary_key($this->table_name);
    	$primary_key_related_table = $this->get_primary_key($this->table_name);

        $this->db->where("{$this->table_name}.{$primary_key}", $primary_key_value);
    	$this->db->join(
    			$related_table,
    			"{$related_table}.{$primary_key_related_table} = {$this->table_name}.{$field_name}"
    		);
    	$results = $this->db->get($this->table_name)->result();

    	if ($results) {
    		$result = $results[0];
    	} else {
            $result = null;
        }

    	return $result;
    }
    
    protected function relation_n_n_queries($select)
    {
        $this_table_primary_key = $this->get_primary_key();
    	foreach($this->relation_n_n as $relation_n_n)
    	{
    		list($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table,
    					$primary_key_alias_to_selection_table, $title_field_selection_table, $priority_field_relation_table, $where_clause) = array_values((array)$relation_n_n);// AGREGADOS DATOS

    		$primary_key_selection_table = $this->get_primary_key($selection_table);

            // usar la tabla y si hay una vista definida cambiarla
            $relation_table_view = $relation_table;
            if (isset($relation_n_n->config['view']) && $relation_n_n->config['view']) 
                $relation_table_view = $relation_n_n->config['view'].' AS '.$relation_table;

	    	$field = "";
            // AGREGADO
            $joins = "";
            if (
                isset($relation_n_n->config['name_from_join']['from']) &&
                isset($relation_n_n->config['name_from_join']['from_id']) &&
                isset($relation_n_n->config['name_from_join']['to']) &&
                isset($relation_n_n->config['name_from_join']['to_id']) &&
                isset($relation_n_n->config['name_from_join']['title_table']) &&
                isset($relation_n_n->config['name_from_join']['title']) &&
                $relation_n_n->config['name_from_join']['from'] &&
                $relation_n_n->config['name_from_join']['from_id'] &&
                $relation_n_n->config['name_from_join']['to'] &&
                $relation_n_n->config['name_from_join']['to_id'] &&
                $relation_n_n->config['name_from_join']['title_table'] &&
                $relation_n_n->config['name_from_join']['title']
            ) {
                $from = $relation_n_n->config['name_from_join']['from'];
                $from_id = $relation_n_n->config['name_from_join']['from_id'];
                $to = $relation_n_n->config['name_from_join']['to'];
                $to_id = $relation_n_n->config['name_from_join']['to_id'];
                $title_table = $relation_n_n->config['name_from_join']['title_table'];
                $title = $relation_n_n->config['name_from_join']['title'];
                $where = isset($relation_n_n->config['name_from_join']['where']) ? $relation_n_n->config['name_from_join']['where'] : '';
                
                $title_field_selection_table = "$title";
                $joins .= "LEFT JOIN $to ON $from.$from_id = $to.$to_id ".($where ? "AND ".$where." " : "");
            }
            // FIN AGREGADO
            $use_template = strpos($title_field_selection_table,'{') !== false;
            $field_name_hash = $this->_unique_field_name($title_field_selection_table);
            if($use_template)
            {
                //$title_field_selection_table = str_replace(" ", "&nbsp;", $title_field_selection_table);
                $field .= "CONCAT('".str_replace(array('{','}'),array("',COALESCE(",", ''),'"),str_replace("'","\\'",$title_field_selection_table))."')";
            }
            else
            {
                $field .= "$selection_table.$title_field_selection_table";
            }

            /*//Sorry Codeigniter but you cannot help me with the subquery!
            $select .= ", (SELECT GROUP_CONCAT(DISTINCT $field) FROM $selection_table "
                ."LEFT JOIN $relation_table ON $relation_table.$primary_key_alias_to_selection_table = $selection_table.$primary_key_selection_table "
                .$joins
                ."WHERE $relation_table.$primary_key_alias_to_this_table = `{$this->table_name}`.$this_table_primary_key GROUP BY $relation_table.$primary_key_alias_to_this_table) AS $field_name";*/
            // AGREGADO
            if (
                isset($relation_n_n->config['show_grouped_totals_in_list']['fields_to_sum']) &&
                isset($relation_n_n->config['show_grouped_totals_in_list']['ids_selection_table']) &&
                $relation_n_n->config['show_grouped_totals_in_list']['fields_to_sum'] &&
                $relation_n_n->config['show_grouped_totals_in_list']['ids_selection_table']
            ) {
                $sum_fields = "";
                
                foreach ($relation_n_n->config['show_grouped_totals_in_list']['fields_to_sum'] as $field_to_add) {
                    $sum_fields .= ($sum_fields != "" ? " + " : "")."$relation_table.$field_to_add";
                }
                
                foreach ($relation_n_n->config['show_grouped_totals_in_list']['ids_selection_table'] as $id) {
                    $id = $id['id'];
                    
                    $select .= ", (SELECT ($sum_fields) FROM $selection_table "
                        ."LEFT JOIN $relation_table_view ON $relation_table.$primary_key_alias_to_selection_table = $selection_table.$primary_key_selection_table "
                        ."WHERE $relation_table.$primary_key_alias_to_this_table = `{$this->table_name}`.$this_table_primary_key "
                        ."AND $relation_table.$primary_key_alias_to_selection_table = '$id') AS ".$field_name."_".$id;
                }
            } else {
                //Sorry Codeigniter but you cannot help me with the subquery!
                $select .= ", (SELECT GROUP_CONCAT(DISTINCT $field) FROM $selection_table "
                    ."LEFT JOIN $relation_table_view ON $relation_table.$primary_key_alias_to_selection_table = $selection_table.$primary_key_selection_table "
                    .$joins
                    ."WHERE $relation_table.$primary_key_alias_to_this_table = `{$this->table_name}`.$this_table_primary_key GROUP BY $relation_table.$primary_key_alias_to_this_table) AS $field_name";
            }
    	}

    	return $select;
    }

    function get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit = null, $search_like = null, $related_id_field = null, $config = null)// AGREGADO PARÁMETRO
    {
    	$relation_array = array();
    	$field_name_hash = $this->_unique_field_name($field_name);

        // AGREGADO
        if ($related_id_field) {
            $related_primary_key = $related_id_field;
        } else {
            $related_primary_key = $this->get_primary_key($related_table);
        }

        // usar la tabla y si hay una vista definida cambiarla
        $relation_table_view = $related_table;
        if (isset($config['view']) && $config['view']) 
            $relation_table_view = $config['view'].' AS '.$related_table;

    	$select = "$related_table.$related_primary_key, ";

    	if(strstr($related_field_title,'{'))
    	{
    		//$related_field_title = str_replace(" ", "&nbsp;", $related_field_title);
    		$select .= "CONCAT('".str_replace(array('{','}'),array("',COALESCE(",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $field_name_hash";
    	}
    	else
    	{
	    	$select .= "$related_table.$related_field_title as $field_name_hash";
    	}

    	$this->db->select($select,false);
    	if($where_clause !== null)
    		$this->db->where($where_clause);

    	if($where_clause !== null)
    		$this->db->where($where_clause);

    	if($limit !== null)
    		$this->db->limit($limit);

    	if($search_like !== null)
    		$this->db->having("$field_name_hash LIKE '%".$this->db->escape_like_str($search_like)."%'");

    	$order_by !== null
    		? $this->db->order_by($order_by)
    		: $this->db->order_by($field_name_hash);

    	$results = $this->db->get($relation_table_view)->result();

    	foreach($results as $row)
    	{
    		$relation_array[$row->$related_primary_key] = $row->$field_name_hash;
    	}

    	return $relation_array;
    }
    
    function get_relation_total_rows($field_name , $related_table , $related_field_title, $where_clause, $config = null)
    {
        $relation_table_view = $related_table;
        if (isset($config['view']) && $config['view']) 
            $relation_table_view = $config['view'].' AS '.$related_table;

        if($where_clause !== null)
            $this->db->where($where_clause);

        return $this->db->count_all_results($relation_table_view);
    }

    function get_relation_n_n_selection_array($primary_key_value, $field_info)
    {
    	$select = "";
        // $related_field_title = $field_info->title_field_selection_table;
        // AGREGADO
        if (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $title_table = $field_info->config['name_from_join']['title_table'];
            $title = $field_info->config['name_from_join']['title'];
            
            $related_field_title = "$title_table.$title";
            if(strpos($title,'{') !== false) $related_field_title = "$title";
        } else {
            $related_field_title = $field_info->title_field_selection_table;
        }
    	$use_template = strpos($related_field_title,'{') !== false;;
    	$field_name_hash = $this->_unique_field_name($related_field_title);
    	if($use_template)
    	{
    		//$related_field_title = str_replace(" ", "&nbsp;", $related_field_title);
    		$select .= "CONCAT('".str_replace(array('{','}'),array("',COALESCE(",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $field_name_hash";
    	}
    	else
    	{
    		$select .= "$related_field_title as $field_name_hash";
    	}
    	$this->db->select('*, '.$select,false);

        // $selection_primary_key = $this->get_primary_key($field_info->selection_table);
        // AGREGADO
        if (isset($field_info->id_field_selection_table) && $field_info->id_field_selection_table) {
            $selection_primary_key = $field_info->id_field_selection_table;
        } else {
            $selection_primary_key = $this->get_primary_key($field_info->selection_table);
        }

    	if(empty($field_info->priority_field_relation_table))
    	{
    		if(!$use_template){
    			$this->db->order_by("{$field_info->selection_table}.{$field_info->title_field_selection_table}");
    		}
    	}
    	else
    	{
    		$this->db->order_by("{$field_info->relation_table}.{$field_info->priority_field_relation_table}");
    	}
    	$this->db->where($field_info->primary_key_alias_to_this_table, $primary_key_value);
    	$this->db->join(
    			$field_info->selection_table,
    			"{$field_info->relation_table}.{$field_info->primary_key_alias_to_selection_table} = {$field_info->selection_table}.{$selection_primary_key}"
    		);
        // AGREGADO
        if (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $from = $field_info->config['name_from_join']['from'];
            $from_id = $field_info->config['name_from_join']['from_id'];
            $to = $field_info->config['name_from_join']['to'];
            $to_id = $field_info->config['name_from_join']['to_id'];
            $title_table = $field_info->config['name_from_join']['title_table'];
            $title = $field_info->config['name_from_join']['title'];
            $where = isset($field_info->config['name_from_join']['where']) ? $field_info->config['name_from_join']['where'] : '';
            
            $this->db->join(
    			$to,
    			"{$from}.{$from_id} = {$to}.{$to_id}".($where ? " AND ".$where : "")
    		);
        }
    	$results = $this->db->get($field_info->relation_table)->result();

    	$results_array = array();
    	foreach($results as $row)
    	{
    		$results_array[$row->{$field_info->primary_key_alias_to_selection_table}] = $row->{$field_name_hash};
    	}

    	return $results_array;
    }
    
    function get_relation_n_n_unselected_array($field_info, $selected_values)
    {
    	$use_where_clause = !empty($field_info->where_clause);

    	$select = "";
    	// $related_field_title = $field_info->title_field_selection_table;
        // AGREGADO
        if (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $title_table = $field_info->config['name_from_join']['title_table'];
            $title = $field_info->config['name_from_join']['title'];
            
            $related_field_title = "$title_table.$title";
            if(strpos($title,'{') !== false) $related_field_title = "$title";
        } else {
            $related_field_title = $field_info->title_field_selection_table;
        }
    	$use_template = strpos($related_field_title,'{') !== false;
    	$field_name_hash = $this->_unique_field_name($related_field_title);

    	if($use_template)
    	{
    		//$related_field_title = str_replace(" ", "&nbsp;", $related_field_title);
    		$select .= "CONCAT('".str_replace(array('{','}'),array("',COALESCE(",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $field_name_hash";
    	}
    	else
    	{
    		$select .= "$related_field_title as $field_name_hash";
    	}
        // AGREGADO
        if (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $title_table = $field_info->config['name_from_join']['title_table'];
            $title = $field_info->config['name_from_join']['title'];
            
            $selection_primary_key = $this->get_primary_key($field_info->selection_table);
            
            $select .= ", $field_info->selection_table.$selection_primary_key as $field_info->primary_key_alias_to_selection_table";
        }
    	$this->db->select('*, '.$select,false);

    	if($use_where_clause){
    		$this->db->where($field_info->where_clause);
    	}

        // $selection_primary_key = $this->get_primary_key($field_info->selection_table);
        // AGREGADO
        if (isset($field_info->id_field_selection_table) && $field_info->id_field_selection_table) {
            $selection_primary_key = $field_info->id_field_selection_table;
        } elseif (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $selection_primary_key = $field_info->primary_key_alias_to_selection_table;
        } else {
            $selection_primary_key = $this->get_primary_key($field_info->selection_table);
        }
        if(!$use_template)
        	$this->db->order_by("{$field_info->selection_table}.{$field_info->title_field_selection_table}");
        // AGREGADO
        if (
            isset($field_info->config['name_from_join']['from']) &&
            isset($field_info->config['name_from_join']['from_id']) &&
            isset($field_info->config['name_from_join']['to']) &&
            isset($field_info->config['name_from_join']['to_id']) &&
            isset($field_info->config['name_from_join']['title_table']) &&
            isset($field_info->config['name_from_join']['title']) &&
            $field_info->config['name_from_join']['from'] &&
            $field_info->config['name_from_join']['from_id'] &&
            $field_info->config['name_from_join']['to'] &&
            $field_info->config['name_from_join']['to_id'] &&
            $field_info->config['name_from_join']['title_table'] &&
            $field_info->config['name_from_join']['title']
        ) {
            $from = $field_info->config['name_from_join']['from'];
            $from_id = $field_info->config['name_from_join']['from_id'];
            $to = $field_info->config['name_from_join']['to'];
            $to_id = $field_info->config['name_from_join']['to_id'];
            $title_table = $field_info->config['name_from_join']['title_table'];
            $title = $field_info->config['name_from_join']['title'];
            $where = isset($field_info->config['name_from_join']['where']) ? $field_info->config['name_from_join']['where'] : '';
            
            $this->db->join(
    			$to,
    			"{$from}.{$from_id} = {$to}.{$to_id}".($where ? " AND ".$where : "")
    		);
        }
        $results = $this->db->get($field_info->selection_table)->result();

        $results_array = array();
        foreach($results as $row)
        {
            if(!isset($selected_values[$row->$selection_primary_key]))
                $results_array[$row->$selection_primary_key] = $row->{$field_name_hash};
        }

        return $results_array;
    }

    function get_primary_key($table_name = null)
    {
        if($table_name == null)
    	{
    		if(isset($this->primary_keys[$this->table_name]))
    		{
    			return $this->primary_keys[$this->table_name];
    		}

	    	if(empty($this->primary_key))
	    	{
		    	$fields = $this->get_field_types_basic_table();

		    	foreach($fields as $field)
		    	{
		    		if($field->primary_key == 1)
		    		{
		    			return $field->name;
		    		}
		    	}

		    	return false;
	    	}
	    	else
	    	{
	    		return $this->primary_key;
	    	}
    	}
    	else
    	{
            // AGREGADO
            if ($this->relation_n_n) {
                foreach ($this->relation_n_n as $relation_n_n) {
                    if (
                        $relation_n_n->selection_table == $table_name &&
                        isset($relation_n_n->id_field_selection_table) &&
                        $relation_n_n->id_field_selection_table
                    ) {
                        return $relation_n_n->id_field_selection_table;
                    }
                }
            }
            
    		if(isset($this->primary_keys[$table_name]))
    		{
    			return $this->primary_keys[$table_name];
    		}

	    	$fields = $this->get_field_types($table_name);

	    	foreach($fields as $field)
	    	{
	    		if($field->primary_key == 1)
	    		{
	    			return $field->name;
	    		}
	    	}

	    	return false;
    	}

    }

    function join_relation($field_name, $related_table, $related_field_title, $where_clause = null, $order_by = null, $related_id_field = null, $config = null)// AGREGADOS PARÁMETROS
    {
        // AGREGADO
        if ($related_id_field) {
            $related_primary_key = $related_id_field;
        } else {
            $related_primary_key = $this->get_primary_key($related_table);
        }

        // usar la tabla y si hay una vista definida cambiarla
        $field_name_view = $this->table_name .'.'. $field_name;
        $relation_table_view = $related_table;
        if (isset($config['view']) && $config['view']){
            $relation_table_view = $config['view'];
            $field_name_view = $config['parent_id'];
        } 
            

		if($related_primary_key !== false)
		{
			$unique_name = $this->_unique_join_name($field_name);
			$this->db->join( $relation_table_view.' as '.$unique_name , "$unique_name.$related_primary_key = $field_name_view",'left');

			$this->relation[$field_name] = array($field_name, $related_table, $related_field_title);// AGREGADOS PARÁMETROS

			return true;
		}

    	return false;
    }

    function get_total_results()
    {
        // A fast way to calculate the total results
        $key = $this->get_primary_key();

        //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
        if(!empty($this->relation_n_n))
        {
            $select = "{$this->table_name}." . $key;
            $select = $this->relation_n_n_queries($select);

            $this->db->select($select,false);
        } else {
            $this->db->select($this->table_name . '.' . $key);
        }
        
        if(!empty($this->relation_1_1))
        {
            foreach($this->relation_1_1 as $relation)
            {
                list($field_name , $related_table , $related_field_titles) = $relation;
                $unique_join_name = $this->_unique_join_name($field_name);
                
                $primary_key_related_table = $this->get_primary_key($this->table_name);

                $this->db->join( $related_table . ' ' . $unique_join_name, "{$unique_join_name}.{$primary_key_related_table} = {$this->table_name}.{$field_name}" ,'LEFT' );
            }
        }

        return $this->db->get($this->table_name)->num_rows();
    }

    function db_update($post_array, $primary_key_value)
    {
        $log = array();
        $log['id_ci_users'] = $this->ion_auth->user()->row()->id;
        $log['email'] = $this->ion_auth->user()->row()->email;
        $log['first_name'] = $this->ion_auth->user()->row()->first_name;
        $log['last_name'] = $this->ion_auth->user()->row()->last_name;
        $log['last_name_2'] = $this->ion_auth->user()->row()->last_name_2;
        $log['table_name'] = $this->table_name;
        $log['date'] = date('Y-m-d H:i:s');

        $primary_key_field = $this->get_primary_key();
        $query = $this->db->update($this->table_name,$post_array, array( $primary_key_field => $primary_key_value));
        $log['last_query'] = $this->db->last_query();

    	return $query;
    }

    function db_insert($post_array)
    {

        $log = array();
        $log['id_ci_users'] = $this->ion_auth->user()->row()->id;
        $log['email'] = $this->ion_auth->user()->row()->email;
        $log['first_name'] = $this->ion_auth->user()->row()->first_name;
        $log['last_name'] = $this->ion_auth->user()->row()->last_name;
        $log['last_name_2'] = $this->ion_auth->user()->row()->last_name_2;
        $log['table_name'] = $this->table_name;
        $log['date'] = date('Y-m-d H:i:s');

    	$insert = $this->db->insert($this->table_name,$post_array);
    	if($insert)
    	{
            $insert_id = $this->db->insert_id();
            $log['last_query'] = $this->db->last_query();
            return $insert_id;
    	}
        return false;
        

    }

    function db_delete($primary_key_value)
    {

        $log = array();
        $log['id_ci_users'] = $this->ion_auth->user()->row()->id;
        $log['email'] = $this->ion_auth->user()->row()->email;
        $log['first_name'] = $this->ion_auth->user()->row()->first_name;
        $log['last_name'] = $this->ion_auth->user()->row()->last_name;
        $log['last_name_2'] = $this->ion_auth->user()->row()->last_name_2;
        $log['table_name'] = $this->table_name;
        $log['date'] = date('Y-m-d H:i:s');

    	$primary_key_field = $this->get_primary_key();

    	if($primary_key_field === false)
    		return false;

    	$this->db->limit(1);
    	$this->db->delete($this->table_name,array( $primary_key_field => $primary_key_value));
    	if( $this->db->affected_rows() != 1)
    		return false;
        else
            $log['last_query'] = $this->db->last_query();
    		return true;
    }


}
