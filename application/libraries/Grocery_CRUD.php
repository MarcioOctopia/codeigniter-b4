<?php
include('Grocery_CRUD_base.php');

// aca agrego extensiones de grocery, para agregar una nueva hacer heredar de la ultima y hacer que este ultimo herede la nueva
// revisar que no este sobreescrita ya la fucnoion que queremos modificar

/* NTON y mensajes de error en callback */
class grocery_CRUD_NTON extends grocery_CRUD_Base
{
	public $config    			= null;

	public $default_javascript_path	= null; //autogenerate, please do not modify
	public $default_css_path			= null; //autogenerate, please do not modify
	public $default_texteditor_path 	= null; //autogenerate, please do not modify
	public $default_theme_path		= null; //autogenerate, please do not modify
	public $default_language_path	= 'assets/grocery_crud/languages';
	public $default_config_path		= 'assets/grocery_crud/config';
	public $default_assets_path		= 'assets/grocery_crud';

	// hay que usar esta para agregar js a los abm
	protected $states = array(
		0	=> 'unknown',
		1	=> 'list',
		2	=> 'add',
		3	=> 'edit',
		4	=> 'delete',
		5	=> 'insert',
		6	=> 'update',
		7	=> 'ajax_list',
		8   => 'ajax_list_info',
		9	=> 'insert_validation',
		10	=> 'update_validation',
		11	=> 'upload_file',
		12	=> 'delete_file',
		13	=> 'ajax_relation',
		14	=> 'ajax_relation_n_n',
		15	=> 'success',
		16  => 'export',
		17  => 'print',
		18  => 'read',
		19  => 'delete_multiple',
		20  => 'clone',
		21  => 'export_zip',
		30  => 'custom_trabajadores_export'
	);

	public function inline_js($inline_js = '')
	{
		$state_code = $this->getStateCode();
		if(
			$state_code == 1
			|| $state_code == 2
			|| $state_code == 3
			|| $state_code == 20 // CLONE
		)
		{
			$this->_inline_js($inline_js);
		}
	}


	protected function change_list_value($field_info, $value = null)
	{
		$real_type = $field_info->crud_type;

		switch ($real_type) {
			case 'hidden':
			case 'invisible':
			case 'integer':

				break;
			case 'true_false':
				if(is_array($field_info->extras) && array_key_exists($value,$field_info->extras)) {
					$value = $field_info->extras[$value];
				} else if(isset($this->default_true_false_text[$value])) {
					$value = $this->default_true_false_text[$value];
				}
				break;
			case 'string':
				$value = $this->character_limiter($value,$this->character_limiter,"...");
				break;
			case 'text':
				$value = $this->character_limiter(strip_tags($value),$this->character_limiter,"...");
				break;
			case 'date':
				if(!empty($value) && $value != '0000-00-00' && $value != '1970-01-01')
				{
					list($year,$month,$day) = explode("-",$value);

					$value = date($this->php_date_format, mktime (0, 0, 0, (int)$month , (int)$day , (int)$year));
				}
				else
				{
					$value = '';
				}
				break;
			case 'datetime':
				if(!empty($value) && $value != '0000-00-00 00:00:00' && $value != '1970-01-01 00:00:00')
				{
					list($year,$month,$day) = explode("-",$value);
					list($hours,$minutes) = explode(":",substr($value,11));

					$value = date($this->php_date_format." - H:i", mktime ((int)$hours , (int)$minutes , 0, (int)$month , (int)$day ,(int)$year));
				}
				else
				{
					$value = '';
				}
				break;
			case 'enum':
				$value = $this->character_limiter($value,$this->character_limiter,"...");
				break;

			case 'multiselect':
				$value_as_array = array();
				foreach(explode(",",$value) as $row_value)
				{
					$value_as_array[] = array_key_exists($row_value,$field_info->extras) ? $field_info->extras[$row_value] : $row_value;
				}
				$value = implode(",",$value_as_array);
				break;

			case 'relation_n_n':
				$value = $this->character_limiter(str_replace(',',', ',$value),$this->character_limiter,"...");
				break;

			case 'password':
				$value = '******';
				break;

			case 'dropdown':
				$value = array_key_exists($value,$field_info->extras) ? $field_info->extras[$value] : $value;
				break;

			case 'upload_file':
				if(empty($value))
				{
					$value = "";
				}
				else
				{
					$is_image = !empty($value) &&
					( substr($value,-4) == '.jpg'
						|| substr($value,-4) == '.png'
						|| substr($value,-5) == '.jpeg'
						|| substr($value,-4) == '.gif'
						|| substr($value,-5) == '.tiff')
						? true : false;

					$file_url = base_url().$field_info->extras->upload_path."/$value";

					$script = "
					<script>
					function openWindow() {
						url = ".$file_url.";
						if (window.innerWidth <= 640) {
							// if width is smaller then 640px, create a temporary a elm that will open the link in new tab
							var a = document.createElement('a');
							a.setAttribute(\"href\", url);
							a.setAttribute(\"target\", \"_blank\");

							var dispatch = document.createEvent(\"HTMLEvents\");
							dispatch.initEvent(\"click\", true, true);

							a.dispatchEvent(dispatch);
						}
						else {
							var width = window.innerWidth * 0.66 ;
							// define the height in
							var height = width * window.innerHeight / window.innerWidth ;
							// Ratio the hight to the width as the user screen ratio
							window.open(url , 'newwindow', 'width=' + width + ', height=' + height + ', top=' + ((window.innerHeight - height) / 2) + ', left=' + ((window.innerWidth - width) / 2));
						}
						return false;
					}
					</script>
					";

					$file_url_anchor = $script . ' <a href="'.$file_url.'"';
					if($is_image)
					{
						$file_url_anchor .= ' class="image-thumbnail"><img src="'.$file_url.'" height="50px">';
					}
					else
					{
						$file_url_anchor .= ' target="_blank">'.$this->character_limiter($value,$this->character_limiter,'...',true);
					}
					$file_url_anchor .= '</a>';

					$value = $file_url_anchor;
				}
				break;

			default:
				$value = $this->character_limiter($value,$this->character_limiter,"...");
				break;
		}

		return $value;
	}


	public function _inline_js($inline_js = '')
	{
		if (!$this->_is_ajax()) {
			parent::_inline_js($inline_js);
		}
	}

	protected function db_insert($state_info)
	{
		//$validation_result = $this->db_insert_validation();
		$validation_result = (object) ['success' => false];
		$validation_result->success = true;
		if($validation_result->success)
		{
			$post_data = $state_info->unwrapped_data;

			if ($this->config->xss_clean) {
				$post_data = $this->filter_data_from_xss($post_data);
			}

			$add_fields = $this->get_add_fields();

			if($this->callback_insert === null)
			{
				if($this->callback_before_insert !== null)
				{
					$callback_return = call_user_func($this->callback_before_insert, $post_data);

					if(!empty($callback_return) && is_array($callback_return))
						$post_data = $callback_return;
					elseif($callback_return === false)
						return false;

					// ADD MENSAJE DE ERROR
					elseif( is_string($callback_return))
					{
						return array('status' => false, 'message' => $callback_return);
					}
				}

				$insert_data = array();
				$types = $this->get_field_types();
				foreach($add_fields as $num_row => $field)
				{
					/* If the multiselect or the set is empty then the browser doesn't send an empty array. Instead it sends nothing */
					if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect') && !isset($post_data[$field->field_name]))
					{
						$post_data[$field->field_name] = array();
					}

					if(isset($post_data[$field->field_name]) && !isset($this->relation_n_n[$field->field_name]))
					{
						if(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && is_array($post_data[$field->field_name]) && empty($post_data[$field->field_name]))
						{
							$insert_data[$field->field_name] = null;
						}
						elseif(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && $post_data[$field->field_name] === '')
						{
							$insert_data[$field->field_name] = null;
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'date')
						{
							$insert_data[$field->field_name] = $this->_convert_date_to_sql_date($post_data[$field->field_name]);
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'readonly')
						{
							//This empty if statement is to make sure that a readonly field will never inserted/updated
						}
						elseif(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect'))
						{
							$insert_data[$field->field_name] = !empty($post_data[$field->field_name]) ? implode(',',$post_data[$field->field_name]) : '';
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'datetime'){
							$insert_data[$field->field_name] = $this->_convert_date_to_sql_date(substr($post_data[$field->field_name],0,10)).
								substr($post_data[$field->field_name],10);
						}
						else
						{
							$insert_data[$field->field_name] = $post_data[$field->field_name];
						}
					}
				}

				$insert_result =  $this->basic_model->db_insert($insert_data);

				if($insert_result !== false)
				{
					$insert_primary_key = $insert_result;
				}
				else
				{
					return false;
				}

				if(!empty($this->relation_n_n))
				{
					foreach($this->relation_n_n as $field_name => $field_info)
					{
						$relation_data = isset( $post_data[$field_name] ) ? $post_data[$field_name] : array() ;

						// ADD NTON
						$extra_data = isset($post_data[$field_name.'Extras'])?$post_data[$field_name.'Extras']:array();
						$extra_fields_types = $this->basic_model->get_field_types($field_info->relation_table);
						foreach($extra_data as $key=>$extra_field){
							foreach($extra_field as $key2=>$column){
								foreach($extra_fields_types as $extra_fields_type){
									if($extra_fields_type->name == $key2){
										$extra_fields_type->db_type = $extra_fields_type->type;
										$extra_fields_type->db_max_length = $extra_fields_type->max_length;
										$column_type = $this->get_type($extra_fields_type);
										break;
									}
								}
								if($column_type == 'datetime'){
									$extra_data[$key][$key2] = $this->_convert_date_to_sql_date(substr($column,0,10)).
										substr($column,10);
								}
								elseif($column_type == 'date') {
									$extra_data[$key][$key2] = $this->_convert_date_to_sql_date($column);
								}
								elseif($column_type == 'set' || $column_type == 'multiselect'){
									$extra_data[$key][$key2] = !empty($extra_data[$key][$key2]) ? implode(',',$extra_data[$key][$key2]) : '';
								}
							}
						}

						$this->db_relation_n_n_update_new($field_info, $relation_data  ,$insert_primary_key, $extra_data);
					}
				}

				if($this->callback_after_insert !== null)
				{
					$callback_return = call_user_func($this->callback_after_insert, $post_data, $insert_primary_key);

					if($callback_return === false)
					{
						return false;
					}

				}
			}else
			{
				$callback_return = call_user_func($this->callback_insert, $post_data);

				if($callback_return === false)
				{
					return false;
				}
			}

			if(isset($insert_primary_key))
				return $insert_primary_key;
			else
				return true;
		}

		return false;
	}

	protected function insert_layout($insert_result = false)
	{
		@ob_end_clean();
		if($insert_result === false)
		{
			echo json_encode(array('success' => false));
		}
		elseif(isset($insert_result['status'])){
			//TODO:custom error
			$error_message = $insert_result['message'];
			echo json_encode(array(
				'success' => $insert_result['status'] ,
				'error_message' => $error_message,
			));
		}
		else
		{
			$success_message = '<p>'.$this->l('insert_success_message');

			if(!$this->unset_back_to_list && !empty($insert_result) && !$this->unset_edit)
			{
				$success_message .= " <a class='go-to-edit-form' href='".$this->getEditUrl($insert_result)."'>".$this->l('form_edit')." {$this->subject}</a> ";

				if (!$this->_is_ajax()) {
					$success_message .= $this->l('form_or');
				}
			}

			if(!$this->unset_back_to_list && !$this->_is_ajax())
			{
				$success_message .= " <a href='".$this->getListUrl()."'>".$this->l('form_go_back_to_list')."</a>";
			}

			$success_message .= '</p>';

			echo json_encode(array(
				'success' => true ,
				'insert_primary_key' => $insert_result,
				'success_message' => $success_message,
				'success_list_url'	=> $this->getListSuccessUrl($insert_result)
			));
		}
		$this->set_echo_and_die();
	}

	protected function db_update($state_info)
	{
		//$validation_result = $this->db_update_validation();

		$validation_result = (object) ['success' => false];
		$validation_result->success = true;

		$edit_fields = $this->get_edit_fields();

		if($validation_result->success)
		{
			$post_data 		= $state_info->unwrapped_data;
			$primary_key 	= $state_info->primary_key;

			if ($this->config->xss_clean) {
				$post_data = $this->filter_data_from_xss($post_data);
			}

			if($this->callback_update === null)
			{
				if($this->callback_before_update !== null)
				{
					$callback_return = call_user_func($this->callback_before_update, $post_data, $primary_key);

					if(!empty($callback_return) && is_array($callback_return))
					{
						$post_data = $callback_return;
					}
					elseif($callback_return === false)
					{
						return false;
					}

					// ADD MENSAJE DE ERROR
					elseif( is_string($callback_return))
					{
						return array('status' => false, 'message' => $callback_return);
					}

				}

				$update_data = array();
				$types = $this->get_field_types();
				foreach($edit_fields as $num_row => $field)
				{
					/* If the multiselect or the set is empty then the browser doesn't send an empty array. Instead it sends nothing */
					if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect') && !isset($post_data[$field->field_name]))
					{
						$post_data[$field->field_name] = array();
					}

					if(isset($post_data[$field->field_name]) && !isset($this->relation_n_n[$field->field_name]))
					{
						if(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && is_array($post_data[$field->field_name]) && empty($post_data[$field->field_name]))
						{
							$update_data[$field->field_name] = null;
						}
						elseif(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && $post_data[$field->field_name] === '')
						{
							$update_data[$field->field_name] = null;
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'date')
						{
							$update_data[$field->field_name] = $this->_convert_date_to_sql_date($post_data[$field->field_name]);
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'readonly')
						{
							//This empty if statement is to make sure that a readonly field will never inserted/updated
						}
						elseif(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect'))
						{
							$update_data[$field->field_name] = !empty($post_data[$field->field_name]) ? implode(',',$post_data[$field->field_name]) : '';
						}
						elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'datetime'){
							$update_data[$field->field_name] = $this->_convert_date_to_sql_date(substr($post_data[$field->field_name],0,10)).
								substr($post_data[$field->field_name],10);
						}
						else
						{
							$update_data[$field->field_name] = $post_data[$field->field_name];
						}
					}
				}

				if($this->basic_model->db_update($update_data, $primary_key) === false)
				{
					return false;
				}

				if(!empty($this->relation_n_n))
				{
					foreach($this->relation_n_n as $field_name => $field_info)
					{
						if (   $this->unset_edit_fields !== null
							&& is_array($this->unset_edit_fields)
							&& in_array($field_name,$this->unset_edit_fields)
						) {
							continue;
						}

						$relation_data = isset( $post_data[$field_name] ) ? $post_data[$field_name] : array() ;

						// ADD NTON
						$extra_data = isset($post_data[$field_name.'Extras'])?$post_data[$field_name.'Extras']:array();
						$extra_fields_types = $this->basic_model->get_field_types($field_info->relation_table);
						foreach($extra_data as $key=>$extra_field){
							foreach($extra_field as $key2=>$column){
								foreach($extra_fields_types as $extra_fields_type){
									if($extra_fields_type->name == $key2){
										$extra_fields_type->db_type = $extra_fields_type->type;
										$extra_fields_type->db_max_length = $extra_fields_type->max_length;
										$column_type = $this->get_type($extra_fields_type);
										break;
									}
								}
								if($column_type == 'datetime'){
									$extra_data[$key][$key2] = $this->_convert_date_to_sql_date(substr($column,0,10)).
										substr($column,10);
								}
								elseif($column_type == 'date') {
									$extra_data[$key][$key2] = $this->_convert_date_to_sql_date($column);
								}
								elseif($column_type == 'set' || $column_type == 'multiselect'){
									$extra_data[$key][$key2] = !empty($extra_data[$key][$key2]) ? implode(',',$extra_data[$key][$key2]) : '';
								}
							}
						}
						$this->db_relation_n_n_update_new($field_info, $relation_data ,$primary_key, $extra_data);
					}
				}

				if($this->callback_after_update !== null)
				{
					$callback_return = call_user_func($this->callback_after_update, $post_data, $primary_key);

					if($callback_return === false)
					{
						return false;
					}

				}
			}
			else
			{
				$callback_return = call_user_func($this->callback_update, $post_data, $primary_key);

				if($callback_return === false)
				{
					return false;
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	protected function update_layout($update_result = false, $state_info = null)
	{
		@ob_end_clean();
		if($update_result === false)
		{
			echo json_encode(array('success' => $update_result));
		}
		elseif(isset($update_result['status'])){
			//TODO:custom error
			$error_message = $update_result['message'];
			echo json_encode(array(
				'success' => $update_result['status'] ,
				'error_message' => $error_message,
			));
		}
		else
		{
			$success_message = '<p>'.$this->l('update_success_message');
			if(!$this->unset_back_to_list && !$this->_is_ajax())
			{
				$success_message .= " <a href='".$this->getListUrl()."'>".$this->l('form_go_back_to_list')."</a>";
			}
			$success_message .= '</p>';

			echo json_encode(array(
				'success' => true ,
				'insert_primary_key' => $update_result,
				'success_message' => $success_message,
				'success_list_url'	=> $this->getListSuccessUrl($state_info->primary_key)
			));
		}
		$this->set_echo_and_die();
	}

	protected function db_relation_n_n_update_new($field_info, $post_data , $primary_key_value, $extra_data = array())
	{
		$this->basic_model->db_relation_n_n_update($field_info, $post_data , $primary_key_value, $extra_data);
	}

	protected function get_relation_n_n_input($field_info_type, $selected_values)
	{
		// ADD NTON

		$has_priority_field = !empty($field_info_type->extras->priority_field_relation_table) ? true : false;
		$has_extra_fields = isset($field_info_type->extras->extra_fields) && !empty($field_info_type->extras->extra_fields) ? true : false;
		$is_ie_7 = isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') !== false) ? true : false;

		if($has_priority_field || $is_ie_7)
		{
			$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
			$this->set_css($this->default_css_path.'/jquery_plugins/ui.multiselect.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui.multiselect.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.multiselect.js');

			if($this->language !== 'english')
			{
				include($this->default_config_path.'/language_alias.php');
				if(array_key_exists($this->language, $language_alias))
				{
					$i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/multiselect/ui-multiselect-'.$language_alias[$this->language].'.js';
					if(file_exists($i18n_date_js_file))
					{
						$this->set_js_lib($i18n_date_js_file);
					}
				}
			}
		}
		else if($has_extra_fields){
			$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
			$this->set_css($this->default_css_path.'/jquery_plugins/chosen/chosen.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);

			$this->set_js($this->default_javascript_path.'/jquery_plugins/config/jquery.tab.config.js');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.chosen.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
		}
		else
		{
			$this->set_css($this->default_css_path.'/jquery_plugins/chosen/chosen.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.chosen.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
		}

		$this->inline_js("var ajax_relation_url = '".$this->getAjaxRelationUrl()."';\n");

		$field_info 		= $this->relation_n_n[$field_info_type->name]; //As we use this function the relation_n_n exists, so don't need to check
		$unselected_values 	= $this->get_relation_n_n_unselected_array($field_info, $selected_values);

		if(empty($unselected_values) && empty($selected_values))
		{
			$input = "Please add {$field_info_type->display_as} first";
		}
		else
		{
			// ADD NTON
			if($has_extra_fields)
			{
				$extra_field_name = $field_info_type->name;
				$select_title = str_replace('{field_display_as}',$field_info_type->display_as,$this->l('set_relation_title'));
				$input = "<select id='field-{$field_info_type->name}' name='{$field_info_type->name}[]' multiple='multiple' size='8' data-placeholder='$select_title' style='display:none;' >";

				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name){
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name){
						$input .= "<option value='$id' selected='selected'>$name</option>";
					}

				$input .= "</select>";

				$input .= "<select class='relation_table_value_selector chosen-select' data-field='{$field_info_type->name}' data-placeholder='$select_title'>";

				$input .= "<option value=''></option>";

				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name){
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name){
						$input .= "<option value='$id' selected='selected'>$name</option>";
						//$input .= "<option value='$id'>$name</option>";
					}

				$input .= "</select>";

				$extra_fields = $this->basic_model->get_field_types($field_info_type->extras->relation_table);
				$extra_state_info = $this->getStateInfo();

				$input .= "<div id='{$field_info_type->name}Tabs' class='tabs'>";
				$input .= " <ul>";

				if(!empty($selected_values))
				{
					foreach($selected_values as $id => $name){
						$id_html = "$extra_field_name-relation-$id";

						$extra_where = array($field_info_type->extras->primary_key_alias_to_this_table => $extra_state_info->primary_key ,
							$field_info_type->extras->primary_key_alias_to_selection_table => $id);
						$values = $this->basic_model->get_extra_edit_values($field_info_type->extras->relation_table, $extra_where);

						$campos_extra = '';
						foreach ($extra_fields as $extra_field){
							if (   $this->unset_edit_fields !== null
								&& is_array($this->unset_edit_fields)
								&& in_array(('extra_field_'.$extra_field->name),$this->unset_edit_fields)
							) {
								continue;
							}
							if( $extra_field->name != $field_info_type->extras->primary_key_alias_to_this_table
								&& $extra_field->name != $field_info_type->extras->primary_key_alias_to_selection_table){

								$extra_field->db_type = $extra_field->type;
								$extra_field->db_max_length = $extra_field->max_length;

								$extra_field_info = (object)array();
								$extra_field_info->name		= $field_info_type->name.'Extras['.$id.']['.$extra_field->name.']';
								$extra_field_info->type 	= $this->get_type($extra_field);
								$extra_field_info->crud_type 	= $this->get_type($extra_field);
								if($extra_field_info->crud_type== 'text'){
									$extra_field_info->extras = 'text_editor';
									$extra_field_info->extras = null;
								}
								elseif($extra_field_info->crud_type == 'set' || $extra_field_info->crud_type == 'enum'){

									$extra_field_info->extras = $this->basic_model->enum_select($field_info_type->extras->relation_table, $extra_field->name);
								}
								else{
									$extra_field_info->extras = null;
								}

								//var_dump($extra_field_info->extras);
								$extra_field_info->db_max_length = $extra_field->db_max_length;
								$extra_field_info->required	= false;
								//$extra_field_info->display_as = ucfirst(str_replace("_"," ",$extra_field->name));
								$extra_field_info->display_as = isset($this->display_as['extra_field_'.$extra_field->name]) ?
									$this->display_as['extra_field_'.$extra_field->name] :
									ucfirst(str_replace("_"," ",$extra_field->name));

								$campos_extra .= "<div>".$extra_field_info->display_as."</div><div class='".$field_info_type->name."Extra_".$extra_field->name."'>".$this->get_field_input($extra_field_info, $values->{$extra_field->name})->input."</div>";
							}
						}

						$input .= "<li id='$id_html' class='edit_charge_field'>";
						$input .= "<div class='col-md-2 relation_nton_name'>";
						$input .= "$name <span class='ui-icon ui-icon-close remove_nton' data-value='$id_html' data-field='{$field_info_type->name}' role='presentation'>Remove Tab</span>";
						$input .= "</div>";
						$input .= "<div class='col-md-10 relation_nton_datos'>";
						$input .= "$campos_extra";
						$input .= "</div>";
						$input .= "</li>";
					}
				}

				$input .= "</ul>";
				$input .= "</div>";
				$input .= "<div id='{$field_info_type->name}Template' style='display:none'>";

				foreach ($extra_fields as $extra_field){
					if (   $this->unset_edit_fields !== null
						&& is_array($this->unset_edit_fields)
						&& in_array(('extra_field_'.$extra_field->name),$this->unset_edit_fields)
					) {
						continue;
					}
					if( $extra_field->name != $field_info_type->extras->primary_key_alias_to_this_table
						&& $extra_field->name != $field_info_type->extras->primary_key_alias_to_selection_table){

						$extra_field->db_type = $extra_field->type;
						$extra_field->db_max_length = $extra_field->max_length;

						$extra_field_info = (object)array();
						$extra_field_info->name		= $field_info_type->name.'Extras[{primary_key_value}]['.$extra_field->name.']';
						$extra_field_info->type 	= $this->get_type($extra_field);
						$extra_field_info->crud_type 	= $this->get_type($extra_field);
						if($extra_field_info->crud_type == 'text'){
							$extra_field_info->extras = 'text_editor_to_be';

							$extra_field_info->extras = null;
						}
						elseif($extra_field_info->crud_type == 'set' || $extra_field_info->crud_type == 'enum'){
							$extra_field_info->extras = $this->basic_model->enum_select($field_info_type->extras->relation_table, $extra_field->name);
						}
						else{
							$extra_field_info->extras = null;
						}
						$extra_field_info->db_max_length = $extra_field->db_max_length;
						$extra_field_info->required	= false;
						$extra_field_info->display_as = isset($this->display_as['extra_field_'.$extra_field->name]) ?
							$this->display_as['extra_field_'.$extra_field->name] :
							ucfirst(str_replace("_"," ",$extra_field->name));
						$input .= "<div>".$extra_field_info->display_as."</div><div class='".$field_info_type->name."Extra_".$extra_field->name."'>".$this->get_field_input($extra_field_info)->input."</div>";
					}
				}
				$input .= "</div>";
			}
			else
			{
				$css_class = $has_priority_field || $is_ie_7 ? 'multiselect': 'chosen-multiple-select';
				$width_style = $has_priority_field || $is_ie_7 ? '' : 'width:510px;';

				$select_title = str_replace('{field_display_as}',$field_info_type->display_as,$this->l('set_relation_title'));
				$input = "<select id='field-{$field_info_type->name}' name='{$field_info_type->name}[]' multiple='multiple' size='8' class='$css_class' data-placeholder='$select_title' style='$width_style' >";

				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name)
					{
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name)
					{
						$input .= "<option value='$id' selected='selected'>$name</option>";
					}

				$input .= "</select>";
			}
		}

		return $input;
	}

	public function set_relation_n_n_extra($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table, $primary_key_alias_to_selection_table , $title_field_selection_table , $priority_field_relation_table = null, $where_clause = null)
	{
		$this->relation_n_n[$field_name] =
			(object)array(
				'field_name' => $field_name,
				'relation_table' => $relation_table,
				'selection_table' => $selection_table,
				'primary_key_alias_to_this_table' => $primary_key_alias_to_this_table,
				'primary_key_alias_to_selection_table' => $primary_key_alias_to_selection_table ,
				'title_field_selection_table' => $title_field_selection_table ,
				'priority_field_relation_table' => $priority_field_relation_table,
				'where_clause' => $where_clause,
				'extra_fields' => true, // ADD NTON
			);



		return $this;
	}
}

class grocery_CRUD_CHAINSELECT extends grocery_CRUD_NTON
{
	protected $unset_ajax_extension = false;
	protected $state_code = null;
	protected $slash_replacement = "_agsl_";
	protected $relation_dependency = array();

	function __construct()
	{
		parent::__construct();

		$this->states[101]='ajax_extension';
	}

	protected function get_relation_array($relation_info, $primary_key_value = null, $limit = null)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by)  = $relation_info;

		if($primary_key_value !== null)
		{
			$primary_key = $this->basic_model->get_primary_key($related_table);

			//A where clause with the primary key is enough to take the selected key row
			$where_clause = array($primary_key => $primary_key_value);
		}

		$relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit);

		$dep = 0;
		if($this->state_code != 3)
		{
			foreach($this->relation_dependency as $target_field => $dependency)
			{
				if($field_name == $target_field) $dep = 1;
			}
		}

		// TODO si es state 3 hay que cargar solo los de ese grupo

		if($dep == 1) $relation_array = array();

		return $relation_array;
	}

	public function set_relation_dependency($target_field, $source_field, $relation_field_on_source_table)
	{
		$this->relation_dependency[$target_field] = array($target_field, $source_field,$relation_field_on_source_table);
		return $this;
	}

	private function render_relation_dependencies()
	{
		foreach($this->relation_dependency as $dependency)
		{
			$this->render_relation_dependency($dependency[0],$dependency[1],$dependency[2]);
		}
	}

	private function render_relation_dependency($target_field, $source_field, $relation_field_on_source_table)
	{
		$ci = &get_instance();

		$name_sec = $ci->security->get_csrf_token_name();
		$val_sec = $ci->security->get_csrf_hash();

		$sourceElement = "'#field-$source_field'";
		$targetElement = "'#field-$target_field'";

		/*$js_text = "
            $(document).ready(function() {

                $($sourceElement).change(function() {
                    var selectedValue = $($sourceElement).val();
                    $.post(
                        'ajax_extension/$target_field/$relation_field_on_source_table/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')),
                        {
                            $name_sec : '$val_sec'
                        },
                        function(data) {

                            var \$el = $($targetElement);
                            var newOptions = data;

                            \$el.empty(); // remove old options
                            \$el.append(\$('<option></option>').attr('value', '').text(''));
                            \$.each(newOptions, function(key, value) {
                                if(key)
                                {
                                    \$el.append(\$('<option></option>').attr('value', key).text(value));
                                }
                            });
                            \$el.chosen().trigger('chosen:updated');

                              },'json');
                      $($targetElement).change();
                });
            });
            ";*/
		// AGREGADO
		$js_text = "
			$(document).ready(function()
            {
				$($sourceElement).change(function()
                {
					update_values_$target_field()
				});
                
                update_values_$target_field()
			});
            
            function update_values_$target_field()
            {
                if (typeof $($sourceElement).val() !== 'undefined')
                {
                    var selectedValue = $($sourceElement).val();
                    $.post(
                        'ajax_extension/$target_field/$relation_field_on_source_table/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')), 
                        {
                            $name_sec : '$val_sec'
                        }, 
                        function(data) {
                            
                            var \$el = $($targetElement);
                            var newOptions = data;
                            
                            // ordenar array
                            var sortable = [];
                            
                            for (var option in newOptions) {
                                sortable.push([option, newOptions[option]]);
                            }
                            
                            sortable.sort(function(a, b) {
                                var x=a[1].toLowerCase(),
                                    y=b[1].toLowerCase();
                                return x<y ? -1 : x>y ? 1 : 0;
                            });
                            
                            newOptions = sortable;
                            // fin ordenar array
                            
                            var selectedTargetOption = \$el.val();
                            
                            \$el.empty(); // remove old options
                            \$el.append(\$('<option></option>').attr('value', '').text(''));
                            \$.each(newOptions, function(key, value) {
                                key = value[0];
                                value = value[1];
                                
                                if(key == selectedTargetOption)
                                {
                                    \$el.append(\$('<option></option>').attr('value', key).text(value).attr('selected', 'selected'));
                                }
                                else if(key)
                                {
                                    \$el.append(\$('<option></option>').attr('value', key).text(value));
                                }
                            });
                            \$el.chosen().trigger('chosen:updated');

                            },'json');
                    $($targetElement).change();
                }
            }
			";

		$this->inline_js($js_text);
	}

	public function render()
	{
		// lo saco poruqe se ejecuta 2 veces una aca y otra en el parent y el multiupload no guarda
		//$this->pre_render();

		$this->state_code = $this->getStateCode();

		if( $this->state_code != 0 )
		{
			$this->state_info = $this->getStateInfo();
		}
		else
		{
			throw new Exception('The state is unknown , I don\'t know what I will do with your data!', 4);
			die();
		}

		switch ($this->state_code)
		{
			case 2://add
			case 3://edit
			case 5://insert
			case 6://update
				$this->render_relation_dependencies();
				$output = parent::render();
				break;

			case 21: //export to zip

				$this->character_limiter = 1000000;

				if($this->unset_export)
				{
					throw new Exception('You don\'t have permissions for this operation', 15);
					die();
				}

				$state_info = $this->getStateInfo();
				$this->set_ajax_list_queries($state_info);
				$this->exportToZIPGastos($state_info);
				break;

			case 30: //export to excel custom

				$this->character_limiter = 1000000;

				if($this->unset_export)
				{
					throw new Exception('You don\'t have permissions for this operation', 15);
					die();
				}

				$state_info = $this->getStateInfo();
				$this->set_ajax_list_queries($state_info);
				$this->customTrabajadoresExport($state_info);
				break;

			case 101://ajax_extension

				$state_info = $this->getStateInfo();
				$ajax_extension_result = $this->ajax_extension($state_info);
				$ajax_extension_result[""] = "";
				echo json_encode($ajax_extension_result);

				die();

				break;
			default:
				$output = parent::render();
				break;
		}

		if(empty($output)){
			$output = $this->get_layout();
		}else{
		}

		return $output;
	}

	protected function exportToZIPGastos($state_info = null)
	{
		$base_url = base_url();

		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;
		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();
		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->columns 				= $this->get_columns();
		$data->primary_key 			= $this->get_primary_key();

		$zip = new ZipArchive();
		$filename = '/assets/gastos.zip';

		$pathZIP = realpath($_SERVER['DOCUMENT_ROOT'] . parse_url( $base_url, PHP_URL_PATH ));
		$zip->open($pathZIP.$filename, ZIPARCHIVE::CREATE | ZipArchive::OVERWRITE);

		foreach ($data->list as $key => $value) {

			$patkTicket = $base_url.'assets/uploads/eventos/'.$value->id_evento.'/tickets/'.$value->foto_ticket;
			$patkTicket = realpath($_SERVER['DOCUMENT_ROOT'] . parse_url( $patkTicket, PHP_URL_PATH ));

			$zip->addFile($patkTicket, $value->foto_ticket);

		}

		$zip->close();

		header('Location: ' . $base_url.$filename);

	}

	protected function customTrabajadoresExport($state_info = null)
	{

		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;
		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();

		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->columns 				= $this->get_columns();
		$data->primary_key 			= $this->get_primary_key();

		@ob_end_clean();
		$this->_custom_trabajadores_export_to_excel($data);
	}

	protected function _custom_trabajadores_export_to_excel($data)
	{

		/**
		 * No need to use an external library here. The only bad thing without using external library is that Microsoft Excel is complaining
		 * that the file is in a different format than specified by the file extension. If you press "Yes" everything will be just fine.
		 * */

		$string_to_export = '';

		$string_to_export .= '<table border="1" cellpadding="2" cellspacing="0" width="100%">';
		$string_to_export .= '<tr>';
		foreach($data->columns as $column){
			$string_to_export .= '<td>'.$column->display_as.'</td>';
		}
		$string_to_export .= '</tr>';

		foreach($data->list as $num_row => $row){
			$string_to_export .= '<tr>';
			foreach($data->columns as $column) {
				$bg = '';
				if($this->check_if_value_is_invalid($column, $row->{$column->field_name}))
					$bg = 'bgcolor="red"';
				$string_to_export .= '<td '.$bg.'>'.$this->_trim_export_string($row->{$column->field_name})."</td>";
			}
			$string_to_export .= '</tr>';
		}

		$string_to_export .= '</table>';

		// Convert to UTF-16LE and Prepend BOM
		//$string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-16LE', 'UTF-8');

		$filename = "export-".date("Y-m-d_H:i:s").".xls";

		header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
		//header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
		header('Content-Disposition: attachment; filename='.$filename);
		header("Cache-Control: no-cache");
		echo $string_to_export;
		die();
	}

	protected function check_if_value_is_invalid($column, $value)
	{

		ini_set('display_errors',0);

		if($this->check_if_value_is_empty($value) &&
			(in_array($column->field_name, ['documento', 'numero_seguridad_social', 'iban', 'id_poblaciones', 'id_provincias']) || $column->display_as == 'Población' || $column->display_as == 'Provincia'))
			return true;

		$ci = &get_instance();
		$ci->load->model('trabajadorModel', 'trabajadorModel');

		switch ($column->field_name) {
			case 'documento':
				return !$ci->trabajadorModel->documento_check($value);
				break;

			case 'numero_seguridad_social':
				return !$ci->trabajadorModel->numero_seguridad_social_check($value);
				break;

			case 'iban':
				return !$ci->trabajadorModel->iban_check($value);
				break;

			default:
				return false;
				break;
		}
	}

	protected function check_if_value_is_empty($value)
	{
		return empty($value) || $value == '' || $value == NULL;
	}

	public function getStateInfo()
	{
		$state_code = $this->getStateCode();

		$segment_object = $this->chain_get_state_info_from_url();

		$first_parameter = $segment_object->first_parameter;
		$second_parameter = $segment_object->second_parameter;
		$third_parameter = $segment_object->third_parameter;

		$state_info = (object)array();

		switch ($state_code) {
			case 101: //ajax_extension
				$state_info->target_field_name = $first_parameter;
				$state_info->relation_field_on_source_table = $second_parameter;
				$state_info->filter_value = $third_parameter;

				break;

			case 21:
			case 30: //export_zip

				$state_info = (object)array();
				$data = !empty($_POST) ? $_POST : $_GET;

				if(!empty($data['per_page']))
				{
					$state_info->per_page = is_numeric($data['per_page']) ? $data['per_page'] : null;
				}
				if(!empty($data['page']))
				{
					$state_info->page = is_numeric($data['page']) ? $data['page'] : null;
				}
				//If we request an export or a print we don't care about what page we are
				if($state_code === 16 || $state_code === 17)
				{
					$state_info->page = 1;
					$state_info->per_page = 1000000; //a very big number!
				}
				if(!empty($data['order_by'][0]))
				{
					$state_info->order_by = $data['order_by'];
				}
				if(!empty($data['search_text']))
				{
					if(empty($data['search_field']))
					{
						$search_text = strip_tags($data['search_field']);
						$state_info->search = (object)array('field' => null , 'text' => $data['search_text']);
					}
					else
					{
						if (is_array($data['search_field'])) {
							$search_array = array();
							foreach ($data['search_field'] as $search_key => $search_field_name) {
								$search_array[$search_field_name] = !empty($data['search_text'][$search_key]) ? $data['search_text'][$search_key] : '';
							}
							$state_info->search	= $search_array;
						} else {
							$state_info->search	= (object)array(
								'field' => strip_tags($data['search_field']) ,
								'text' => $data['search_text'] );
						}
					}
				}
				break;

			default:
				$state_info = parent::getStateInfo();

		}

		return $state_info;
	}

	protected function getStateCode()
	{
		$state_string = $this->get_state_info_from_url()->operation;

		if( $state_string != 'unknown' && in_array( $state_string, $this->states ) )
			$state_code =  array_search($state_string, $this->states);
		else
			$state_code = 0;

		return $state_code;
	}

	protected function get_state_info_from_url()
	{
		$ci = &get_instance();

		$segment_position = count($ci->uri->segments) + 1;
		$operation = 'list';

		$segements = $ci->uri->segments;
		foreach($segements as $num => $value)
		{
			if($value != 'unknown' && in_array($value, $this->states))
			{
				$segment_position = (int)$num;
				$operation = $value; //I don't have a "break" here because I want to ensure that is the LAST segment with name that is in the array.
			}
		}

		$function_name = $this->get_method_name();

		if($function_name == 'index' && !in_array('index',$ci->uri->segments))
			$segment_position++;

		$first_parameter = isset($segements[$segment_position+1]) ? $segements[$segment_position+1] : null;
		$second_parameter = isset($segements[$segment_position+2]) ? $segements[$segment_position+2] : null;

		return (object)array('segment_position' => $segment_position, 'operation' => $operation, 'first_parameter' => $first_parameter, 'second_parameter' => $second_parameter);
	}

	protected function set_ajax_list_queries($state_info = null)
	{
		$field_types = $this->get_field_types();

		if(!empty($state_info->per_page))
		{
			if(empty($state_info->page) || !is_numeric($state_info->page) )
				$this->limit($state_info->per_page);
			else
			{
				$limit_page = ( ($state_info->page-1) * $state_info->per_page );
				$this->limit($state_info->per_page, $limit_page);
			}
		}

		if(!empty($state_info->order_by))
		{
			$this->order_by($state_info->order_by[0],$state_info->order_by[1]);
		}

		if(!empty($state_info->search))
		{
			if (!empty($this->relation)) {
				foreach ($this->relation as $relation_name => $relation_values) {
					$temp_relation[$this->_unique_field_name($relation_name)] = $this->_get_field_names_to_search($relation_values);
				}
			}

			if (is_array($state_info->search)) {
				foreach ($state_info->search as $search_field => $search_text) {


					if (isset($temp_relation[$search_field])) {
						if (is_array($temp_relation[$search_field])) {
							$temp_where_query_array = [];

							foreach ($temp_relation[$search_field] as $relation_field) {
								$escaped_text = $this->basic_model->escape_str($search_text);
								$temp_where_query_array[] = $relation_field . ' LIKE \'%' . $escaped_text . '%\'';
							}
							if (!empty($temp_where_query_array)) {
								$this->where('(' . implode(' OR ', $temp_where_query_array) . ')', null);
							}

						} else {
							$this->like($temp_relation[$search_field] , $search_text);
						}
					} elseif(isset($this->relation_n_n[$search_field])) {
						$escaped_text = $this->basic_model->escape_str($search_text);
						$this->having($search_field." LIKE '%".$escaped_text."%'");
					} else {
						$this->like($search_field, $search_text);
					}



				}
			} elseif ($state_info->search->field !== null) {
				if (isset($temp_relation[$state_info->search->field])) {
					if (is_array($temp_relation[$state_info->search->field])) {
						foreach ($temp_relation[$state_info->search->field] as $search_field) {
							$this->or_like($search_field , $state_info->search->text);
						}
					} else {
						$this->like($temp_relation[$state_info->search->field] , $state_info->search->text);
					}
				} elseif(isset($this->relation_n_n[$state_info->search->field])) {
					$escaped_text = $this->basic_model->escape_str($state_info->search->text);
					$this->having($state_info->search->field." LIKE '%".$escaped_text."%'");
				} else {
					$this->like($state_info->search->field , $state_info->search->text);
				}
			}
			// Search all field
			else
			{
				$columns = $this->get_columns();

				$search_text = $state_info->search->text;

				if(!empty($this->where))
					foreach($this->where as $where)
						$this->basic_model->having($where[0],$where[1],$where[2]);

				$temp_where_query_array = [];
				$basic_table = $this->get_table();

				foreach($columns as $column)
				{
					if(isset($temp_relation[$column->field_name]))
					{
						if(is_array($temp_relation[$column->field_name]))
						{
							foreach($temp_relation[$column->field_name] as $search_field)
							{
								$escaped_text = $this->basic_model->escape_str($search_text);
								$temp_where_query_array[] = $search_field . ' LIKE \'%' . $escaped_text . '%\'';
							}
						}
						else
						{
							$escaped_text = $this->basic_model->escape_str($search_text);
							$temp_where_query_array[] = $temp_relation[$column->field_name] . ' LIKE \'%' . $escaped_text . '%\'';
						}
					}
					elseif(isset($this->relation_n_n[$column->field_name]))
					{
						//@todo have a where for the relation_n_n statement
					}
					elseif (
						isset($field_types[$column->field_name]) &&
						!in_array($field_types[$column->field_name]->type, array('date', 'datetime', 'timestamp'))
					) {
						$escaped_text = $this->basic_model->escape_str($search_text);
						$temp_where_query_array[] =  '`' . $basic_table . '`.' . $column->field_name . ' LIKE \'%' . $escaped_text . '%\'';
					}
				}

				if (!empty($temp_where_query_array)) {
					$this->where('(' . implode(' OR ', $temp_where_query_array) . ')', null);
				}
			}
		}
	}

	protected function ajax_extension($state_info)
	{
		if(!isset($this->relation[$state_info->target_field_name]))
			return false;

		list($field_name, $related_table, $related_field_title, $where_clause, $order_by)  = $this->relation[$state_info->target_field_name];

		$target_field_name = $state_info->target_field_name;

		$relation_field_on_source_table = $state_info->relation_field_on_source_table;

		$filter_value = $state_info->filter_value;

		if(is_int($filter_value))
		{
			$final_filter_value = $filter_value;
		}
		else
		{
			$decoded_filter_value = urldecode($filter_value);

			$replaced_filter_value = str_replace($this->slash_replacement,'/',$decoded_filter_value);

			if(strpos($replaced_filter_value,'/') !== false)
			{
				$final_filter_value = $this->_convert_date_to_sql_date($replaced_filter_value);
			}
			else
			{
				$final_filter_value = $replaced_filter_value;
			}
		}

		$target_field_relation = $this->relation[$target_field_name];

		$result = $this->get_dependency_relation_array($target_field_relation, $relation_field_on_source_table, $final_filter_value);

		return $result;
	}

	protected function get_dependency_relation_array($relation_info, $relation_key_field, $relation_key_value, $limit = null)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by)  = $relation_info;

		$where_clause = array($relation_key_field => $relation_key_value);
		if(empty($relation_key_value)){
			$relation_array = array();
		}else{
			$relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit);
		}
		return $relation_array;
	}



	public function unset_ajax_extension()
	{
		$this->unset_ajax_extension = true;
		return $this;
	}


	//Overriden with the purpose of adding a third parameter, currently not calling parent. It should be changed in future if changes are made to parent.
	protected function chain_get_state_info_from_url()
	{
		$ci = &get_instance();

		$segment_position = count($ci->uri->segments) + 1;
		$operation = 'list';

		$segements = $ci->uri->segments;
		foreach($segements as $num => $value)
		{
			if($value != 'unknown' && in_array($value, $this->states))
			{
				$segment_position = (int)$num;
				$operation = $value; //I don't have a "break" here because I want to ensure that is the LAST segment with name that is in the array.
			}
		}

		$function_name = $this->get_method_name();

		if($function_name == 'index' && !in_array('index',$ci->uri->segments))
			$segment_position++;

		$first_parameter = isset($segements[$segment_position+1]) ? $segements[$segment_position+1] : null;
		$second_parameter = isset($segements[$segment_position+2]) ? $segements[$segment_position+2] : null;
		$third_parameter = isset($segements[$segment_position+3]) ? $segements[$segment_position+3] : null;

		return (object)array('segment_position' => $segment_position, 'operation' => $operation, 'first_parameter' => $first_parameter, 'second_parameter' => $second_parameter, 'third_parameter' => $third_parameter);
	}
}

/**
 * PHP Grocery_CRUD_Multiuploader
 *
 *
 *
 * @package    	Grocery_CRUD_Multiuploader
 * @author     	Akshay Hegde <akshay.k.hegde@gmail.com>
 *              www.linkedin.com/profile/view?id=206267783
 *		https://github.com/Akshay-Hegde
 *
 * @version    	1.0.2 ( update callback fix (28-07-2015 13:17:00), function_renamed (11-08-2015 10:24:00 ) )
 * @copyright  	Copyright (c) 2010 through 2014, John Skoumbourdis
 * @license    	https://github.com/scoumbourdis/grocery-crud/blob/master/license-grocery-crud.txt
 *
 * Thanks
 * John Skoumbourdis, Amit Shah & Victor
 */
class Grocery_CRUD_Multiuploader extends grocery_CRUD_CHAINSELECT
{
	protected $callback_read_field	= array();

	protected $multi_upload_function ;

	/* multiupload css directory */
	protected $multiupload_css_path        = 'assets/grocery_crud_multiuploader/styles/';

	/* multiupload js directory */
	protected $multiupload_javascript_path = 'assets/grocery_crud_multiuploader/scripts/';

	/* Default upload directory */
	protected $path_to_directory           = 'assets/grocery_crud_multiuploader/GC_uploads';

	/* No file text on list/read state */
	protected $no_file_text                = '<p>Empty</p>';

	/* Anchor text */
	protected $enable_full_path            = true;

	/* download button on read state */
	protected $enable_download_button      = true;

	/* download button filetypes read state */
	protected $download_allowed            = null;


	/* Table where files saved */
	protected $file_table;

	/* Primary Key of table */
	protected $primary_key;

	/* upload Field */
	protected $upload_field;

	/* Allowed file types */
	protected $allowed_types      = 'gif|jpeg|jpg|png|pdf|doc';

	/* Show allowed types - edit state */
	protected $show_allowed_types = true;

	/* Upload options */
	protected $hash_fields = array(
		"upload_field",
		"allowed_types",
		"show_allowed_types",
		"path_to_directory",
		"no_file_text",
		"enable_full_path",
		"enable_download_button",
		"download_allowed"
	);

	/* Temp storage */
	protected $hash = array();

	public $basic_model, $ci;


	/*
     * Constructor - Initializes and references CI
    */
	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->ci->load->model('Grocery_crud_model');
		$this->basic_model = new Grocery_crud_model();
		log_message('debug', "Grocery_CRUD_Multiuploader Class Initialized.");
	}

	/*
    *  Nothing much changed added field info
    */
	protected function change_list($list,$types)
	{
		$primary_key    = $this->get_primary_key();
		$has_callbacks  = !empty($this->callback_column);
		$output_columns = $this->get_columns();

		foreach($list as $num_row => $row)
		{
			foreach($output_columns as $column)
			{
				$field_name 	= $column->field_name;
				$field_value 	= isset( $row->{"$column->field_name"} ) ? $row->{"$column->field_name"} : null;
				if( $has_callbacks && isset($this->callback_column[$field_name]) )
				{
					$field_info = isset($this->get_field_types()[$column->field_name])?$this->get_field_types()[$column->field_name]:null;

					$list[$num_row]->$field_name = call_user_func($this->callback_column[$field_name], $field_value, $row, $field_info);

				}
				elseif(isset($types[$field_name]))
					$list[$num_row]->$field_name = $this->change_list_value($types[$field_name] , $field_value);
				else
					$list[$num_row]->$field_name = $field_value;
			}
		}

		return $list;
	}

	/*
     * _is_image
     *
     * @access	public
     * @param	string
     * @return      boolean
     */
	function _is_image($name)
	{
		$imgs = array('.jpg','.png','.jpeg','.gif','.tiff');
		$inp  = array(substr($name, -4),substr($name, -5));
		return count(array_intersect($imgs, $inp)) > 0;
	}

	/*
     * _is_video
     *
     * @access	public
     * @param	string
     * @return      boolean
     */
	function _is_video($name)
	{
		$imgs = array('.mp4','.mov','.avi','.mpeg');
		$inp  = array(substr($name, -4),substr($name, -5));
		return count(array_intersect($imgs, $inp)) > 0;
	}


	/*
     * _reset - used for multiple upload field...
     *
     * @access	public
     * @param	string
     * @return      void
     */
	function _reset($field)
	{
		foreach($this->hash_fields as $f){
			$this->{"$f"} = $this->hash[$field][$f];
		}
	}


	/*
     * remote_file_exists
     *
     * @access	public
     * @param	string
     * @return      boolean
     */
	function remote_file_exists($url)
	{
		return(bool)preg_match( '~HTTP/1\.\d\s+200\s+OK~', @current(get_headers($url)) );
	}


	/*
     * segment_check
     *
     * @access	public
     * @param	string
     * @param	string
     * @return      void
     */
	function segment_check($fun, $field)
	{
		if($this->ci->input->post("field") && $this->ci->input->post("field") === $field)
		{
			$this->_reset($field);

			$seg = $this->ci->uri->segments;

			if(!empty($seg))
			{
				if(
					$seg[count($seg)-1] === $fun &&
					in_array($seg[count($seg)],array('uploade','delete_file'))
				)
				{
					switch ($seg[count($seg)])
					{
						case 'uploade':
							$this->multiupload_file();
							break;
						case 'delete_file':
							$this->delete_multi_file();
							break;
						default:
							break;
					}

					die();
				}
			}
		}
	}

	/*
     * Callback override fix for callback_before_(insert|update|delete)
     *
     * @access	protected
     * @return      void
     */

	protected function pre_render()
	{
		$this->_initialize_variables();
		$this->_initialize_helpers();
		$this->_load_language();
		$this->state_code = $this->getStateCode();

		if($this->basic_model === null)
			$this->set_default_Model();

		$this->set_basic_db_table($this->get_table());

		$this->_load_date_format();

		$this->_set_primary_keys_to_model();


		switch($this->state_code)
		{
			case 4 : // before delete
				$this->_delete_this_key($this->get_state_info_from_url()->first_parameter);
				break;

			case 5 : // before insert
				$_POST = $this->_set_files($_POST);
				break;
			case 6 :
				// before update
				$_POST = $this->_set_files($_POST,$this->get_state_info_from_url()->first_parameter);
				break;
		}
	}

	/*
     * set_callbacks
     *
     * @access	protected
     * @param	string
     * @return      void
     */
	protected function set_callbacks($file_field)
	{

		/* Callback on list state */
		$this->callback_column($file_field,array($this,'_file_url'));

		/* Callback on add state */
		$this->callback_add_field($file_field,array($this, 'add_upload_field'));

		/* Callback on edit state */
		$this->callback_edit_field($file_field,array($this, 'edit_upload_fied'));


		/* Callback on read state */
		// GC < 1.5 we have some issue...
		$version = explode(".",grocery_CRUD::VERSION);
		switch($version[1])
		{
			case   5:
				$this->callback_read_field($file_field, array($this,'view_upload_field'));
				break;
			default :
				if($this->getState() == "read")
				{
					$this->callback_field($file_field, array($this,'view_upload_field'));
				}else
				{
					$this->callback_read_field($file_field, array($this,'view_upload_field'));
				}
		}

	}



	/*
     * new_multi_upload
     *
     * @access	public
     * @param	upload-field
     * @param	obj/array
     * @return	void
     */
	public function new_multi_upload($field=null, $obj=null)
	{
		/* Whether field is null */
		if(is_null($field))
		{
			throw new Exception("field is mandotory");
			die();
		}

		$this->file_table   = $this->basic_db_table;
		$this->primary_key  = $this->primary_key = $this->basic_model->get_primary_key($this->file_table);
		$this->multi_upload_function = __FUNCTION__;
		$this->upload_field = $field;

		/* Whether field exists in table ? */
		if(!$this->basic_model->field_exists($this->upload_field,$this->file_table))
		{
			throw new Exception("field : ".$this->upload_field." Not Found in table ".$this->file_table);
			die();
		}

		/* Override default configuration */
		if (!is_null($obj) && is_array($obj) && !empty($obj) )
		{
			foreach ($obj as $k => $v)
			{
				if (property_exists($this,$k))
				{
					$this->{"$k"} = $v;
				}else
				{
					log_message('debug', "$k doesn't exists in Grocery_CRUD_Multiuploader.");
				}
			}
		}

		/* Upload directory exists ? */
		if(!file_exists($this->path_to_directory))
		{
			throw new Exception("Directory does not exist : ".$this->path_to_directory);
			die();
		}

		/* Upload directory has write permission ? */
		if(!is_writable($this->path_to_directory))
		{
			throw new Exception("Not writable : ".$this->path_to_directory."\n Current Permission : ".substr(sprintf('%o', fileperms($this->path_to_directory)), -4));
			die();
		}

		/* temp storage */
		foreach($this->hash_fields as $f){
			$this->hash[$field][$f] = $this->{"$f"};
		}

		/* Check whether req is to upload file / delete file */
		$this->segment_check(__FUNCTION__,$field);

		/* Initialize Callbacks */
		$this->set_callbacks($field);

		/* Set Scripts */
		$this->set_scripts();

	}


	/*
     * set_scripts
     *
     * @access	protected
     * @return      void
     */
	protected function set_scripts()
	{

		$css = array(
			'assets/grocery_crud/css/ui/simple/' . grocery_CRUD::JQUERY_UI_CSS,
			'assets/grocery_crud/css/jquery_plugins/file_upload/file-uploader.css',
			'assets/grocery_crud/css/jquery_plugins/file_upload/jquery.fileupload-ui.css',
			'assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css',
			$this->multiupload_css_path.'multi_uploader.css',
		);

		$js  = array(
			'assets/grocery_crud/js/' . grocery_CRUD::JQUERY,
			'assets/grocery_crud/js/jquery_plugins/ui/' . grocery_CRUD::JQUERY_UI_JS,
			'assets/grocery_crud/js/jquery_plugins/tmpl.min.js',
			'assets/grocery_crud/js/jquery_plugins/load-image.min.js',
			'assets/grocery_crud/js/jquery_plugins/jquery.iframe-transport.js',
			'assets/grocery_crud/js/jquery_plugins/jquery.fileupload.js',
			'assets/grocery_crud/js/jquery_plugins/config/jquery.fileupload.config.js',
			'assets/grocery_crud/js/jquery_plugins/jquery.fancybox.pack.js',
			'assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js',
			'assets/grocery_crud/js/jquery_plugins/config/jquery.fancybox.config.js',
			$this->multiupload_javascript_path.'jquery.mousewheel.js',
		);

		foreach($css as $c){
			$this->set_css($c);
		}

		foreach($js as $c){
			$this->set_js($c);
		}
	}


	/*
     * add_upload_field callback
     *
     * @access	public
     * @return      string
     */
	function add_upload_field()
	{
		$args = func_get_args();
		$name = $args[2]->name;

		$this->_reset( $name );


		$html = '<div>
			<span class="fileinput-button qq-upload-button" id="' . $this->upload_field . '_upload-button-svc">
				<span>Upload a file</span>
				<input type="file" name="' . $this->upload_field . '_new_multi_upload" id="' . $this->upload_field . '_new_multi_upload_field" >
			</span>
                       '.( $this->show_allowed_types ? '<span class="allowed_types '.$this->upload_field.'_allowed_types">'.str_replace('|',',',$this->allowed_types).'</span>' : null ) .'
                        <span class="qq-upload-spinner" id="ajax-loader-file" style="display:none;"></span>
			<span id="' . $this->upload_field . '_progress-multiple" style="display:none;"></span>
		</div>
		<select name="' . $this->upload_field . '_files[]" multiple="multiple" size="8" class="multiselect" id="' . $this->upload_field . '_multiple_select" style="display:none;">
		</select>
		<div id="' . $this->upload_field . '_list_svc" class="mutiupload_list" style="margin-top: 40px;">
		</div>';

		$html.=$this->JS( $name  );

		return $html;
	}


	function edit_upload_fied($value, $primary_key)
	{
		$args = func_get_args();
		$name = $args[2]->name;


		$this->_reset( $name );

		$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
		$result = $result->result_array();

		$files = array();
		if(!empty($result) && $result[0][$this->upload_field])
		{
			$files = unserialize($result[0][$this->upload_field]);
		}

		$html = '<div>
			 <span class="fileinput-button qq-upload-button" 
			         id="' . $this->upload_field . '_upload-button-svc">
			 <span>Upload a file</span>
			 <input type="file" 
				name="' . $this->upload_field . '_new_multi_upload" 
				id="' . $this->upload_field . '_new_multi_upload_field" >
			 </span>
                         '.( $this->show_allowed_types ? '<span class="allowed_types '.$this->upload_field.'_allowed_types">'.str_replace('|',',',$this->allowed_types).'</span>' : null ) .'
			 <span class="qq-upload-spinner" 
				  id="ajax-loader-file" 
			       style="display:none;"></span>
			 <span    id="' . $this->upload_field . '_progress-multiple" 	
			       style="display:none;"></span>
			</div>';

		$html.= '<select 
				name="' . $this->upload_field . '_files[]" 
			    multiple="multiple" 
			        size="8" 
			       class="multiselect" 
				  id="' . $this->upload_field . '_multiple_select" 
                               style="display:none;">';

		if (!empty($files))
		{
			foreach ($files as $items)
			{
				$html.="<option value=" . $items . " selected='selected'>" . $items . "</option>";
			}
		}
		$html.='</select>';
		$html.='<div id="' . $this->upload_field . '_list_svc" 
			     class="mutiupload_list" 
			     style="margin-top: 40px;">';

		if (!empty($files))
		{
			foreach ($files as $items)
			{
				$thisfile = base_url() . $this->path_to_directory . $items ;
				if($this->remote_file_exists($thisfile))
				{

					if( strpos ($items,"." ) !== false )
					{

						if ($this->_is_image($items) === true)
						{
							$html.= '<div id="' . $items . '">';
							$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
							$html.='<img src="' . $thisfile . '" height="50"/>';
							$html.='</a><br>';
							$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
							$html.='</div>';
						} elseif ($this->_is_video($items) === true){
							// $html.= '<div id="' . $items . '">';
							// AGREGADO

							$html.= '<div id="' . $items . '" class="upload-success-url">';
							$html.= '<video width="320" height="240" controls>';
							//$html.= '<source src="'.base_url().'assets/uploads/videobooks_trabajadores/'.$items.'" type="video/mp4">';
							$html.= '<source src="'.$thisfile.'" type="video/mp4">';
							$html.= '<source src="'.$thisfile.'" type="video/ogg">';
							$html.= 'Your browser does not support the video tag.';
							$html.= '</video>';
							$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
							$html.='</a><br>';
							$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
							$html.='</div>';
						}
						else
						{
							$html.='<div id="' . $items . '" >
					<span><a href="'.$thisfile.'" target="_blank">' . $items . '</a></span>
					<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>
					</div>';
						}

					}

				}
			}
		}
		$html.='</div>';
		$html.=$this->JS($name);
		return $html;
	}



	/*
    * JS
    * @param	upload-field
    * @access	protected
    * @return      string
    */
	protected function JS($field=null)
	{

		if(is_null($field))return $field;

		$js = "
 		if (typeof string_progress === 'undefined') {
 			var string_upload_file 	= 'Subir archivos';
			var string_delete_file 	= 'Borrar';
			var string_progress 			= 'Cargando: ';
			var error_on_uploading 			= 'Ocurrio un errro al subir el archivo.';
			var message_prompt_delete_file 	= 'Esta seguro que quiere borrar el archivo?';

			var error_max_number_of_files 	= 'Solamente se puede subir de a un archivo.';
			var error_accept_file_types 	= 'No tienes permitido subir ese tipo de archivo.';
			var error_max_file_size 		= 'El archivo excede los 5000MB.';
			var error_min_file_size 		= 'No es posible subir archivos vacios.';

 		}	
	 	function delete_" . $this->upload_field . "_svc(link,filename)
	 	{
	 		$('#" . $this->upload_field . "_multiple_select option[value=\"'+filename+'\"]').remove();
	 		link.parent().remove();
	 		$.post('" . $this->multi_upload_function . "/delete_file', {'file_name':filename,'".$this->ci->security->get_csrf_token_name()."':'".$this->ci->security->get_csrf_hash()."','field':'".$field."','state':window.location.href.substring(window.location.href.lastIndexOf('/') + 1)}, function(json){
	 			if(json.succes == 'true')
	 			{
	 				console.log('json data', json);
	 			}
	 		}, 'json');
}

$(document).ready(function() {
	$('#" . $this->upload_field . "_new_multi_upload_field').fileupload({
		url: '" . $this->multi_upload_function . "/uploade',
		sequentialUploads: true,
		formData:{'".$this->ci->security->get_csrf_token_name()."':'".$this->ci->security->get_csrf_hash()."','field':'".$field."'},
		cache: false,
		autoUpload: true,
		dataType: 'json',
		acceptFileTypes: /(\.|\/)(" . $this->ci->config->item('grocery_crud_file_upload_allow_file_types') . ")$/i,
		limitMultiFileUploads: 1,
		beforeSend: function()
		{
			$('#" . $this->upload_field . "_upload-button-svc').slideUp('fast');
			$('#ajax-loader-file').css('display','block');
			$('#" . $this->upload_field . "_progress-multiple').css('display','block');
		},
		progress: function (e, data) {
			$('#" . $this->upload_field . "_progress-multiple').html(string_progress + parseInt(data.loaded / data.total * 100, 10) + '%');
		},
		done: function (e, data)
		{
			/*console.log(data.result);*/
			if(data.result.success == 'false') {
				alert(data.result.error);
				$('#" . $this->upload_field . "_upload-button-svc').show('fast');
				$('#ajax-loader-file').css('display','none');
				$('#" . $this->upload_field . "_progress-multiple').css('display','none');
				$('#" . $this->upload_field . "_progress-multiple').html('');
				return;
			}
			$('#" . $this->upload_field . "_multiple_select').append('<option value=\"'+data.result.file_name+'\" selected=\"selected\">'+data.result.file_name+'</select>');
			var is_image = (data.result.file_name.substr(-4) == '.jpg'
				|| data.result.file_name.substr(-4) == '.png'
				|| data.result.file_name.substr(-5) == '.jpeg'
				|| data.result.file_name.substr(-4) == '.gif'
				|| data.result.file_name.substr(-5) == '.tiff')
				? true : false;
				var html;
				if(is_image==true)
				{
					html='<div id=\"'+data.result.file_name+'\" class=\"'+col-md-12+'\" ><a href=\"" . base_url() . $this->path_to_directory . "'+data.result.file_name+'\" class=\"image-thumbnail\" id=\"fancy_'+data.result.file_name+'\">';
					html+='<img src=\"" . base_url() . $this->path_to_directory . "'+data.result.file_name+'\" height=\"50\"/>';
					html+='</a><br><a href=\"javascript:;\" onclick=\"delete_" . $this->upload_field . "_svc($(this),\''+data.result.file_name+'\')\" style=\"color:red;\" >Delete</a></div>';
					$('#" . $this->upload_field . "_list_svc').append(html);
					$('.image-thumbnail').fancybox({
						'closeBtn'   : true,
						'transitionIn' : 'elastic',
						'transitionOut' : 'elastic',
						'speedIn' : 600,
						'speedOut' : 200,
						'overlayShow' : true
					});
				}
				else
				{
					html = '<div id=\"'+data.result.file_name+'\" class=\"'+col-md-12+'\" ><span>'+data.result.file_name+'</span> <br><a href=\"javascript:\" onclick=\"delete_" . $this->upload_field . "_svc($(this),\''+data.result.file_name+'\')\" style=\"color:red;\" >Delete</a></div>';
					$('#" . $this->upload_field . "_list_svc').append(html);
				}
					$('#" . $this->upload_field . "_upload-button-svc').show('fast');
					$('#ajax-loader-file').css('display','none');
					$('#" . $this->upload_field . "_progress-multiple').css('display','none');
					$('#" . $this->upload_field . "_progress-multiple').html('');
				}
			});

		});
";


		$js = "<script>\n".$js."\n</script>";

		return $js;
	}

	/*
    * _create_unique_filename
    * @param	string
    * @access	protected
    * @return      string
    */
	protected function _create_unique_filename($filename)
	{
		return sprintf(
			"%s_%s.%s",
			pathinfo($filename, PATHINFO_FILENAME),
			date('YmdHis'),
			pathinfo($filename, PATHINFO_EXTENSION)
		);
	}

	/*
    * upload_file
    * @access	public
    * @return      json string
    */
	function multiupload_file($state=null)
	{

		$json                    = array();
		$config['upload_path']   = $this->path_to_directory;
		$config['allowed_types'] = $this->allowed_types;
		$config['remove_spaces'] = TRUE;
		$config['max_filename']  = 0;
		$json['error']   = 'Failed';
		$json['success'] = 'false';

		if(array_key_exists($this->upload_field . '_new_multi_upload',$_FILES))
		{
			$config['file_name'] = $this->_create_unique_filename(
				$_FILES[$this->upload_field . '_new_multi_upload']['name']
			);
		}

		/* Mad issue with CI */
		/* http://stackoverflow.com/questions/8664758/the-upload-path-does-not-appear-to-be-valid-codeigniter-file-upload-not-worki */
		$this->ci->load->library('upload');
		$this->ci->upload->initialize($config);

		if (!$this->ci->upload->do_upload($this->upload_field . '_new_multi_upload'))
		{

			$error = preg_replace("/<p[^>]*?>|<\/p>/", "", $this->ci->upload->display_errors());
			$json['error'] =  $error. " Archivos permitidos " . $this->allowed_types ;
			$json['success'] = 'false';
		}
		else
		{
			$uploade_data = $this->ci->upload->data();
			$json['success'] = 'true';
			$json['file_name'] = $uploade_data['file_name'];
			unset($json['error']);
		}

		echo json_encode($json);
	}


	/*
    * view_upload_field callback
    * @access	public
    * @return      string
    */
	function view_upload_field($value, $primary_key)
	{

		$args = func_get_args();
		$name = $args[2]->name;


		$this->_reset( $name );

		$any_file = 0;


		$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
		$result = $result->result_array();

		if(!empty($result) && $result[0][$this->upload_field])
		{
			$files = unserialize($result[0][$this->upload_field]);
		}
		else
		{
			return $this->no_file_text;
		}


		$html = '<select name="' . $this->upload_field . '_files[]" multiple="multiple" size="8" class="multiselect" id="' . $this->upload_field . '_multiple_select" style="display:none;">';
		if (!empty($files))
		{
			foreach ($files as $items)
			{
				$html.="<option value=" . $items . " selected='selected'>" . $items . "</option>";
			}
		}
		$html.='</select>';
		$html.='<div id="' . $this->upload_field . '_list_svc" class="mutiupload_list" style="margin-top: 40px;">';
		if (!empty($files))
		{

			$html .= "<table>";

			foreach ($files as $items)
			{

				$thisfile = base_url() . $this->path_to_directory . $items;

				if($this->remote_file_exists($thisfile))
				{
					$html .="<tr>";	$any_file = 1;

					if ($this->_is_image($items) === true)
					{
						$html.= '<td><div id="' . $items . '">';
						$html.= '<a href="' . $thisfile . '" class="image-thumbnail" id="fancy_' . $items . '">';
						$html.='<img src="' .$thisfile. '" height="50"/>';
						$html.='</a>';
						$html.='</div></td>';
					}
					elseif ($this->_is_video($items) === true){
						// $html.= '<div id="' . $items . '">';
						// AGREGADO

						$html.= '<div id="' . $items . '" class="upload-success-url">';
						$html.= '<video width="320" height="240" controls>';
						//$html.= '<source src="'.base_url().'assets/uploads/videobooks_trabajadores/'.$items.'" type="video/mp4">';
						$html.= '<source src="'.$thisfile.'" type="video/mp4">';
						$html.= '<source src="'.$thisfile.'" type="video/ogg">';
						$html.= 'Your browser does not support the video tag.';
						$html.= '</video>';
						$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
						$html.='</a><br>';
						$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
						$html.='</div>';
					}
					else
					{
						$html.='<div id="' . $items . '" >
					<span><a href="'.$thisfile.'" target="_blank">' . $items . '</a></span>
					<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>
					</div>';
					}

					if( strpos ($items,"." ) !== false )
					{
						if(
							(
								$this->enable_download_button &&
								is_null($this->download_allowed)
							) ||
							(
								$this->enable_download_button &&
								in_array(
									pathinfo(
										$thisfile, PATHINFO_EXTENSION
									),
									explode(
										"|",$this->download_allowed
									)
								)
							)
						)
						{
							$html.='<td><a href="'.$thisfile.'">Download</a></td>';
						}
					}

					$html .= "</tr>";

				}

			}
			$html .= "</table>";
		}
		$html.='</div>';

		if(!$any_file){ $html = $this->no_file_text; }

		return $html;
	}



	/*
    * _file_url callback
    * @access	public
    * @return      string
    */
	function _file_url($value,$row)
	{

		$args = func_get_args();
		$name = $args[2]->name;


		$this->_reset( $name );

		$files = '';
		if($value){
			$data = @unserialize($value);
			if ($data !== false) {
				$files = $data;
			} else {
				$files = [];
			}
		}

		$any_file = 0;

		if(!empty($files))
		{
			$html = "";
			foreach($files as $items)
			{

				// No extension don't consider
				if (  strpos ($items,"." ) === false )
				{
					continue;
				}

				$thisfile = base_url() . $this->path_to_directory . $items;

				if($this->remote_file_exists($thisfile))
				{
					$any_file = 1;
					$html .= strlen($html)? " " : "";
					$html .= "<span><a href='";

					if  (  	(
							$this->enable_download_button &&
							is_null($this->download_allowed)
						) ||
						(
							$this->enable_download_button &&
							in_array(
								pathinfo(
									$thisfile, PATHINFO_EXTENSION
								),
								explode(
									"|",$this->download_allowed
								)
							)
						)

					){ $html .= $thisfile; } else { $html .= "#"; }

					$html .="'";

					if($this->_is_image($items) === true) {
						$html .= ' class="image-thumbnail"';
					}

					$html .=">";
					if($this->_is_image($items) === true) {
						$html.='<img src="' .$thisfile. '" height="50"/>';
					} else {
						$html.= ( $this->enable_full_path ? $thisfile : $items );
					}
					$html.= "</a></span><br>";
				}
			}

		}
		if(!$any_file){ $html = $this->no_file_text; }

		return $html;
	}


	/*
    * _remove_files
    * @access	protected
        * @param	string
        * @param	array
    * @return      void
    */
	protected function _remove_files($path=null, $files=array())
	{
		if(!is_null($path) && !empty($files))
		{
			foreach($files as $file_name)
			{
				if (file_exists($path . $file_name))
				{
					unlink($path . $file_name);
				}
			}
		}
	}

	/*
    * _set_files callback
    * @access	public
        * @param	array
        * @param	int
    * @return      array
    */
	function _set_files($post_array, $primary_key=null)
	{

		foreach(array_keys($this->hash) as $key)
		{
			$this->upload_field = $key;

			$this->_reset($key);

			$files_to_delete = array();


			$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
			$result = $result->result_array();

			$files_exists = array();
			if(!empty($result) && $result[0][$this->upload_field])
			{
				$files_exists = unserialize($result[0][$this->upload_field]);
			}

			if(array_key_exists($this->upload_field.'_files',$post_array))
			{

				$files = $post_array[$this->upload_field . '_files'];
				unset($post_array[$this->upload_field . '_files']);

				foreach($files_exists as $file_name)
				{
					if(!in_array($file_name, $files))
					{
						array_push($files_to_delete,$file_name);
					}
				}

				if (!empty($files))
				{
					$post_array[$this->upload_field]   = serialize($files);
				}
			}
			else
			{
				$files_to_delete = $files_exists;
				$post_array[$this->upload_field]   = serialize(array());
			}

			$this->_remove_files($this->path_to_directory, $files_to_delete);
		}

		return $post_array;
	}

	/*
    * _delete_this_key callback
    * @access	public
        * @param	int
    * @return      void
    */
	function _delete_this_key($primary_key)
	{
		foreach(array_keys($this->hash) as $key)
		{

			$this->_reset($key);

			$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
			$result = $result->result_array();

			$files = array();
			if(!empty($result) && $result[0][$key])
			{
				$files = unserialize($result[0][$key]);
			}

			$this->_remove_files($this->path_to_directory, $files);
		}
	}

	/*
    * delete_file
    * @access	public
        * @param	post array
    * @return      json string
    */
	function delete_multi_file($state=null)
	{

		$state     = $this->ci->input->post("state");
		$field     = $this->ci->input->post("field");
		$file_name = $this->ci->input->post("file_name");

		// Just send msg success will delete files inside _set_files, if state is
		// other than add
		// only After update
		if($state == "add")
		{
			$this->_reset($field);
			$this->_remove_files($this->path_to_directory,array($file_name));
		}
		echo json_encode(array('success' => true));
		die();

	}

}
/* END OF Grocery_CRUD_Multiuploader */



class Grocery_CRUD extends Grocery_CRUD_Multiuploader
{
	// AGREGADO
	protected $filtro_fecha_relations = [];
	protected $relation_1_1 = [];
	protected $unset_goTo = true;
	protected $goTo_array = [];

	// AGREGADO
	public function get_filtro_fecha_relations()//
	{
		$this->relation_1_1[$field_name] = array($field_name, $related_table, $related_title_fields);
		return $this;
	}

	public function set_relation_1_1($field_name, $related_table, $related_title_fields)
	{
		$this->relation_1_1[$field_name] = array($field_name, $related_table, $related_title_fields);
		return $this;
	}

	protected function get_list()
	{

		// AGREGADO
		if(!empty($this->in_where))
			foreach($this->in_where as $in_where){
				$this->basic_model->where($in_where[0].' in '.$in_where[1]);

			}
		// print_r($in_where[1]);
		// die;

		if(!empty($this->order_by))
			$this->basic_model->order_by($this->order_by[0],$this->order_by[1]);

		if(!empty($this->where))
			foreach($this->where as $where)
				$this->basic_model->where($where[0],$where[1],$where[2]);

		if(!empty($this->or_where))
			foreach($this->or_where as $or_where)
				$this->basic_model->or_where($or_where[0],$or_where[1],$or_where[2]);

		if(!empty($this->like))
			foreach($this->like as $like)
				$this->basic_model->like($like[0],$like[1],$like[2]);

		if(!empty($this->or_like))
			foreach($this->or_like as $or_like)
				$this->basic_model->or_like($or_like[0],$or_like[1],$or_like[2]);

		if(!empty($this->having))
			foreach($this->having as $having)
				$this->basic_model->having($having[0],$having[1],$having[2]);

		if(!empty($this->or_having))
			foreach($this->or_having as $or_having)
				$this->basic_model->or_having($or_having[0],$or_having[1],$or_having[2]);

		if(!empty($this->relation))
			foreach($this->relation as $relation)
				$this->basic_model->join_relation($relation[0],$relation[1],$relation[2],$relation[3],$relation[4],$relation[5],$relation[6]);// AGREGADOS PARÁMETROS

		// AGREGADO
		if(!empty($this->relation_1_1))
			foreach($this->relation_1_1 as $relation)
				$this->basic_model->join_relation_1_1($relation[0],$relation[1],$relation[2]);

		// AGREGADO
		if(!empty($this->filtro_fecha_relations)){
			if(isset($this->filtro_fecha_relations['relations'])){

				foreach($this->filtro_fecha_relations['relations'] as $relation){
					$this->basic_model->db->join($relation[0],$relation[1]);
				}

			}

			if(isset($this->filtro_fecha_relations['group_by'])){
				foreach($this->filtro_fecha_relations['group_by'] as $relation){
					$this->basic_model->db->group_by($relation);
				}
			}
		}

		if(!empty($this->relation_n_n))
		{
			$columns_load = array();

			$columns = $this->get_columns();
			foreach($columns as $column)
			{
				//Use the relation_n_n ONLY if the column is called . The set_relation_n_n are slow and it will make the table slower without any reason as we don't need those queries.
				if(!in_array($column->field_name, $columns_load) && isset($this->relation_n_n[$column->field_name]))
				{
					$this->basic_model->set_relation_n_n_field($this->relation_n_n[$column->field_name]);
					$columns_load[] = $column->field_name;
				}
			}

			if(isset($this->filters))
			{
				foreach($this->filters as $filter)
				{
					if(!in_array($filter, $columns_load) && isset($this->relation_n_n[$filter]))
					{
						$this->basic_model->set_relation_n_n_field($this->relation_n_n[$filter]);
						$columns_load[] = $filter;
					}
				}
			}

			if(isset($this->filters_adv))
			{
				foreach($this->filters_adv as $filter)
				{
					if(!in_array($filter, $columns_load) && isset($this->relation_n_n[$filter]))
					{
						$this->basic_model->set_relation_n_n_field($this->relation_n_n[$filter]);
						$columns_load[] = $filter;
					}
				}
			}



		}

		if ( isset($this->theme_config['crud_paging']) ) {
			if($this->theme_config['crud_paging'] === true)
			{
				if($this->limit === null)
				{
					$default_per_page = $this->config->default_per_page;
					if(is_numeric($default_per_page) && $default_per_page >1)
					{
						$this->basic_model->limit($default_per_page);
					}
					else
					{
						$this->basic_model->limit(10);
					}
				}
				else
				{
					$this->basic_model->limit($this->limit[0],$this->limit[1]);
				}
			}
		}

		$results = $this->basic_model->get_list();

		if($this->getState() == 'list' || $this->getState() == 'ajax_list' && $this->subject == 'Trabajadores') {
			$this->ci->session->set_userdata('trabajadores_list_result_query', $this->basic_model->db->last_query());
		}

		if($this->getState() == 'list' || $this->getState() == 'ajax_list' && $this->subject == 'Gastos') {
			$this->ci->session->set_userdata('zip_informes_gastos', $this->basic_model->db->last_query());
		}

		// AGREGADO PARA QUE FUNCIONEN LOS FILTROS EN EL EXPORTAR TRABAJADORES DEL EXCEL
		if($this->getState() == 'export' && $this->subject == 'Trabajadores') {
			$lastQuery = $this->ci->session->userdata('trabajadores_list_result_query');
			$lastQuery = str_replace('LIMIT 10', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 25', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 50', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 100', '', $lastQuery);
			$results = $this->basic_model->db->query(
				$lastQuery
			)->result();
		}

		// AGREGADO PARA QUE FUNCIONEN LOS FILTROS EN EL EXPORTAR EL ZIP DE INFORMES DE GASTOS
		if($this->getState() == 'export_zip' && $this->subject == 'Gastos') {
			$lastQuery = $this->ci->session->userdata('zip_informes_gastos');
			$lastQuery = str_replace('LIMIT 10', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 25', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 50', '', $lastQuery);
			$lastQuery = str_replace('LIMIT 100', '', $lastQuery);
			$results = $this->basic_model->db->query(
				$lastQuery
			)->result();
		}
		return $results;
	}

	protected function get_total_results()
	{
		// AGREGADO
		// if(!empty($this->in_where))
		// foreach($this->in_where as $in_where)
		//     $this->basic_model->where($in_where[0].' in '.$in_where[1]);
		if(!empty($this->in_where))
			foreach($this->in_where as $in_where){
				$this->basic_model->where($in_where[0].' in '.$in_where[1]);

			}
		if(!empty($this->where))
			foreach($this->where as $where)
				$this->basic_model->where($where[0],$where[1],$where[2]);

		if(!empty($this->or_where))
			foreach($this->or_where as $or_where)
				$this->basic_model->or_where($or_where[0],$or_where[1],$or_where[2]);

		if(!empty($this->like))
			foreach($this->like as $like)
				$this->basic_model->like($like[0],$like[1],$like[2]);

		if(!empty($this->or_like))
			foreach($this->or_like as $or_like)
				$this->basic_model->or_like($or_like[0],$or_like[1],$or_like[2]);

		if(!empty($this->having))
			foreach($this->having as $having)
				$this->basic_model->having($having[0],$having[1],$having[2]);

		if(!empty($this->or_having))
			foreach($this->or_having as $or_having)
				$this->basic_model->or_having($or_having[0],$or_having[1],$or_having[2]);

		if(!empty($this->relation))
			foreach($this->relation as $relation)
				$this->basic_model->join_relation($relation[0],$relation[1],$relation[2],$relation[3],$relation[4],$relation[5],$relation[6]);


		if(!empty($this->relation_n_n))
		{
			$columns = $this->get_columns();
			foreach($columns as $column)
			{
				//Use the relation_n_n ONLY if the column is called . The set_relation_n_n are slow and it will make the table slower without any reason as we don't need those queries.
				if(isset($this->relation_n_n[$column->field_name]))
				{
					$this->basic_model->set_relation_n_n_field($this->relation_n_n[$column->field_name]);
				}
			}

		}
		//AGREGADO
		if(!empty($this->filtro_fecha_relations)){
			if(isset($this->filtro_fecha_relations['relations'])){

				foreach($this->filtro_fecha_relations['relations'] as $relation){
					$this->basic_model->db->join($relation[0],$relation[1]);
				}

			}

			if(isset($this->filtro_fecha_relations['group_by'])){
				foreach($this->filtro_fecha_relations['group_by'] as $relation){
					$this->basic_model->db->group_by($relation);
				}
			}
		}
		return $this->basic_model->get_total_results();
	}

	protected function get_relation_total_rows($relation_info)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by, $related_id_field, $config)  = $relation_info;// AGREGADO PARÁMETRO

		$relation_array = $this->basic_model->get_relation_total_rows($field_name , $related_table , $related_field_title, $where_clause, $config);

		return $relation_array;
	}

	protected function get_edit_values($primary_key_value)
	{
		$values = $this->basic_model->get_edit_values($primary_key_value);

		if(!empty($this->relation_n_n))
		{
			foreach($this->relation_n_n as $field_name => $field_info)
			{
				$values->$field_name = $this->get_relation_n_n_selection_array($primary_key_value, $field_info);
			}
		}

		// AGREGADO
		if(!empty($this->relation_1_1))
		{
			foreach($this->relation_1_1 as $relation)
			{
				list($field_name , $related_table , $related_field_titles) = $relation;

				$data = $this->get_relation_1_1_selection_array($primary_key_value, $field_name, $related_table, $related_field_titles);

				foreach($related_field_titles as $related_field_title)
				{
					$values->$related_field_title = isset($data->$related_field_title)?$data->$related_field_title:'';
				}
			}
		}

		return $values;
	}

	// AGREGADO
	protected function get_relation_1_1_selection_array($primary_key_value, $field_name, $related_table, $related_field_titles)
	{
		return $this->basic_model->get_relation_1_1_selection_array($primary_key_value, $field_name, $related_table, $related_field_titles);
	}

	protected function showList($ajax = false, $state_info = null)
	{
		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;

		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();
		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->dialog_forms = $this->config->dialog_forms;
		$data->columns 				= $this->get_columns();
		// AGREGADO
		if ($this->relation_n_n) {
			foreach ($this->relation_n_n as $field_name => $configuration) {
				if (
					isset($configuration->config['show_grouped_totals_in_list']['fields_to_sum']) &&
					isset($configuration->config['show_grouped_totals_in_list']['ids_selection_table']) &&
					$configuration->config['show_grouped_totals_in_list']['fields_to_sum'] &&
					$configuration->config['show_grouped_totals_in_list']['ids_selection_table']
				) {
					// eliminar columna original
					$display_as = '';

					foreach ($data->columns as $key => $column) {
						if ($column->field_name == $field_name) {
							$display_as = $column->display_as;
							unset($data->columns[$key]);
							break;
						}
					}

					// agregar columnas nuevas
					foreach ($configuration->config['show_grouped_totals_in_list']['ids_selection_table'] as $id) {
						$column = new stdclass();
						$column->field_name = $field_name.'_'.$id['id'];
						$column->display_as = ($display_as ? $display_as.' ' : '').$id['nombre'];

						$data->columns[] = $column;
					}
				}


			}
		}

		$data->success_message		= $this->get_success_message_at_list($state_info);

		$data->primary_key 			= $this->get_primary_key();
		$data->add_url				= $this->getAddUrl();
		$data->edit_url				= $this->getEditUrl();
		$data->clone_url			= $this->getCloneUrl();
		$data->delete_url			= $this->getDeleteUrl();
		$data->delete_multiple_url	= $this->getDeleteMultipleUrl();
		$data->read_url				= $this->getReadUrl();
		$data->ajax_list_url		= $this->getAjaxListUrl();
		$data->ajax_list_info_url	= $this->getAjaxListInfoUrl();
		$data->export_url			= $this->getExportToExcelUrl();
		$data->print_url			= $this->getPrintUrl();
		$data->actions				= $this->actions;
		$data->unique_hash			= $this->get_method_hash();
		$data->order_by				= $this->order_by;

		$data->unset_add			= $this->unset_add;
		$data->unset_edit			= $this->unset_edit;
		$data->unset_clone			= $this->unset_clone;
		$data->unset_read			= $this->unset_read;
		$data->unset_delete			= $this->unset_delete;
		$data->unset_export			= $this->unset_export;
		$data->unset_print			= $this->unset_print;
		// AGREGADO
		$data->unset_goTo			= $this->unset_goTo;
		$data->goTo_array		    = $this->goTo_array;

		$data->filters		    	= $this->get_filters();
		$data->filters_adv	    	= $this->get_filters_adv();

		$default_per_page = $this->config->default_per_page;
		$data->paging_options = $this->config->paging_options;
		$data->default_per_page		= is_numeric($default_per_page) && $default_per_page >1 && in_array($default_per_page,$data->paging_options)? $default_per_page : 25;

		if($data->list === false)
		{
			throw new Exception('It is impossible to get data. Please check your model and try again.', 13);
			$data->list = array();
		}

		foreach($data->list as $num_row => $row)
		{
			$data->list[$num_row]->primary_key_value = $row->{$data->primary_key};
			$data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$data->primary_key};
			$data->list[$num_row]->clone_url = $data->clone_url.'/'.$row->{$data->primary_key};
		}

		if(!$ajax)
		{
			$this->_add_js_vars(array('dialog_forms' => $this->config->dialog_forms));

			$data->list_view = $this->_theme_view('list.php',$data,true);
			$this->_theme_view('list_template.php',$data);
		}
		else
		{
			$this->set_echo_and_die();
			$this->_theme_view('list.php',$data);
		}

		if (!empty($this->upload_fields)) {
			$this->load_js_fancybox();
		}

		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

	}

	protected function exportToExcel($state_info = null)
	{
		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;
		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();

		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->columns 				= $this->get_columns();
		$data->primary_key 			= $this->get_primary_key();

		@ob_end_clean();
		$this->_export_to_excel($data);
	}

	// AGREGADO
	public function set_goTo($goTo_array_in)
	{
		$this->unset_goTo = false;
		$this->goTo_array = $goTo_array_in;
	}

	public function set_relation($field_name , $related_table, $related_title_field, $where_clause = null, $order_by = null, $related_id_field = null, $config = null)// AGREGADO PARÁMETRO
	{
		$show = true;
		$state_code = $this->getStateCode();
		if($state_code == 1 || $state_code == 7) // si es list armo una comun para que se arme filtro correctamente
		{
		}
		else
		{
			if(isset($config['only_list']) && $config['only_list']) $show = false;
		}

		if($show)
		{
			$this->relation[$field_name] = array($field_name, $related_table, $related_title_field, $where_clause, $order_by, $related_id_field, $config);// AGREGADO PARÁMETRO
		}
		return $this;
	}

	public function set_relation_n_n($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table, $primary_key_alias_to_selection_table , $title_field_selection_table , $priority_field_relation_table = null, $where_clause = null, $id_field_selection_table = null, $config = null)// AGREGADOS PARÁMETROS
	{
		$show = true;
		$state_code = $this->getStateCode();
		if($state_code == 1 || $state_code == 7) // si es list armo una comun para que se arme filtro correctamente
		{
		}
		else
		{
			if(isset($config['only_list']) && $config['only_list']) $show = false;
		}

		if($show)
		{
			$this->relation_n_n[$field_name] =
				(object)array(
					'field_name' => $field_name,
					'relation_table' => $relation_table,
					'selection_table' => $selection_table,
					'primary_key_alias_to_this_table' => $primary_key_alias_to_this_table,
					'primary_key_alias_to_selection_table' => $primary_key_alias_to_selection_table ,
					'title_field_selection_table' => $title_field_selection_table ,
					'priority_field_relation_table' => $priority_field_relation_table,
					'where_clause' => $where_clause,
					'id_field_selection_table' => $id_field_selection_table,// AGREGADO
					'config' => $config,// AGREGADO
				);
		}
		return $this;
	}

	public function set_relation_n_n_last($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table, $primary_key_alias_to_selection_table , $title_field_selection_table , $priority_field_relation_table = null, $where_clause = null, $id_field_selection_table = null, $config = null)// AGREGADOS PARÁMETROS
	{
		$show = true;
		$state_code = $this->getStateCode();
		if($state_code == 1 || $state_code == 7) // si es list armo una comun para que se arme filtro correctamente
		{
		}
		else
		{
			if(isset($config['only_list']) && $config['only_list']) $show = false;
		}

		if($show)
		{
			$this->relation_n_n[$field_name] =
				(object)array(
					'field_name' => $field_name,
					'relation_table' => $relation_table,
					'selection_table' => $selection_table,
					'primary_key_alias_to_this_table' => $primary_key_alias_to_this_table,
					'primary_key_alias_to_selection_table' => $primary_key_alias_to_selection_table ,
					'title_field_selection_table' => $title_field_selection_table ,
					'priority_field_relation_table' => $priority_field_relation_table,
					'where_clause' => $where_clause,
					'id_field_selection_table' => $id_field_selection_table,// AGREGADO
					'config' => $config,// AGREGADO
				);
		}
		return $this;
	}
	protected function get_relation_array($relation_info, $primary_key_value = null, $limit = null)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by, $related_id_field, $config)  = $relation_info;// AGREGADO PARÁMETRO

		if($primary_key_value !== null)
		{
			$primary_key = $this->basic_model->get_primary_key($related_table);

			//A where clause with the primary key is enough to take the selected key row
			$where_clause = array($primary_key => $primary_key_value);
		}

		$relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit, null, $related_id_field, $config);// AGREGADO PARÁMETRO

		$dep = 0;
		if($this->state_code != 3)
		{
			foreach($this->relation_dependency as $target_field => $dependency)
			{
				if($field_name == $target_field) $dep = 1;
			}
		}

		if($this->state_code == 1) $dep = 0;

		// TODO si es state 3 hay que cargar solo los de ese grupo

		if($dep == 1) $relation_array = array();

		return $relation_array;
	}

	protected function get_dependency_relation_array($relation_info, $relation_key_field, $relation_key_value, $limit = null)
	{
		list($field_name , $related_table , $related_field_title, $where_clause, $order_by, $related_id_field)  = $relation_info;// AGREGADO PARÁMETRO

		$where_clause = array($relation_key_field => $relation_key_value);
		if(empty($relation_key_value)){
			$relation_array = array();
		}else{
			$relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit, $related_id_field);// AGREGADO PARÁMETRO
		}
		return $relation_array;
	}

	/* PARA AGREGAR NUEVOS TIPOS DE DATO */

	protected function change_list_value($field_info, $value = null)
	{
		$real_type = $field_info->crud_type;

		switch ($real_type) {
			case 'text':
				// AGREGADO PARA EL GROUP_CONCAT DE PERFILES DE LA TABLA PERSONAL
				// $value = $this->character_limiter(strip_tags($value),$this->character_limiter,"...");
				break;

			case 'time':
				if(!empty($value) && $value != '00:00:00') {
					list($hours,$minutes) = explode(":",substr($value,0));
					$value = $hours.':'.$minutes;
				} else {
					$value = '';
				}
				break;

			default:
				$value = parent::change_list_value($field_info, $value);
				break;
		}

		return $value;
	}

	public function set_relation_n_n_extra($field_name, $relation_table, $selection_table, $primary_key_alias_to_this_table, $primary_key_alias_to_selection_table , $title_field_selection_table , $priority_field_relation_table = null, $where_clause = null, $config = null)
	{
		$show = true;
		$state_code = $this->getStateCode();
		if($state_code == 1 || $state_code == 7) // si es list armo una comun para que se arme filtro correctamente
		{
			$extra_fields = false;
		}
		else
		{
			$extra_fields = true;

			if(isset($config['only_list']) && $config['only_list']) $show = false;
		}

		if($show)
		{
			$this->relation_n_n[$field_name] =
				(object)array(
					'field_name' => $field_name,
					'relation_table' => $relation_table,
					'selection_table' => $selection_table,
					'primary_key_alias_to_this_table' => $primary_key_alias_to_this_table,
					'primary_key_alias_to_selection_table' => $primary_key_alias_to_selection_table ,
					'title_field_selection_table' => $title_field_selection_table ,
					'priority_field_relation_table' => $priority_field_relation_table,
					'where_clause' => $where_clause,
					'extra_fields' => $extra_fields, // ADD NTON
					'config' => $config,// AGREGADO
				);
		}
		return $this;
	}

	protected function get_success_message_at_list($field_info = null)
	{
		if($field_info !== null && isset($field_info->success_message) && $field_info->success_message)
		{
			if(!empty($field_info->primary_key) && !$this->unset_edit)
			{
				// return $this->l('insert_success_message')." <a class='go-to-edit-form' href='".$this->getEditUrl($field_info->primary_key)."'>".$this->l('form_edit')." {$this->subject}</a> ";
				// AGREGADO
				if($this->ci->session->has_userdata('custom_success_message'))
				{
					return $this->l('insert_success_message')." <a class='go-to-edit-form' href='".$this->getEditUrl($field_info->primary_key)."'>".$this->l('form_edit')." {$this->subject}</a></br>".$this->ci->session->userdata('custom_success_message');
				}
				else
				{
					return $this->l('insert_success_message')." <a class='go-to-edit-form' href='".$this->getEditUrl($field_info->primary_key)."'>".$this->l('form_edit')." {$this->subject}</a> ";
				}
			}
			else
			{
				return $this->l('insert_success_message');
			}
		}
		else
		{
			return null;
		}
	}

	protected function update_layout($update_result = false, $state_info = null)
	{
		@ob_end_clean();
		if($update_result === false)
		{
			echo json_encode(array('success' => $update_result));
		}
		elseif(isset($update_result['status'])){
			//TODO:custom error
			$error_message = $update_result['message'];
			echo json_encode(array(
				'success' => $update_result['status'] ,
				'error_message' => $error_message,
			));
		}
		else
		{
			$success_message = '<p>'.$this->l('update_success_message');
			if(!$this->unset_back_to_list && !$this->_is_ajax())
			{
				$success_message .= " <a href='".$this->getListUrl()."'>".$this->l('form_go_back_to_list')."</a>";
			}
			$success_message .= '</p>';

			// AGREGADO
			if($this->ci->session->has_userdata('custom_success_message'))
			{
				$success_message .= '<p>';
				$success_message .= $this->ci->session->userdata('custom_success_message');
				$success_message .= '</p>';
			}
			// FIN AGREGADO
			echo json_encode(array(
				'success' => true ,
				'insert_primary_key' => $update_result,
				'success_message' => $success_message,
				'success_list_url'	=> $this->getListSuccessUrl($state_info->primary_key)
			));
		}
		$this->set_echo_and_die();
	}

	protected function get_string_input($field_info,$value)
	{
		$value = !is_string($value) ? '' : str_replace('"',"&quot;",$value);

		// AGREGADO
		if ($this->getState() == 'add' && empty($value)) $value = $field_info->extras;
		// FIN AGREGADO
		$extra_attributes = '';
		if (!empty($field_info->db_max_length)) {

			if (in_array($field_info->type, array("decimal", "float"))) {
				$decimal_lentgh = explode(",", $field_info->db_max_length);
				$decimal_lentgh = ((int)$decimal_lentgh[0]) + 1;

				$extra_attributes .= "maxlength='" . $decimal_lentgh . "'";
			} else {
				$extra_attributes .= "maxlength='{$field_info->db_max_length}'";
			}

		}
		$input = "<input id='field-{$field_info->name}' class='form-control' name='{$field_info->name}' type='text' value=\"$value\" $extra_attributes />";
		return $input;
	}

	function edit_upload_fied($value, $primary_key)
	{
		$args = func_get_args();
		$name = $args[2]->name;


		$this->_reset( $name );

		$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
		$result = $result->result_array();

		$files = array();
		if(!empty($result) && $result[0][$this->upload_field])
		{
			$data = @unserialize($result[0][$this->upload_field]);
			if ($data !== false) {
				$files = $data;
			} else {
				$files = [];
			}
		}

		$html = '<div>
			 <span class="fileinput-button qq-upload-button" 
			         id="' . $this->upload_field . '_upload-button-svc">
			 <span>Upload a file</span>
			 <input type="file" 
				name="' . $this->upload_field . '_new_multi_upload" 
				id="' . $this->upload_field . '_new_multi_upload_field" >
			 </span>
                         '.( $this->show_allowed_types ? '<span class="allowed_types '.$this->upload_field.'_allowed_types">'.str_replace('|',',',$this->allowed_types).'</span>' : null ) .'
			 <span class="qq-upload-spinner" 
				  id="ajax-loader-file" 
			       style="display:none;"></span>
			 <span    id="' . $this->upload_field . '_progress-multiple" 	
			       style="display:none;"></span>
			</div>';

		$html.= '<select 
				name="' . $this->upload_field . '_files[]" 
			    multiple="multiple" 
			        size="8" 
			       class="multiselect" 
				  id="' . $this->upload_field . '_multiple_select" 
                               style="display:none;">';

		if (!empty($files))
		{
			foreach ($files as $items)
			{
				$html.="<option value=" . $items . " selected='selected'>" . $items . "</option>";
			}
		}
		$html.='</select>';
		$html.='<div id="' . $this->upload_field . '_list_svc" 
			     class="mutiupload_list row" 
			     style="margin-top: 40px;">';

		if (!empty($files))
		{

			foreach ($files as $items)
			{
				$thisfile = base_url() . $this->path_to_directory . $items ;

				if($this->remote_file_exists($thisfile))
				{
					if( strpos ($items,"." ) !== false )
					{

						$html.= '<input type="hidden" class="' . $this->upload_field . '-uploaded-file" value="'.$thisfile.'">';

						if ($this->_is_image($items) === true)
						{
							// $html.= '<div id="' . $items . '">';
							// AGREGADO
							$html.= '<div id="' . $items . '" class="upload-success-url">';
							$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
							$html.='<img src="' . $thisfile . '" height="50"/>';
							$html.='</a><br>';
							$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
							$html.='</div>';
						} elseif ($this->_is_video($items) === true){
							// $html.= '<div id="' . $items . '">';
							// AGREGADO

							$html.= '<div id="' . $items . '" class="upload-success-url">';
							$html.= '<video width="320" height="240" controls>';
							//$html.= '<source src="'.base_url().'assets/uploads/videobooks_trabajadores/'.$items.'" type="video/mp4">';
							$html.= '<source src="'.$thisfile.'" type="video/mp4">';
							$html.= '<source src="'.$thisfile.'" type="video/ogg">';
							$html.= 'Your browser does not support the video tag.';
							$html.= '</video>';
							$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
							$html.='</a><br>';
							$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
							$html.='</div>';
						}
						else
						{
							$html.='<div id="' . $items . '" >
                            <span><a href="'.$thisfile.'" target="_blank">' . $items . '</a></span>
                            <a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>
                            </div>';
						}

					}

				}
			}
		}

		$html.='</div>';

		$html.='<div class="col-md-12">';
		$html.='<a href="#" class="download-files-button" data-key="' . $this->upload_field . '">Descargar archivos</a>';
		$html.='</div>';


		$html.=$this->JS($name);
		$html.=$this->downloadJS($name);

		return $html;
	}

	protected function JS($field=null)
	{

		if(is_null($field))return $field;

		$js = "
 		if (typeof string_progress === 'undefined') {
 			var string_upload_file 	= 'Subir archivos';
			var string_delete_file 	= 'Borrar';
			var string_progress 			= 'Cargando: ';
			var error_on_uploading 			= 'Ocurrio un errro al subir el archivo.';
			var message_prompt_delete_file 	= 'Esta seguro que quiere borrar el archivo?';

			var error_max_number_of_files 	= 'Solamente se puede subir de a un archivo.';
			var error_accept_file_types 	= 'No tienes permitido subir ese tipo de archivo.';
			var error_max_file_size 		= 'El archivo excede los 5000MB.';
			var error_min_file_size 		= 'No es posible subir archivos vacios.';

 		}	
	 	function delete_" . $this->upload_field . "_svc(link,filename)
	 	{
	 		$('#" . $this->upload_field . "_multiple_select option[value=\"'+filename+'\"]').remove();
	 		link.parent().remove();
	 		$.post('" . $this->multi_upload_function . "/delete_file', {'file_name':filename,'".$this->ci->security->get_csrf_token_name()."':'".$this->ci->security->get_csrf_hash()."','field':'".$field."','state':window.location.href.substring(window.location.href.lastIndexOf('/') + 1)}, function(json){
	 			if(json.succes == 'true')
	 			{
	 				console.log('json data', json);
	 			}
	 		}, 'json');
}

$(document).ready(function() {
	$('#" . $this->upload_field . "_new_multi_upload_field').fileupload({
		url: '" . $this->multi_upload_function . "/uploade',
		sequentialUploads: true,
		formData:{'".$this->ci->security->get_csrf_token_name()."':'".$this->ci->security->get_csrf_hash()."','field':'".$field."'},
		cache: false,
		autoUpload: true,
		dataType: 'json',
		acceptFileTypes: /(\.|\/)(" . $this->ci->config->item('grocery_crud_file_upload_allow_file_types') . ")$/i,
		limitMultiFileUploads: 1,
		beforeSend: function()
		{
			$('#" . $this->upload_field . "_upload-button-svc').slideUp('fast');
			$('#ajax-loader-file').css('display','block');
			$('#" . $this->upload_field . "_progress-multiple').css('display','block');
		},
		progress: function (e, data) {
			$('#" . $this->upload_field . "_progress-multiple').html(string_progress + parseInt(data.loaded / data.total * 100, 10) + '%');
		},
		done: function (e, data)
		{
			/*console.log(data.result);*/
			if(data.result.success == 'false') {
				alert(data.result.error);
				$('#" . $this->upload_field . "_upload-button-svc').show('fast');
				$('#ajax-loader-file').css('display','none');
				$('#" . $this->upload_field . "_progress-multiple').css('display','none');
				$('#" . $this->upload_field . "_progress-multiple').html('');
				return;
			}
			$('#" . $this->upload_field . "_multiple_select').append('<option value=\"'+data.result.file_name+'\" selected=\"selected\">'+data.result.file_name+'</select>');
			var is_image = (data.result.file_name.substr(-4) == '.jpg'
				|| data.result.file_name.substr(-4) == '.png'
				|| data.result.file_name.substr(-5) == '.jpeg'
				|| data.result.file_name.substr(-4) == '.gif'
				|| data.result.file_name.substr(-5) == '.tiff')
				? true : false;
				var html;
				if(is_image==true)
				{
					// html='<div id=\"'+data.result.file_name+'\" class=\"upload-success-url\" ><a href=\"" . base_url() . $this->path_to_directory . "'+data.result.file_name+'\" class=\"image-thumbnail\" id=\"fancy_'+data.result.file_name+'\">';
					// AGREGADO
                    html='<div id=\"'+data.result.file_name+'\" class=\"upload-success-url\" ><a href=\"" . base_url() . $this->path_to_directory . "'+data.result.file_name+'\" class=\"image-thumbnail\" id=\"fancy_'+data.result.file_name+'\">';
					html+='<img src=\"" . base_url() . $this->path_to_directory . "'+data.result.file_name+'\" height=\"50\"/>';
					html+='</a><br><a href=\"javascript:;\" onclick=\"delete_" . $this->upload_field . "_svc($(this),\''+data.result.file_name+'\')\" style=\"color:red;\" >Delete</a></div>';
					$('#" . $this->upload_field . "_list_svc').append(html);
					$('.image-thumbnail').fancybox({
						'closeBtn'   : true,
						'transitionIn' : 'elastic',
						'transitionOut' : 'elastic',
						'speedIn' : 600,
						'speedOut' : 200,
						'overlayShow' : true
					});
				}
				else
				{
					html = '<div id=\"'+data.result.file_name+'\" class=\"col-md-12\" ><span>'+data.result.file_name+'</span> <br><a href=\"javascript:\" onclick=\"delete_" . $this->upload_field . "_svc($(this),\''+data.result.file_name+'\')\" style=\"color:red;\" >Delete</a></div>';
					$('#" . $this->upload_field . "_list_svc').append(html);
				}
					$('#" . $this->upload_field . "_upload-button-svc').show('fast');
					$('#ajax-loader-file').css('display','none');
					$('#" . $this->upload_field . "_progress-multiple').css('display','none');
					$('#" . $this->upload_field . "_progress-multiple').html('');
				}
			});

		});
";


		$js = "<script>\n".$js."\n</script>";

		return $js;
	}

	protected function downloadJS($field=null)
	{

		if(is_null($field))return $field;

		$js = "
        $(document).ready(function() {
            
            $('.download-files-button').unbind().click(function(e){

                e.preventDefault();
                e.stopPropagation();
                
                var files = [];
                
                $('.'+$(this).data('key')+'-uploaded-file').each(function(index){
                    files.push($(this).val());
                });  
                
                var fileName = $(this).data('key');
                
                $.ajax({
                async: false,
                method: \"POST\",
                url: __url__ + '/admin/trabajadores/descargar_archivos',
                data: {
                    '".$this->ci->security->get_csrf_token_name()."':'".$this->ci->security->get_csrf_hash()."',
                    archivos: files,
                    file_name: fileName
                }
                }).done(function (res) {
        
                    window.open(String(res), '_blank');
                   
                }).fail(function (res) {
                    //alert(error_msg);
                }); 
              
            });
            
            
        
        });
        ";


		$js = "<script>\n".$js."\n</script>";

		return $js;
	}

	function view_upload_field($value, $primary_key)
	{

		$args = func_get_args();
		$name = $args[2]->name;


		$this->_reset( $name );

		$any_file = 0;


		$result = $this->ci->db->get_where($this->file_table,array($this->primary_key => $primary_key));
		$result = $result->result_array();

		if(!empty($result) && $result[0][$this->upload_field])
		{
			$files = unserialize($result[0][$this->upload_field]);
		}
		else
		{
			return $this->no_file_text;
		}


		$html = '<select name="' . $this->upload_field . '_files[]" multiple="multiple" size="8" class="multiselect" id="' . $this->upload_field . '_multiple_select" style="display:none;">';
		if (!empty($files))
		{
			foreach ($files as $items)
			{
				$html.="<option value=" . $items . " selected='selected'>" . $items . "</option>";
			}
		}
		$html.='</select>';
		$html.='<div id="' . $this->upload_field . '_list_svc" class="mutiupload_list" style="margin-top: 40px;">';
		if (!empty($files))
		{

			$html .= "<table>";

			foreach ($files as $items)
			{

				$thisfile = base_url() . $this->path_to_directory . $items;

				if($this->remote_file_exists($thisfile))
				{
					$html .="<tr>";	$any_file = 1;

					if ($this->_is_image($items) === true)
					{
						// $html.= '<td><div id="' . $items . '">';
						// AGREGADO
						$html.= '<td><div id="' . $items . '" class="upload-success-url">';
						$html.= '<a href="' . $thisfile . '" class="image-thumbnail" id="fancy_' . $items . '">';

						$html.='<img src="' .$thisfile. '" height="50"/>';
						$html.='</a>';
						$html.='</div></td>';
					} elseif ($this->_is_video($items) === true){
						// $html.= '<div id="' . $items . '">';
						// AGREGADO

						$html.= '<div id="' . $items . '" class="upload-success-url">';
						$html.= '<video width="320" height="240" controls>';
						//$html.= '<source src="'.base_url().'assets/uploads/videobooks_trabajadores/'.$items.'" type="video/mp4">';
						$html.= '<source src="'.$thisfile.'" type="video/mp4">';
						$html.= '<source src="'.$thisfile.'" type="video/ogg">';
						$html.= 'Your browser does not support the video tag.';
						$html.= '</video>';
						$html.= '<a href="' .$thisfile. '" class="image-thumbnail" id="fancy_' . $items . '">';
						$html.='</a><br>';
						$html.='<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>';
						$html.='</div>';
					}
					else
					{
						$html.='<div id="' . $items . '" >
					<span><a href="'.$thisfile.'" target="_blank">' . $items . '</a></span>
					<a href="javascript:" onclick="delete_' . $this->upload_field . '_svc($(this),\'' . $items . '\')" style="color:red;" >Delete</a>
					</div>';
					}

					if( strpos ($items,"." ) !== false )
					{
						if(
							(
								$this->enable_download_button &&
								is_null($this->download_allowed)
							) ||
							(
								$this->enable_download_button &&
								in_array(
									pathinfo(
										$thisfile, PATHINFO_EXTENSION
									),
									explode(
										"|",$this->download_allowed
									)
								)
							)
						)
						{
							$html.='<td><a href="'.$thisfile.'">Download</a></td>';
						}
					}

					$html .= "</tr>";

				}

			}
			$html .= "</table>";
		}
		$html.='</div>';

		if(!$any_file){ $html = $this->no_file_text; }

		return $html;
	}

	protected function showAddForm()
	{
		$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

		$data 				= $this->get_common_data();
		$data->types 		= $this->get_field_types();

		$data->list_url 		= $this->getListUrl();
		$data->insert_url		= $this->getInsertUrl();
		$data->validation_url	= $this->getValidationInsertUrl();
		$data->input_fields 	= $this->get_add_input_fields();

		$data->fields 			= $this->get_add_fields();
		$data->hidden_fields	= $this->get_add_hidden_fields();
		$data->unset_back_to_list	= $this->unset_back_to_list;
		$data->unique_hash			= $this->get_method_hash();
		$data->is_ajax 			= $this->_is_ajax();

		// AGREGADO LINK NUEVO ITEM
		$data->from_url 	    = '';

		$ci = &get_instance();

		if ($ci->session->userdata('from_url'))
		{
			$data->from_url 	= $ci->session->userdata('from_url');
			$data->unset_back_to_list	= true;
		}
		// FIN AGREGADO LINK NUEVO ITEM
		$this->_theme_view('add.php',$data);
		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

		$this->_get_ajax_results();
	}

	protected function get_relation_n_n_input($field_info_type, $selected_values)
	{
		// ADD NTON

		$has_priority_field = !empty($field_info_type->extras->priority_field_relation_table) ? true : false;
		$has_extra_fields = isset($field_info_type->extras->extra_fields) && !empty($field_info_type->extras->extra_fields) ? true : false;
		$is_ie_7 = isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7') !== false) ? true : false;

		if($has_priority_field || $is_ie_7)
		{
			$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
			$this->set_css($this->default_css_path.'/jquery_plugins/ui.multiselect.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui.multiselect.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.multiselect.js');

			if($this->language !== 'english')
			{
				include($this->default_config_path.'/language_alias.php');
				if(array_key_exists($this->language, $language_alias))
				{
					$i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/multiselect/ui-multiselect-'.$language_alias[$this->language].'.js';
					if(file_exists($i18n_date_js_file))
					{
						$this->set_js_lib($i18n_date_js_file);
					}
				}
			}
		}
		else if($has_extra_fields){
			$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
			$this->set_css($this->default_css_path.'/jquery_plugins/chosen/chosen.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);

			$this->set_js($this->default_javascript_path.'/jquery_plugins/config/jquery.tab.config.js');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.chosen.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
		}
		else
		{
			$this->set_css($this->default_css_path.'/jquery_plugins/chosen/chosen.css');
			$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.chosen.min.js');
			$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
		}

		$this->inline_js("var ajax_relation_url = '".$this->getAjaxRelationUrl()."';\n");

		$field_info 		= $this->relation_n_n[$field_info_type->name]; //As we use this function the relation_n_n exists, so don't need to check
		$unselected_values 	= $this->get_relation_n_n_unselected_array($field_info, $selected_values);

		if(empty($unselected_values) && empty($selected_values))
		{
			$input = "Please add {$field_info_type->display_as} first";
		}
		else
		{
			// ADD NTON
			if($has_extra_fields)
			{
				$extra_field_name = $field_info_type->name;
				$select_title = str_replace('{field_display_as}',$field_info_type->display_as,$this->l('set_relation_title'));
				$input = "<select id='field-{$field_info_type->name}' name='{$field_info_type->name}[]' multiple='multiple' size='8' data-placeholder='$select_title' style='display:none;' >";

				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name){
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name){
						$input .= "<option value='$id' selected='selected'>$name</option>";
					}

				$input .= "</select>";

				$input .= "<select class='relation_table_value_selector chosen-select' data-field='{$field_info_type->name}' data-placeholder='$select_title'>";

				$input .= "<option value=''></option>";

				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name){
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name){
						$input .= "<option value='$id' selected='selected'>$name</option>";
						//$input .= "<option value='$id'>$name</option>";
					}

				$input .= "</select>";

				$extra_fields = $this->basic_model->get_field_types($field_info_type->extras->relation_table);
				$extra_state_info = $this->getStateInfo();

				$input .= "<div id='{$field_info_type->name}Tabs' class='tabs'>";
				$input .= " <ul>";

				if(!empty($selected_values))
				{
					foreach($selected_values as $id => $name){
						$id_html = "$extra_field_name-relation-$id";

						$extra_where = array($field_info_type->extras->primary_key_alias_to_this_table => $extra_state_info->primary_key ,
							$field_info_type->extras->primary_key_alias_to_selection_table => $id);
						$values = $this->basic_model->get_extra_edit_values($field_info_type->extras->relation_table, $extra_where);

						$campos_extra = '';
						foreach ($extra_fields as $extra_field){
							if (   $this->unset_edit_fields !== null
								&& is_array($this->unset_edit_fields)
								&& in_array(('extra_field_'.$extra_field->name),$this->unset_edit_fields)
							) {
								continue;
							}
							if( $extra_field->name != $field_info_type->extras->primary_key_alias_to_this_table
								&& $extra_field->name != $field_info_type->extras->primary_key_alias_to_selection_table){

								$extra_field->db_type = $extra_field->type;
								$extra_field->db_max_length = $extra_field->max_length;

								$extra_field_info = (object)array();
								$extra_field_info->name		= $field_info_type->name.'Extras['.$id.']['.$extra_field->name.']';
								$extra_field_info->type 	= $this->get_type($extra_field);
								$extra_field_info->crud_type 	= $this->get_type($extra_field);
								if($extra_field_info->crud_type== 'text'){
									$extra_field_info->extras = 'text_editor';
									$extra_field_info->extras = null;
								}
								elseif($extra_field_info->crud_type == 'set' || $extra_field_info->crud_type == 'enum'){

									$extra_field_info->extras = $this->basic_model->enum_select($field_info_type->extras->relation_table, $extra_field->name);
								}
								else{
									$extra_field_info->extras = null;
								}

								//var_dump($extra_field_info->extras);
								$extra_field_info->db_max_length = $extra_field->db_max_length;
								$extra_field_info->required	= false;
								//$extra_field_info->display_as = ucfirst(str_replace("_"," ",$extra_field->name));
								$extra_field_info->display_as = isset($this->display_as['extra_field_'.$extra_field->name]) ?
									$this->display_as['extra_field_'.$extra_field->name] :
									ucfirst(str_replace("_"," ",$extra_field->name));

								$campos_extra .= "<div>".$extra_field_info->display_as."</div><div class='".$field_info_type->name."Extra_".$extra_field->name."'>".$this->get_field_input($extra_field_info, $values->{$extra_field->name})->input."</div>";
							}
						}

						$input .= "<li id='$id_html' class='edit_charge_field'>";
						$input .= "<div class='col-md-2 relation_nton_name'>";
						$input .= "$name <span class='ui-icon ui-icon-close remove_nton' data-value='$id_html' data-field='{$field_info_type->name}' role='presentation'>Remove Tab</span>";
						$input .= "</div>";
						$input .= "<div class='col-md-10 relation_nton_datos'>";
						$input .= "$campos_extra";
						$input .= "</div>";
						$input .= "</li>";
					}
				}

				$input .= "</ul>";
				$input .= "</div>";
				$input .= "<div id='{$field_info_type->name}Template' style='display:none'>";

				foreach ($extra_fields as $extra_field){
					if (   $this->unset_edit_fields !== null
						&& is_array($this->unset_edit_fields)
						&& in_array(('extra_field_'.$extra_field->name),$this->unset_edit_fields)
					) {
						continue;
					}
					if( $extra_field->name != $field_info_type->extras->primary_key_alias_to_this_table
						&& $extra_field->name != $field_info_type->extras->primary_key_alias_to_selection_table){

						$extra_field->db_type = $extra_field->type;
						$extra_field->db_max_length = $extra_field->max_length;

						$extra_field_info = (object)array();
						$extra_field_info->name		= $field_info_type->name.'Extras[{primary_key_value}]['.$extra_field->name.']';
						$extra_field_info->type 	= $this->get_type($extra_field);
						$extra_field_info->crud_type 	= $this->get_type($extra_field);
						if($extra_field_info->crud_type == 'text'){
							$extra_field_info->extras = 'text_editor_to_be';

							$extra_field_info->extras = null;
						}
						elseif($extra_field_info->crud_type == 'set' || $extra_field_info->crud_type == 'enum'){
							$extra_field_info->extras = $this->basic_model->enum_select($field_info_type->extras->relation_table, $extra_field->name);
						}
						else{
							$extra_field_info->extras = null;
						}
						$extra_field_info->db_max_length = $extra_field->db_max_length;
						$extra_field_info->required	= false;
						$extra_field_info->display_as = isset($this->display_as['extra_field_'.$extra_field->name]) ?
							$this->display_as['extra_field_'.$extra_field->name] :
							ucfirst(str_replace("_"," ",$extra_field->name));
						$input .= "<div>".$extra_field_info->display_as."</div><div class='".$field_info_type->name."Extra_".$extra_field->name."'>".$this->get_field_input($extra_field_info)->input."</div>";
					}
				}
				$input .= "</div>";
			}
			else
			{
				$css_class = $has_priority_field || $is_ie_7 ? 'multiselect': 'chosen-multiple-select';
				$width_style = $has_priority_field || $is_ie_7 ? '' : 'width:510px;';

				$select_title = str_replace('{field_display_as}',$field_info_type->display_as,$this->l('set_relation_title'));
				$input = "<select id='field-{$field_info_type->name}' name='{$field_info_type->name}[]' multiple='multiple' size='8' class='$css_class' data-placeholder='$select_title' style='$width_style' >";

				// AGREGADO
				if (isset($field_info->config['sin_opciones']))
				{
					$input .= "<option value='0'>".$field_info->config['sin_opciones']."</option>";
				}
				// FIN AGREGADO
				if(!empty($unselected_values))
					foreach($unselected_values as $id => $name)
					{
						$input .= "<option value='$id'>$name</option>";
					}

				if(!empty($selected_values))
					foreach($selected_values as $id => $name)
					{
						$input .= "<option value='$id' selected='selected'>$name</option>";
					}

				$input .= "</select>";
			}
		}

		return $input;
	}

	public function having($key, $value = '', $escape = TRUE)
	{
		$this->having[] = array($key, $value, $escape);

		return $this;
	}

	public function in_where($key, $value = NULL, $escape = TRUE)
	{

		if ($value != '()') $this->in_where[] = array($key,$value,$escape);

	}

	protected function get_common_data()
	{
		$data = parent::get_common_data();

		$data->table = $this->get_table();

		return $data;
	}

	/**
	 * cambio de filtros del search
	 *
	 */
	var $filters = array();
	var $filters_adv = array();
	var $filters_check = false;
	var $filters_adv_check = false;
	var $unset_filter_fields = null;
	var $callback_filter_field = array();

	public function filters()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_array($args[0]))
		{
			$args = $args[0];
		}

		$this->filters = $args;

		return $this;
	}

	public function advanced_filters()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_array($args[0]))
		{
			$args = $args[0];
		}

		$this->filters_adv = $args;

		return $this;
	}

	public function unset_filter_fields()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_array($args[0]))
		{
			$args = $args[0];
		}

		$this->unset_filter_fields = $args;

		return $this;
	}

	public function callback_filter_field($field, $callback = null)
	{
		$this->callback_filter_field[$field] = $callback;

		return $this;
	}


	public function get_field_types()
	{

		$types = parent::get_field_types();
//*-*-*-*-*-*-*-*
		$types = $this->set_filters_types('filters', $types);
		$types = $this->set_filters_types('filters_adv', $types);

		$this->field_types = $types;
		return $this->field_types;
	}

	protected function set_filters_types($obj, $types)
	{
		if(!empty($this->$obj))
		{

			foreach($this->$obj as $field_object)
			{

				$field_name = isset($field_object->field_name) ? $field_object->field_name : $field_object;

				if(!isset($types[$field_name]))//Doesn't exist in the database? Create it for the CRUD
				{
					$extras = false;
					if($this->change_field_type !== null && isset($this->change_field_type[$field_name]))
					{
						$field_type = $this->change_field_type[$field_name];
						$extras 	=  $field_type->extras;
					}

					$crud_type = $this->change_field_type !== null && isset($this->change_field_type[$field_name]) ?
						$this->change_field_type[$field_name]->type :
						'string';

					if(isset($this->relation[$field_name]))
					{
						$crud_type 	= 'relation';
						$extras 	= $this->relation[$field_name];
					}

					$field_info = (object)array(
						'name' => $field_name,
						'crud_type' => $crud_type,
						'display_as' => isset($this->display_as[$field_name]) ?
							$this->display_as[$field_name] :
							ucfirst(str_replace("_"," ",$field_name)),
						'required'	=> !empty($this->required_fields) && in_array($field_name,$this->required_fields) ? true : false,
						'extras'	=> $extras
					);

					$types[$field_name] = $field_info;
				}
			}
		}
		return $types;
	}
	protected function get_filters_fields($obj, $full_on_empty = true)
	{
		$obj_check = $obj . '_check';

		if($this->$obj_check === false)
		{
			$field_types = $this->get_field_types();
			if(!empty($this->$obj))
			{
				foreach($this->$obj as $field_num => $field)
				{
					if(isset($this->display_as[$field]))
						$this->{$obj}[$field_num] = (object)array('field_name' => $field, 'display_as' => $this->display_as[$field]);
					elseif(isset($field_types[$field]->display_as))
						$this->{$obj}[$field_num] = (object)array('field_name' => $field, 'display_as' => $field_types[$field]->display_as);
					else
						$this->{$obj}[$field_num] = (object)array('field_name' => $field, 'display_as' => ucfirst(str_replace('_',' ',$field)));
				}
			}
			else
			{
				$this->$obj = array();
				if($full_on_empty)
				{
					foreach($field_types as $field)
					{
						//Check if an unset_add_field is initialize for this field name
						if($this->unset_filter_fields !== null && is_array($this->unset_filter_fields) && in_array($field->name,$this->unset_filter_fields))
							continue;

						if( (!isset($field->db_extra) || $field->db_extra != 'auto_increment') )
						{
							if(isset($this->display_as[$field->name]))
								$this->{$obj}[] = (object)array('field_name' => $field->name, 'display_as' => $this->display_as[$field->name]);
							else
								$this->{$obj}[] = (object)array('field_name' => $field->name, 'display_as' => $field->display_as);
						}
					}
				}

			}

			$this->$obj_check = true;
		}
		return $this->$obj;
	}

	public function get_filters_inputs($obj, $full_on_empty = true)
	{
		$fields = $this->get_filters_fields($obj, $full_on_empty);
		$types 	= $this->get_field_types();

		$input_fields = array();

		foreach($fields as $field_num => $field)
		{
			$field_info = isset($types[$field->field_name])?$types[$field->field_name]:null;

			$field_value = null; // es un search no debe tener contenido
			if(!isset($this->callback_filter_field[$field->field_name]))
			{

				$field_input = $this->get_field_input($field_info, $field_value);
			}
			else
			{
				$primary_key = $this->getStateInfo()->primary_key;
				$field_input = $field_info;
				$field_input->input = call_user_func($this->callback_filter_field[$field->field_name], $field_value, $primary_key, $field_info, $field_values);
			}

			switch ($field_info->crud_type) {
				case 'invisible':
					unset($this->edit_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
				case 'hidden':
					$this->edit_hidden_fields[] = $field_input;
					unset($this->edit_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
			}

			$input_fields[$field->field_name] = $field_input;
		}

		return $input_fields;
	}

	public function get_filters()
	{
		return $this->get_filters_inputs('filters', false);
	}

	public function get_filters_adv()
	{
		return $this->get_filters_inputs('filters_adv', false);
	}


	protected function set_ajax_list_queries($state_info = null)
	{
		$field_fecha_trabajos = true;
		$field_types = $this->get_field_types();

		if(!empty($state_info->per_page))
		{
			if(empty($state_info->page) || !is_numeric($state_info->page) )
				$this->limit($state_info->per_page);
			else
			{
				$limit_page = ( ($state_info->page-1) * $state_info->per_page );
				$this->limit($state_info->per_page, $limit_page);
			}
		}

		if(!empty($state_info->order_by))
		{
			$this->order_by($state_info->order_by[0],$state_info->order_by[1]);
		}

		if(!empty($state_info->search))
		{
			$table = $this->get_table();

			if (!empty($this->relation)) {
				foreach ($this->relation as $relation_name => $relation_values) {
					$temp_relation[$this->_unique_field_name($relation_name)] = $this->_get_field_names_to_search($relation_values);
				}
			}

			if (is_array($state_info->search)) {
				foreach ($state_info->search as $search_field => $search_text) {
					// busco si es una 1_1

					$relation_1_1_table = '';
					if (isset($this->relation_1_1)) {
						foreach($this->relation_1_1 as $relation_1_1_name => $relation_1_1_data){
							if($relation_1_1_data[2]){
								foreach($relation_1_1_data[2] as $relation_1_1_campo){
									if($relation_1_1_campo == $search_field){
										$relation_1_1_table = $this->_unique_join_name($relation_1_1_name);
									}
								}
							}
						}
					}


					$search_field_base = str_replace('_1','',$search_field);
					$search_field_base = str_replace('_2','',$search_field_base);

					if(in_array($search_field_base, $this->filter_between)){

						// AL FILTRO FECHA RELATIONS LO PISAMOS TODAS LAS VECES POR EL BETWEEN
						if($search_field_base == 'filtro_fecha'){
							$this->filtro_fecha_relations = array("relations", "group_by");


							$alias_eventos = $this->_unique_join_name('eventos');
							$alias_personal = $this->_unique_join_name('eventos_personal');
							$alias_trabajador = $this->basic_model->get_table_name();


							$relation = array();
							$relation[0] = "eventos_personal AS ".$alias_personal;
							$relation[1] = $alias_trabajador.".id = ".$alias_personal.'.id_trabajador';

							$relation2 = array();
							$relation2[0] = "eventos AS ".$alias_eventos;
							$relation2[1] = $alias_eventos.".id = ".$alias_personal.'.id_eventos';

							$this->filtro_fecha_relations['relations'][] = $relation;
							$this->filtro_fecha_relations['relations'][] = $relation2;
							$this->filtro_fecha_relations['group_by'][] = $alias_trabajador.'.id';



							// fecha_desde <= fecha_fin
							// Else
							// fecha_hasta >= fecha_inicio

							// $table = $alias_eventos;

							if(strpos($search_field, '_1') !== false){ // Desde
								$this->where($alias_eventos . '. fecha_inicio_evento' .' >= "'. $this->_convert_date_to_sql_date($search_text).'"', null);
							} else if(strpos($search_field, '_2') !== false){ // HASTA
								$this->where($alias_eventos . '.fecha_fin_evento' .' <= "'. $this->_convert_date_to_sql_date($search_text).'"', null);
							}

						} else {

							if (isset($temp_relation[$search_field_base])) {

							} elseif(isset($this->relation[$search_field_base])) {

								$search_field_relation = $table . '.' . $search_field_base;

								if(isset($this->relation[$search_field_base][6]))
								{
									$config = $this->relation[$search_field_base][6];
									if (isset($config['parent_id']) && $config['parent_id'])
									{
										$search_field_relation = $config['parent_id'];
									}
								}

								if(strpos($search_field, '_1') !== false){ // Desde
									$this->where($search_field_relation .' >= '.  $search_text, null);
								} else if(strpos($search_field, '_2') !== false){ // HASTA
									$this->where($search_field_relation .' <= '.  $search_text, null);
								}

								// $this->where($search_field_relation, $search_text);

							} elseif($relation_1_1_table) {

								if(strpos($search_field, '_1') !== false){ // Desde
									$this->where($relation_1_1_table . '.' . $search_field_base .' >= '. $search_text, null);
								} else if(strpos($search_field, '_2') !== false){ // HASTA
									$this->where($relation_1_1_table . '.' . $search_field_base .' <= '. $search_text, null);
								}

							} else {

								if ($this->is_date($search_text))
								{
									$date = $this->date_front_to_back($search_text);

									if ($date)
									{
										if(strpos($search_field, '_1') !== false){ // Desde
											$this->where($table . '.' . $search_field_base . ' >= "' . $date . '"', null);
										} else if(strpos($search_field, '_2') !== false){ // HASTA
											$this->where($table . '.' . $search_field_base . ' <= "' . $date . '"', null);
										}
									}
								}
								else
								{
									if(strpos($search_field, '_1') !== false){ // Desde
										$this->where($table . '.' . $search_field_base .' >= '. $search_text, null);
									} else if(strpos($search_field, '_2') !== false){ // HASTA
										$this->where($table . '.' . $search_field_base .' <= '. $search_text, null);
									}
								}

							}

						}

					} else {

						if (isset($temp_relation[$search_field])) {
							if (is_array($temp_relation[$search_field])) {
								$temp_where_query_array = [];

								foreach ($temp_relation[$search_field] as $relation_field) {
									$escaped_text = $this->basic_model->escape_str($search_text);
									$temp_where_query_array[] = $relation_field . ' LIKE \'%' . $escaped_text . '%\'';
								}
								if (!empty($temp_where_query_array)) {
									$this->where('(' . implode(' OR ', $temp_where_query_array) . ')', null);
								}

							} else {
								$this->like($temp_relation[$search_field] , $search_text);
							}
						} elseif(isset($this->relation_n_n[$search_field])) {

							$escaped_text = $this->basic_model->escape_str($search_text);

							if(is_array($escaped_text))
							{
								foreach($escaped_text as $idval_n_to_n){
									// AGREGADO
									$field_info = $this->relation_n_n[$search_field];

									if (isset($field_info->config['sin_opciones']) && $idval_n_to_n == $field_info->config['sin_opciones'])
									{
										$this->having($search_field." IS NULL", NULL);
									}
									else
									{
										// FIN AGREGADO
										//$idval_n_to_n = preg_replace('/\s+/u', '&nbsp;', $idval_n_to_n);
										//$idval_n_to_n = preg_replace(' - ', '&nbsp;-&nbsp;', $idval_n_to_n);
										//echo $idval_n_to_n."\n";
										$this->having("FIND_IN_SET('".$idval_n_to_n."', ".$search_field.") > 0", NULL);
									}// AGREGADO
								}
							}
							else
							{
								if($escaped_text)
								{
									$this->having($search_field." LIKE '%".$escaped_text."%'");
								}
							}
						} elseif(isset($this->relation[$search_field])) {

							$search_field_relation = $table . '.' . $search_field;
							if(isset($this->relation[$search_field][6]))
							{
								$config = $this->relation[$search_field][6];
								if (isset($config['parent_id']) && $config['parent_id'])
								{
									$search_field_relation = $config['parent_id'] . ' = ';
								}
							}

							$this->where($search_field_relation, $search_text);

						} elseif($relation_1_1_table) {
							$this->like($relation_1_1_table . '.' . $search_field, $search_text);
						}else {
							$this->like($table . '.' . $search_field, $search_text);
						}

					}


					/* elseif($this->field_type_between_desde[$search_field]) {

                                            $search_field = str_replace('_1', '', $search_field);
                                            // $search_field = str_replace('_2', '', $search_field);

                                            $this->where($table . '.' . $search_field .' >= '. $table. '.' . $search_text, null);*/


				}
			} elseif ($state_info->search->field !== null) {
				if (isset($temp_relation[$state_info->search->field])) {
					if (is_array($temp_relation[$state_info->search->field])) {
						foreach ($temp_relation[$state_info->search->field] as $search_field) {
							$this->or_like($search_field , $state_info->search->text);
						}
					} else {
						$this->like($temp_relation[$state_info->search->field] , $state_info->search->text);
					}
				} elseif(isset($this->relation_n_n[$state_info->search->field])) {
					$escaped_text = $this->basic_model->escape_str($state_info->search->text);
					$this->having($state_info->search->field." LIKE '%".$escaped_text."%'");
				} else {
					$this->like($state_info->search->field , $state_info->search->text);
				}
			}
			// Search all field
			else
			{
				$columns = $this->get_columns();

				$search_text = $state_info->search->text;

				$search_text = ($search_text == 'undefined')?'':$search_text;

				if(!empty($this->where))
					foreach($this->where as $where)
						$this->basic_model->having($where[0],$where[1],$where[2]);

				$temp_where_query_array = [];
				$basic_table = $this->get_table();

				foreach($columns as $column)
				{
					if(isset($temp_relation[$column->field_name]))
					{
						if(is_array($temp_relation[$column->field_name]))
						{
							foreach($temp_relation[$column->field_name] as $search_field)
							{
								$escaped_text = $this->basic_model->escape_str($search_text);
								$temp_where_query_array[] = $search_field . ' LIKE \'%' . $escaped_text . '%\'';
							}
						}
						else
						{
							$escaped_text = $this->basic_model->escape_str($search_text);
							$temp_where_query_array[] = $temp_relation[$column->field_name] . ' LIKE \'%' . $escaped_text . '%\'';
						}
					}
					elseif(isset($this->relation_n_n[$column->field_name]))
					{
						//@todo have a where for the relation_n_n statement
					}

					elseif (
						isset($field_types[$column->field_name]) &&
						isset($field_types[$column->field_name]->type) &&
						!in_array($field_types[$column->field_name]->type, array('date', 'datetime', 'timestamp'))
					) {
						$escaped_text = $this->basic_model->escape_str($search_text);
						$temp_where_query_array[] =  '`' . $basic_table . '`.' . $column->field_name . ' LIKE \'%' . $escaped_text . '%\'';
					}
				}

				if (!empty($temp_where_query_array)) {
					$this->where('(' . implode(' OR ', $temp_where_query_array) . ')', null);
				}
			}
		}
	}

	public function get_state_code()
	{
		return parent::getStateCode();
	}

	/**
	 * ADD NEW TYPE TIME
	 */
	var $array_tipos = array(
		'integer',
		'text',
		'true_false',
		'string',
		'date',
		'datetime',
		'enum',
		'set',
		'relation',
		'relation_readonly',
		'relation_n_n',
		'upload_file',
		'upload_file_readonly',
		'hidden',
		'password',
		'readonly',
		'dropdown',
		'multiselect',
		'time'
	);

	public function get_field_input($field_info, $value = null)
	{
		$real_type = $field_info->crud_type;

		$types_array = $this->array_tipos;

		$input = "";

		if (in_array($field_info->name, $this->filter_between))// asdasd
		{
			$state_code = $this->getStateCode();

			if ($state_code == 1 || $state_code == 7)// si es list armo una comun para que se arme filtro correctamente
			{
				$name = $field_info->name;
				$display_as = $field_info->display_as;

				$field_info->display_as = $display_as . " Desde";
				$field_info->name = $name . "_1";

				$field_info->display_as = $display_as . " Desde";

				$input .= "<div class='row'>";
				$input .= "<div class='col-md-6 between_campo ".$field_info->name."'>";
				$input .= $this->get_field_input_if($real_type, $types_array, $field_info, $value);
				$input .= "</div>";
				$field_info->display_as = $display_as . " Hasta";
				$field_info->name = $name . "_2";

				$input .= "<div class='col-md-6 between_campo ".$field_info->name."'>";
				$input .= $this->get_field_input_if($real_type, $types_array, $field_info, $value);
				$input .= "</div>";
				$input .= "</div>";
				$field_info->name = $name;
				$field_info->display_as = $display_as;
			}
			else
			{
				$input .= $this->get_field_input_if($real_type,$types_array, $field_info,$value);
			}
		}
		else
		{
			$input = $this->get_field_input_if($real_type,$types_array, $field_info,$value);
		}

		$field_info->input = $input;

		return $field_info;
	}

	protected function get_field_input_if($real_type,$types_array, $field_info,$value){//asd
		if (in_array($real_type,$types_array)) {
			/* A quick way to go to an internal method of type $this->get_{type}_input .
                 * For example if the real type is integer then we will use the method
                 * $this->get_integer_input
                 *  */
			$input = $this->{"get_".$real_type."_input"}($field_info,$value);
		}
		else
		{
			$input = $this->get_string_input($field_info,$value);
		}

		return $input;
	}

	var $filter_between = array();
	public function set_filter_between()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_array($args[0]))
		{
			$args = $args[0];
		}

		$this->filter_between = $args;

		return $this;
	}

	protected function get_time_input($field_info,$value) {
		$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
		$this->set_css($this->default_css_path.'/jquery_plugins/jquery.ui.datetime.css');
		$this->set_css($this->default_css_path.'/jquery_plugins/jquery-ui-timepicker-addon.css');
		$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
		$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery-ui-timepicker-addon.js');

		if($this->language !== 'english') {
			include($this->default_config_path.'/language_alias.php');

			if(array_key_exists($this->language, $language_alias)) {
				$i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-'.$language_alias[$this->language].'.js';

				if(file_exists($i18n_date_js_file)) {
					$this->set_js_lib($i18n_date_js_file);
				}

				$i18n_datetime_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/timepicker/jquery-ui-timepicker-'.$language_alias[$this->language].'.js';

				if(file_exists($i18n_datetime_js_file)){
					$this->set_js_lib($i18n_datetime_js_file);
				}

			}

		}

		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery-ui-timepicker-addon.config.js');

		if(!empty($value) && $value != '00:00:00') {
			list($hours,$minutes) = explode(":",substr($value,0,6));
			$datetime = $hours.':'.$minutes;
		} else {
			$datetime = '';
		}

		$input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='text' value='$datetime' maxlength='5' class='time-input form-control' /> <a class='time-input-clear' tabindex='-1'>".$this->l('form_button_clear')."</a> (hh:mm)";
		return $input;
	}

	// para agregar un valor por defecto desde la DB
	protected function get_add_input_fields($field_values = null)
	{
		$fields = $this->get_add_fields();
		$types 	= $this->get_field_types();

		$input_fields = array();

		// AGREGADO LINK NUEVO ITEM
		$ci = &get_instance();
		$campos = [];

		if ($ci->session->userdata('from_url') == current_url() && $ci->session->userdata('form'))
		{
			$partes = explode('&', $ci->session->userdata('form'));

			if ($partes)
			{
				foreach ($partes as $parte)
				{
					$campo = explode('=', $parte);

					if (count($campo) == 2)
					{
						$campos[$campo[0]] = $campo[1];
					}
				}
			}

			$ci->session->unset_userdata('from_url');
			$ci->session->unset_userdata('form');
		}
		// FIN AGREGADO LINK NUEVO ITEM
		foreach($fields as $field_num => $field)
		{
			$field_info = $types[$field->field_name];

			$default = isset($field_info->default)? $field_info->default: null;
			$field_value = !empty($field_values) && isset($field_values->{$field->field_name}) ? $field_values->{$field->field_name} : $default;
			// AGREGADO LINK NUEVO ITEM
			if (isset($campos[$field->field_name]))
			{
				$field_value = urldecode($campos[$field->field_name]);
			}
			// FIN AGREGADO LINK NUEVO ITEM

			if(!isset($this->callback_add_field[$field->field_name]))
			{
				$field_input = $this->get_field_input($field_info, $field_value);
			}
			else
			{
				$field_input = $field_info;
				$field_input->input = call_user_func($this->callback_add_field[$field->field_name], $field_value, null, $field_info);
			}

			switch ($field_info->crud_type) {
				case 'invisible':
					unset($this->add_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
				case 'hidden':
					$this->add_hidden_fields[] = $field_input;
					unset($this->add_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
			}

			$input_fields[$field->field_name] = $field_input;
		}
		return $input_fields;
	}

	public function display_as_filter($field_name, $display_as = null)
	{

		$state_code = $this->getStateCode();

		if($state_code == 1 || $state_code == 7) // si es list armo una comun para que se arme filtro correctamente
		{
			$this->display_as($field_name, $display_as);
		}

		return $this;
	}


	protected function get_edit_input_fields($field_values = null)
	{
		$fields = $this->get_edit_fields();
		$types 	= $this->get_field_types();

		$input_fields = array();

		// AGREGADO LINK NUEVO ITEM
		$ci = &get_instance();
		$campos = [];

		if ($ci->session->userdata('from_url') == current_url() && $ci->session->userdata('form'))
		{
			$partes = explode('&', $ci->session->userdata('form'));

			if ($partes)
			{
				foreach ($partes as $parte)
				{
					$campo = explode('=', $parte);

					if (count($campo) == 2)
					{
						$campos[$campo[0]] = $campo[1];
					}
				}
			}

			$ci->session->unset_userdata('from_url');
			$ci->session->unset_userdata('form');
		}
		// FIN AGREGADO LINK NUEVO ITEM
		foreach($fields as $field_num => $field)
		{
			$field_info = $types[$field->field_name];

			$field_value = !empty($field_values) && isset($field_values->{$field->field_name}) ? $field_values->{$field->field_name} : null;
			// AGREGADO LINK NUEVO ITEM
			if (isset($campos[$field->field_name]))
			{
				$field_value = urldecode($campos[$field->field_name]);
			}
			// FIN AGREGADO LINK NUEVO ITEM
			if(!isset($this->callback_edit_field[$field->field_name]))
			{
				$field_input = $this->get_field_input($field_info, $field_value);
			}
			else
			{
				$primary_key = $this->getStateInfo()->primary_key;
				$field_input = $field_info;
				$field_input->input = call_user_func($this->callback_edit_field[$field->field_name], $field_value, $primary_key, $field_info, $field_values);
			}

			switch ($field_info->crud_type) {
				case 'invisible':
					unset($this->edit_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
				case 'hidden':
					$this->edit_hidden_fields[] = $field_input;
					unset($this->edit_fields[$field_num]);
					unset($fields[$field_num]);
					continue;
					break;
			}

			$input_fields[$field->field_name] = $field_input;
		}

		return $input_fields;
	}

	public function is_date($string_in, $format_in = 'd/m/Y')
	{
		$d = DateTime::createFromFormat($format_in, $string_in);
		// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
		return $d && $d->format($format_in) === $string_in;
	}

	public function date_front_to_back($date_in)
	{
		$date = null;

		if ($this->is_date($date_in))
		{
			$d = DateTime::createFromFormat('d/m/Y', $date_in);
			$date = $d->format('Y-m-d');
		}

		return $date;
	}

	protected function get_date_input($field_info,$value)
	{
		$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
		$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);

		if($this->language !== 'english')
		{
			include($this->default_config_path.'/language_alias.php');
			if(array_key_exists($this->language, $language_alias))
			{
				$i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-'.$language_alias[$this->language].'.js';
				if(file_exists($i18n_date_js_file))
				{
					$this->set_js_lib($i18n_date_js_file);
				}
			}
		}

		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.datepicker.config.js');

		if(!empty($value) && $value != '0000-00-00' && $value != '1970-01-01')
		{
			$fecha = explode('-',str_replace('/','-',$value));
			$year = $fecha[0];
			$month = $fecha[1];
			$day = $fecha[2];
			$date = date('d/m/Y', strtotime("$year-$month-$day"));
		}
		else
		{
			$date = '';
		}

		$input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='text' value='$date' maxlength='10' class='datepicker-input form-control' placeholder='{$field_info->display_as}' />
        <a class='datepicker-input-clear' tabindex='-1'>".$this->l('form_button_clear')."</a> (".$this->ui_date_format.")";// AGREGADO PLACEHOLDER

		return $input;
	}

	protected function showCloneForm($state_info)
	{
		$this->set_js_lib($this->default_javascript_path.'/'.grocery_CRUD::JQUERY);

		$data 				= $this->get_common_data();
		$data->types 		= $this->get_field_types();

		$data->field_values = $this->get_edit_values($state_info->primary_key);

		if(isset($this->unset_clone_fields)){
			foreach ($this->unset_clone_fields as $key => $field){
				unset($data->field_values->$field);
			}
		}

		$data->add_url		= $this->getAddUrl();
		$data->list_url 	= $this->getListUrl();
		$data->update_url	= $this->getInsertUrl();
		$data->delete_url	= $this->getDeleteUrl($state_info);
		$data->read_url		= $this->getReadUrl($state_info->primary_key);
		$data->input_fields = $this->get_edit_input_fields($data->field_values);
		$data->unique_hash			= $this->get_method_hash();

		$data->fields 		= $this->get_edit_fields();
		$data->hidden_fields	= $this->get_edit_hidden_fields();
		$data->unset_back_to_list	= $this->unset_back_to_list;

		$data->validation_url	= $this->getValidationInsertUrl();
		$data->is_ajax 			= $this->_is_ajax();

		$this->_theme_view('edit.php',$data);
		$this->_inline_js("var js_date_format = '".$this->js_date_format."';");

		$this->_get_ajax_results();
	}

	protected function get_dropdown_input($field_info,$value)
	{
		$this->load_js_chosen();
		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');

		$select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));

		$input = "<select id='field-{$field_info->name}' name='{$field_info->name}' class='chosen-select form-control' data-placeholder='".$select_title."'>";
		$options = array('' => '') + $field_info->extras;
		foreach($options as $option_value => $option_label)
		{
			$selected = !empty($value) && $value == $option_value ? "selected='selected'" : '';
			$input .= "<option value='$option_value' $selected >$option_label</option>";
		}

		$input .= "</select>";
		return $input;
	}

	protected function get_enum_input($field_info,$value)
	{
		$this->load_js_chosen();
		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');

		$select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));

		$input = "<select id='field-{$field_info->name}' name='{$field_info->name}' class='chosen-select form-control' data-placeholder='".$select_title."'>";
		$options_array = $field_info->extras !== false && is_array($field_info->extras)? $field_info->extras : explode("','",substr($field_info->db_max_length,1,-1));
		$options_array = array('' => '') + $options_array;

		foreach($options_array as $option)
		{
			$selected = !empty($value) && $value == $option ? "selected='selected'" : '';
			$input .= "<option value='$option' $selected >$option</option>";
		}

		$input .= "</select>";
		return $input;
	}

	protected function get_set_input($field_info,$value)
	{
		$this->load_js_chosen();
		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');

		$options_array = $field_info->extras !== false && is_array($field_info->extras)? $field_info->extras : explode("','",substr($field_info->db_max_length,1,-1));
		$selected_values 	= !empty($value) ? explode(",",$value) : array();

		$select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));
		$input = "<select id='field-{$field_info->name}' name='{$field_info->name}[]' multiple='multiple' size='8' class='chosen-multiple-select form-control' data-placeholder='$select_title' style='width:510px;' >";

		foreach($options_array as $option)
		{
			$selected = !empty($value) && in_array($option,$selected_values) ? "selected='selected'" : '';
			$input .= "<option value='$option' $selected >$option</option>";
		}

		$input .= "</select>";

		return $input;
	}

	protected function get_multiselect_input($field_info,$value)
	{
		$this->load_js_chosen();
		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');

		$options_array = $field_info->extras;
		$selected_values 	= !empty($value) ? explode(",",$value) : array();

		$select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));
		$input = "<select id='field-{$field_info->name}' name='{$field_info->name}[]' multiple='multiple' size='8' class='chosen-multiple-select form-control' data-placeholder='$select_title' style='width:510px;' >";

		foreach($options_array as $option_value => $option_label)
		{
			$selected = !empty($value) && in_array($option_value,$selected_values) ? "selected='selected'" : '';
			$input .= "<option value='$option_value' $selected >$option_label</option>";
		}

		$input .= "</select>";

		return $input;
	}

	protected function get_relation_input($field_info,$value)
	{
		$this->load_js_chosen();
		$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');

		$ajax_limitation = 10000;
		$total_rows = $this->get_relation_total_rows($field_info->extras);


		//Check if we will use ajax for our queries or just clien-side javascript
		$using_ajax = $total_rows > $ajax_limitation ? true : false;

		//We will not use it for now. It is not ready yet. Probably we will have this functionality at version 1.4
		$using_ajax = false;

		//If total rows are more than the limitation, use the ajax plugin
		$ajax_or_not_class = $using_ajax ? 'chosen-select' : 'chosen-select';

		$this->_inline_js("var ajax_relation_url = '".$this->getAjaxRelationUrl()."';\n");

		$select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));
		$input = "<select id='field-{$field_info->name}'  name='{$field_info->name}' class='$ajax_or_not_class form-control' data-placeholder='$select_title' style='width:300px'>";
		$input .= "<option value=''></option>";

		if(!$using_ajax)
		{
			$options_array = $this->get_relation_array($field_info->extras);
			foreach($options_array as $option_value => $option)
			{
				$selected = !empty($value) && $value == $option_value ? "selected='selected'" : '';
				$input .= "<option value='$option_value' $selected >$option</option>";
			}
		}
		elseif(!empty($value) || (is_numeric($value) && $value == '0') ) //If it's ajax then we only need the selected items and not all the items
		{
			$selected_options_array = $this->get_relation_array($field_info->extras, $value);
			foreach($selected_options_array as $option_value => $option)
			{
				$input .= "<option value='$option_value'selected='selected' >$option</option>";
			}
		}

		$input .= "</select>";
		// AGREGADO LINK NUEVO ITEM
		if (isset($field_info->extras[6]['link_nuevo_item']))
		{
			$ci = &get_instance();

			$input .= '
                <a href="'.$field_info->extras[6]['link_nuevo_item'].'" class="link_nuevo_item">Agregar</a>
                
                <script>
                    $(document).ready(function() {
                        $(".link_nuevo_item").click(function(e) {
                            e.preventDefault();
                            
                            var link = $(this).attr("href");
                            var form = $("#crudForm").serialize();
                            var from_url = window.location.href; 
                            
                            $.ajax({
                                url: "'.base_url().'admin/form/ajax_save",
                                type: "POST",
                                dataType: "json",
                                data: {
                                    '.$ci->security->get_csrf_token_name().': "'.$ci->security->get_csrf_hash().'",
                                    from_url: from_url,
                                    form: form
                                },
                                success: function(data) {
                                    window.location = link;
                                }
                            });
                        });
                    });
                </script>
            ';
		}
		// FIN AGREGADO LINK NUEVO ITEM
		return $input;
	}

	// Public function
	public function get_subject($plural = false)
	{
		if($plural)
			return $this->subject_plural;
		return $this->subject;
	}

}
