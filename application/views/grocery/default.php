<?php if(isset($pre_html)) { echo $pre_html; } ?>

<?php if (isset($js_to_load)) { ?>
    <?php foreach ($js_to_load as $row) { ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/<?php echo $row; ?>"></script>
    <?php } ?>
<?php } ?>

<?php echo $crud->output; ?>