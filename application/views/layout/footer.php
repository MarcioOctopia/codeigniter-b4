</div>
<!-- END Page Content -->

</main>
<!-- END Main Container -->

<!-- Footer -->
<footer id="page-footer" class="bg-body-light">
	<div class="content py-0">
		<div class="row font-size-sm">
			<div class="col-sm-6 order-sm-2 mb-1 mb-sm-0 text-center text-sm-right">
				Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
			</div>
			<div class="col-sm-6 order-sm-1 text-center text-sm-left">
				<a class="font-w600" href="https://1.envato.market/r6y" target="_blank">Dashmix 2.0</a> &copy; <span data-toggle="year-copy">2018</span>
			</div>
		</div>
	</div>
</footer>
<!-- END Footer -->
</div>
<!-- END Page Container -->

<!--
	Dashmix JS Core

	Vital libraries and plugins used in all pages. You can choose to not include this file if you would like
	to handle those dependencies through webpack. Please check out assets/_es6/main/bootstrap.js for more info.

	If you like, you could also include them separately directly from the assets/js/core folder in the following
	order. That can come in handy if you would like to include a few of them (eg jQuery) from a CDN.

	assets/js/core/jquery.min.js
	assets/js/core/bootstrap.bundle.min.js
	assets/js/core/simplebar.min.js
	assets/js/core/jquery-scrollLock.min.js
	assets/js/core/jquery.appear.min.js
	assets/js/core/js.cookie.min.js
-->
<script src="<?php echo base_url(); ?>assets/dashmix/js/dashmix.core.min.js"></script>

<!--
	Dashmix JS

	Custom functionality including Blocks/Layout API as well as other vital and optional helpers
	webpack is putting everything together at assets/_es6/main/app.js
-->
<script src="<?php echo base_url(); ?>assets/dashmix/js/dashmix.app.min.js"></script>

<?php foreach ($this->jsFiles as $file) : ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

</body>
</html>
