<?php echo form_open("auth/forgot_password", ["class" => "form-signin"]);?>

	<h1><?php echo lang('forgot_password_heading');?></h1>
	<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

	<div id="infoMessage"><?php echo $message;?></div>

      <p>
      	<label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
      	<?php echo form_input($identity, '', ["id" => "identity", "class" => "form-control", "placeholder" => "Email address", "required", "autofocus"]);?>
      </p>

      <p>
		  <?php echo form_submit('submit', lang('forgot_password_submit_btn'), ["class" => "btn btn-lg btn-primary btn-block"]);?>
		  <a href="<?php echo base_url('auth/login'); ?>">Cancelar</a>
	  </p>

<?php echo form_close();?>
