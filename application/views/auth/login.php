<div id="page-container">

	<!-- Main Container -->
	<main id="main-container">

		<!-- Page Content -->
		<div class="bg-image" style="background-image: url('assets/media/photos/photo22@2x.jpg');">
			<div class="row no-gutters bg-primary-op">
				<!-- Main Section -->
				<div class="hero-static col-md-6 d-flex align-items-center bg-white">
					<div class="p-3 w-100">
						<!-- Header -->
						<div class="mb-3 text-center">
							<a class="link-fx font-w700 font-size-h1" href="index.html">
								<span class="text-dark">Dash</span><span class="text-primary">mix</span>
							</a>
							<p class="text-uppercase font-w700 font-size-sm text-muted">Sign In</p>
						</div>
						<!-- END Header -->

						<div class="row no-gutters justify-content-center">
							<div class="col-sm-8 col-xl-6">
								<?php echo form_open("auth/login", ["class" => "form-signin"]);?>

									<h1 class="h3 mb-3 font-weight-normal"><?php echo lang('login_heading');?></h1>
									<p><?php echo lang('login_subheading');?></p>

									<div id="infoMessage"><?php echo $message;?></div>

									  <p>
										<?php echo lang('login_identity_label', 'identity');?>
										<?php echo form_input($identity, '', ["id" => "identity", "class" => "form-control form-control-lg form-control-alt", "placeholder" => "Email address", "required", "autofocus"]);?>
									  </p>

									  <p>
										<?php echo lang('login_password_label', 'password');?>
										<?php echo form_input($password, '', ["id" => "password", "class" => "form-control form-control-lg form-control-alt", "placeholder" => "Password", "required"]);?>
									  </p>

									  <p>
										<?php echo lang('login_remember_label', 'remember');?>
										<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
									  </p>


									  <p><?php echo form_submit('submit', lang('login_submit_btn'), ["class" => "btn btn-lg btn-primary btn-block"]);?></p>

									<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>

								<?php echo form_close();?>
							</div>
						</div>
					</div>

				</div>
				<!-- Meta Info Section -->
				<div class="hero-static col-md-6 d-none d-md-flex align-items-md-center justify-content-md-center text-md-center">
					<div class="p-3">
						<p class="display-4 font-w700 text-white mb-3">
							Welcome to the future
						</p>
						<p class="font-size-lg font-w600 text-white-75 mb-0">
							Copyright &copy; <span class="js-year-copy"><?php echo date('Y'); ?></span>
						</p>
					</div>
				</div>
				<!-- END Meta Info Section -->
			</div>
		</div>
	</main>
